<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">О компании</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk margin_bottom_x2">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <div class="">
                <div class="page-layout__content">
                    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center">Информация</span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Миссия</span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Руководство</span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">География</span>
                    </div>
                    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom margin_top_x2">Москва</span>
                    <span class="d-inline-flex margin_bottom_x2"> Филиал АЛИДИ в Москве был открыт в 2011 году. В настоящее время компания осуществляет дистрибуцию продукции P&G, Nestle Purina, Wrigley, MacCoffee, товаров для животных, кондитерских изделий на территории Москвы и Московской области. Кроме того, в Москве компания АЛИДИ является провайдером 3-PL услуг.

В Москве располагается департамент по продвижению продукции, в зону ответственности которого входит рекламно-маркетинговое сопровождение дистрибьюторских контрактов.

Площадь складских помещений АЛИДИ в Московской области составляет порядка 100 000 м2.

Филиал АЛИДИ в Москве является столицей дивизиона «Москва».</span>
                    <span>
                <b>Адрес офиса:</b><br>
            115201, г. Москва, Каширский проезд, д.23<br><br>
             </span>
                    <span>
            <b>Телефон:</b><br>
            8-800-775-75-00<br><br>
            </span>
                    <span>
            <b>Адрес складов:</b><br>
            142143, Московская область, городской округ Подольск, д. Валищево, 9 км. а/д А107 (Московское кольцо), 7А, стр.5<br>
            <b>Телефон:</b> 8-800-775-75-00<br><br>
            </span>
                    <span class="d-inline-block margin_bottom_x2">
            142322, Московская область, Чеховский район, д. Баранцево, «Новоселки» Промзона, д.11, стр.10<br>
            <b>Телефон:</b> 8-800-775-75-00
            </span>
                    <img class="margin_bottom_x2" src="img/image_about_msk.png" alt="image_msk" width="100%">
                    <a href="about_geography.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 width_260_desk style_padding_10 d-grid align-content-center justify-content-center return_cities">К списку городов</a>
                </div>
                <div class="">
                    <?php require('footer.php'); ?>
                </div>
            </div>
        </div>
        <div class="page-layout__content_mob" >
            <div class="page-layout__content">
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                    <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center">Информация</span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Миссия</span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Руководство</span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">География</span>
                </div>
                <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom margin_top_x2">Москва</span>
                <span class="d-inline-flex margin_bottom_x2"> Филиал АЛИДИ в Москве был открыт в 2011 году. В настоящее время компания осуществляет дистрибуцию продукции P&G, Nestle Purina, Wrigley, MacCoffee, товаров для животных, кондитерских изделий на территории Москвы и Московской области. Кроме того, в Москве компания АЛИДИ является провайдером 3-PL услуг.

В Москве располагается департамент по продвижению продукции, в зону ответственности которого входит рекламно-маркетинговое сопровождение дистрибьюторских контрактов.

Площадь складских помещений АЛИДИ в Московской области составляет порядка 100 000 м2.

Филиал АЛИДИ в Москве является столицей дивизиона «Москва».</span>
                <span>
                <b>Адрес офиса:</b><br>
            115201, г. Москва, Каширский проезд, д.23<br><br>
             </span>
                <span>
            <b>Телефон:</b><br>
            8-800-775-75-00<br><br>
            </span>
                <span>
            <b>Адрес складов:</b><br>
            142143, Московская область, городской округ Подольск, д. Валищево, 9 км. а/д А107 (Московское кольцо), 7А, стр.5<br>
            <b>Телефон:</b> 8-800-775-75-00<br><br>
            </span>
                <span class="d-inline-block margin_bottom_x2">
            142322, Московская область, Чеховский район, д. Баранцево, «Новоселки» Промзона, д.11, стр.10<br>
            <b>Телефон:</b> 8-800-775-75-00
            </span>
                <img class="margin_bottom_x2" src="img/image_about_msk.png" alt="image_msk" width="100%">
                <a href="about_geography.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 width_260_desk style_padding_10 d-grid align-content-center justify-content-center return_cities">К списку городов</a>
            </div>
            </div>
            <div class="">
                <?php require('footer.php'); ?>
            </div>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>
