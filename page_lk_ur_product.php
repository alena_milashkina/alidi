<div>
    <div class="d-flex flex-column flex-xl-row justify-content-between">
        <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill grow_02">
            <img src="img/CalendarBlank.svg" width="20" alt="calendar" class="margin_right">
            <input class="style_16_24 fw-bold input_date_rang width_200_mob cursor_pointer me-auto datepicker" placeholder="Выберите дату" type="text" id="datepicker2"/>
            <img src="img/arrowDropBig.svg" width="20" alt="dropDown">
        </div>
        <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
            <img class="margin_right" src="img/iconSearchBlack.svg" width="20" alt="search">
            <input class="style_gray_radius style_border_transparent" type="text" placeholder="Поиск">
        </div>
    </div>
    <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
        <img src="img/iconUserRectangle.svg" width="20" alt="search" class="margin_right">
        <select class="form-select form-select-sm style_border_transparent input_date_rang fw-bold" aria-label=".form-select-lg example">
            <option selected>Все плательщики</option>
            <option value="1">плательщик 1</option>
            <option value="2">плательщик 2</option>
            <option value="3">плательщик 3</option>
            <option value="4">плательщик 4</option>
            <option value="5">плательщик 5</option>
        </select>
    </div>
    <div class="d-inline-block margin_bottom width_100_desk style_padding_18_20 style_border_lightgray style_border_radius_20 style_border_radius_40_only_desk padding_30_desk">
        <div class="d-flex flex-column flex-xl-row justify-content-start ">
            <div class="position-relative in_stock_tip margin_right_desk">
                <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
            </div>
            <div class="d-flex flex-column justify-content-start margin_right_desk">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom_x2">
                    <span>Цена: 1 069 ₽  х 12 шт.</span>
                </div>
                <select class="form-select form-select-sm style_border_lightgray style_border_radius_20 margin_bottom" aria-label=".form-select-lg">
                    <option selected disabled>Дата заказа: 19 января  2021</option>
                    <option value="1" disabled>11 января 2021 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 781,67 ₽</option>
                    <option value="2" disabled>12 декабря 2020 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 781,67 ₽</option>
                    <option value="3" disabled>3 декабря 2020 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 781,67 ₽</option>
                </select>
                <b class="d-inline-block">№ 378-5929-0214</b>
            </div>
        </div>
    </div>
    <div class="d-inline-block margin_bottom width_100_desk style_padding_18_20 style_border_lightgray style_border_radius_20 style_border_radius_40_only_desk padding_30_desk">
        <div class="d-flex flex-column flex-xl-row justify-content-start ">
            <div class="position-relative in_stock_tip margin_right_desk">
                <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
            </div>
            <div class="d-flex flex-column justify-content-start margin_right_desk">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom_x2">
                    <span>Цена: 1 069 ₽  х 12 шт.</span>
                </div>
                <select class="form-select form-select-sm style_border_lightgray style_border_radius_20 margin_bottom" aria-label=".form-select-lg">
                    <option selected disabled>Дата заказа: 19 января  2021</option>
                    <option value="1" disabled>11 января 2021 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 781,67 ₽</option>
                    <option value="2" disabled>12 декабря 2020 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 781,67 ₽</option>
                    <option value="3" disabled>3 декабря 2020 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 781,67 ₽</option>
                </select>
                <b class="d-inline-block">№ 378-5929-0214</b>
            </div>
        </div>
    </div>
</div>