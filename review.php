<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20">
        <div>
            <span class="heading_24 margin_right">Отзыв</span>
        </div>
        <a href="item.php" class="close_menu"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <span class="d-inline-block margin_bottom_x2 fw-bold">Оцените покупку</span>
        <div class="d-flex flex-row justify-content-between margin_bottom_x2">
            <span class="iconStar_40 iconStar_active_40"></span>
            <span class="iconStar_40 iconStar_active_40"></span>
            <span class="iconStar_40 iconStar_active_40"></span>
            <span class="iconStar_40"></span>
            <span class="iconStar_40"></span>
        </div>
        <textarea class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" placeholder="Ваш отзыв" rows="6"></textarea>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95 position-sticky button_filter bg_white" type="submit" value="Отправить">
    </form>
</div>
<?php require('js.php'); ?>
</body>
</html>


