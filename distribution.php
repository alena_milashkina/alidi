<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="page-layout__content d-flex flex-column justify-content-between">
            <div class="d-flex flex-row justify-content-between align-items-baseline">
               <span class="d-inline-block heading_24 style_text_40_50_desk fw-bold margin_bottom margin_right flex-fit">Услуги</span>
                <hr class="flex-fill align-self-start" style="opacity: 1; height: 2px; color: #000000;">
            </div>
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center"><a href="services.php">BTL</a></span>
                <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="distribution.php">Дистрибуция</a></span>
                <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="logistic.php">Логистика</a></span>
                <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="horeca.php">HoReCa</a></span>
            </div>
            <span class="d-inline-flex style_16_24 fw-bold margin_bottom_x2">АЛИДИ – одна из крупнейших дистрибьюторских компаний в России. Линейка продукции компании включает в себя бытовую химию, товары личной гигиены, продукты питания, детское питание, корма и товары для животных. Сегодня АЛИДИ имеет широкую дистрибуционную сеть, обслуживая более 120 000 торговых точек. Среди них – крупные торговые сети, оптовые компании, частные предприниматели.</span>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

