$(document).ready( function() {
    /*if (document.querySelector('.menu_burger') != null) {
        let openMenu = document.querySelector('.menu_burger');
        let menu = document.querySelector('.open_menu');
        let closeMenu = document.querySelector('.close_menu');
        openMenu.onclick = function () {
            menu.style.position = 'absolute';
            menu.classList.toggle('d-flex');
            menu.classList.toggle('flex-column');
            menu.classList.toggle('justify-content-between');
        };
        closeMenu.onclick = function () {
            menu.style.display = 'none';
            menu.classList.toggle('d-flex');
            menu.classList.toggle('flex-column');
            menu.classList.toggle('justify-content-between');
        };
    }*/

    if (document.querySelector('.accordion') != null) {
        let fiz = document.querySelector('.auth_fiz');
        let ur = document.querySelector('.auth_ur');
        let formFiz = document.querySelector('.form_fiz');
        let formUr = document.querySelector('.form_ur');
        fiz.onclick = function () {
            ur.classList.remove('accordion_active');
            fiz.classList.add('accordion_active');
            formFiz.classList.remove('d-none');
            formUr.classList.add('d-none');
        };
        ur.onclick = function () {
            ur.classList.add('accordion_active');
            fiz.classList.remove('accordion_active');
            formUr.classList.remove('d-none');
            formFiz.classList.add('d-none');

        };
    }
/*    if(document.querySelector('.tabs') != null) {
      let tabs = document.querySelectorAll('.tab');
      let identifyContent = document.querySelector('.contents').children;
      for(let t=0; t<tabs.length; t++) {
          let tab = tabs[t];
          /!*let contents = identifyContent[t].parentNode.querySelector('.contents');
          let content = contents.children;*!/
          for(let i=0; i<tab.length; i++) {
              tab[i].onclick = function() {
                  for(let j=0; j<tab.length; j++) {
                      tab[j].classList.remove('active_tab');
                      identifyContent[j].style.display = 'none';
                  }
                  this.classList.add('active_tab');
                  identifyContent[i].style.display = 'block';
              };
          }
      }
    }*/
    if(document.querySelector('.tabs') != null) {
        let tabs = document.querySelectorAll('.tabs');
        for(let t=0; t<tabs.length; t++) {
            let tab = tabs[t].children;
            /*let tab = document.querySelectorAll('.owl-item');*/
            let contents = tabs[t].parentNode.querySelector('.contents');
            let content = contents.children;
            for(let i=0; i<tab.length; i++) {
                tab[i].onclick = function() {
                    let activeTabs = this.parentNode.parentNode.querySelectorAll('.active_tab');
                    /*console.log(activeTabs);*/
                    for(let a=0; a<activeTabs.length; a++) {
                        activeTabs[a].classList.remove('active_tab');
                    }
                    for(let j=0; j<tab.length; j++) {
                        content[j].style.display = 'none';
                    }
                    this.classList.add('active_tab');
                    content[i].style.display = 'block';
                };
            }
        }
    }
    if(document.querySelector('.frame') != null) {
        /*let slyInstance = document.querySelector('.frame');*/
        let slyOptions = {
            horizontal: true,
            itemNav: 'centered',
            speed: 300,
            mouseDragging: 1,
            touchDragging: 1,
        };
        let slyInstance = new Sly('#frame', slyOptions).init();
    }
    /*    if (innerWidth>1200) {

      }
    /*    if(document.querySelector('.owl8') != null && innerWidth < 1200) {
            let owl8 = $('.owl8');
            owl8.owlCarousel({
                loop:false,
                autoWidth: false,
                items:3,
                dots: false,
                autoplay: false,
            });
        }if(innerWidth > 1200) {
            let owl8 = $('.owl8');
            owl8.trigger('destroy.owl.carousel');
        }*/

    if(document.querySelector('.tabs1') != null) {
        let tabs = document.querySelectorAll('.tabs1');
        for(let t=0; t<tabs.length; t++) {
            let tab = tabs[t].children;
            let contents = tabs[t].parentNode.querySelector('.contents1');
            let content = contents.children;
            for(let i=0; i<tab.length; i++) {
                tab[i].onclick = function() {
                    for(let j=0; j<tab.length; j++) {
                        tab[j].classList.remove('active_tab');
                        content[j].style.display = 'none';
                    }
                    this.classList.add('active_tab');
                    content[i].style.display = 'block';
                };
            }
        }
    }
    if (document.querySelector('.header_accordion') != null) {
        let headers = document.querySelectorAll('.header_accordion');
        let contents = document.querySelectorAll('.content_accordion');
        let arrow = document.querySelectorAll('.icon_arrow_down_small');
        for(let i=0; i<headers.length; i++) {
            headers[i].onclick = function() {
                for(let j=0; j<headers.length; j++) {
                    arrow[j].classList.remove('rotate_270');
                    arrow[j].classList.add('rotate_90');
                    contents[j].classList.add('d-none');
                }
                arrow[i].classList.remove('rotate_90');
                arrow[i].classList.add('rotate_270');
                contents[i].classList.remove('d-none');
            }
        }
    }
/*    if (document.querySelector('.delete_js') != null) {
        let cancelPopUp = document.querySelector('.cancel_popup_js');
        let deleteItemBtn = document.querySelector('.delete_js');
        let confirmationDelete = document.querySelector('.confirmation_delete_js');
        let mask = document.querySelector('.bg_mask');
        deleteItemBtn.onclick = function () {
            confirmationDelete.style.display = 'flex';
            mask.classList.toggle('mask');
        };
        cancelPopUp.onclick = function () {
            confirmationDelete.style.display = 'none';
            mask.classList.toggle('mask');
        };
    }*/
   /* if (document.querySelectorAll('.datepicker') != null) {
        for(let i=0; i<document.querySelectorAll('.datepicker').length; i++){
            let picker = new Lightpick({
                field: document.querySelector('.datepicker1'),
                singleDate: false,
                onSelect: function(start, end){
                    let str = '';
                    str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
                    str += end ? end.format('Do MMMM YYYY') : '...';
                    /!*document.getElementById('result-2').innerHTML = str;*!/
                }
            });
        }

    }*/
    if (document.getElementById('datepicker2') != null) {
        new Lightpick({
            field: document.getElementById('datepicker2'),
            singleDate: false,
            lang: 'ru',
            locale: {
                tooltip: {
                    one: 'день',
                    few: 'дня',
                    many: 'дней',
                },
                pluralize: function(i, locale) {
                    if ('one' in locale && i % 10 === 1 && !(i % 100 === 11)) return locale.one;
                    if ('few' in locale && i % 10 === Math.floor(i % 10) && i % 10 >= 2 && i % 10 <= 4 && !(i % 100 >= 12 && i % 100 <= 14)) return locale.few;
                    if ('many' in locale && (i % 10 === 0 || i % 10 === Math.floor(i % 10) && i % 10 >= 5 && i % 10 <= 9 || i % 100 === Math.floor(i % 100) && i % 100 >= 11 && i % 100 <= 14)) return locale.many;
                    if ('other' in locale) return locale.other;

                    return '';
                }
            }/*,
            onSelect: function(start, end){
                document.getElementById('result-10').innerHTML = rangeText(start, end);
            }*/
        });
        /*let picker = new Lightpick({
            field: document.getElementById('datepicker2'),
            singleDate: false,
            onSelect: function(start, end){
                let str = '';
                str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
                str += end ? end.format('Do MMMM YYYY') : '...';
                /!*document.getElementById('result-2').innerHTML = str;*!/
            }
        });*/
    }
    if (document.getElementById('datepicker3') != null) {
        let date = document.querySelectorAll('.datepicker3');
        for(let d=0;d<date.length;d++){
            let picker = new Lightpick({
                field: date[d],
                singleDate: false,
                onSelect: function(start, end) {
                    let str = '';
                    str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
                    str += end ? end.format('Do MMMM YYYY') : '...';
                }
            });
        }
        /*let picker = new Lightpick({
            field: document.getElementById('datepicker3'),
            singleDate: false,
            onSelect: function(start, end){
                let str = '';
                str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
                str += end ? end.format('Do MMMM YYYY') : '...';

            }
        });*/
    }
    if (document.getElementById('datepicker4') != null) {
        let date = document.querySelectorAll('.datepicker4');
        for(let d=0;d<date.length;d++){
            let picker1 = new Lightpick({
                field: date[d]
            });
        }

    }
    if (document.querySelector('.open_menu_catalog') !== null) {
        let openCatalog = document.querySelector('.menu_catalog');
        let menuCatalog = document.querySelector('.open_menu_catalog');
        openCatalog.onclick = function() {
            menuCatalog.classList.toggle('show');
        }
    }
    if (document.querySelector('.owl1') != null) {
        let owl = $('.owl1');
        owl.owlCarousel({
            items:1,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            dots: false,
        });
    }
    if (document.querySelector('.owl2') != null) {
        let owl1 = $('.owl2');
        owl1.owlCarousel({
            items:1,
            loop:true,
            margin:10,
            dots: false,
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            autoHeight:true,
            nav: true,
            navContainerClass: 'owl-nav-position-owl2',
        });
    }
    if (document.querySelector('.owl3') != null) {
        let owl3 = $('.owl3');
        owl3.owlCarousel({
            items:1,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            stagePadding: 50,
            dots: false,
            autoHeight: true,
            responsive:{
                768:{
                    items:3,
                },
            }
        });
    }

    $('.dropdown-el').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('expanded');
        const id = $(e.target).attr('for');
        $(this).find('input').filter(`#${id}`).prop('checked', true);
    });

 
    if (document.querySelector('.owl4') != null) {
        let owl4 = $('.owl4');
        owl4.owlCarousel({
            items:1,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            dots: false,
        });
    }

    if (document.querySelector('.owl5') != null) {
        let owl5 = $('.owl5');
        owl5.owlCarousel({
            items:1,
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            dots: false,
        });
    }

   
   if(document.querySelector('.owl6') != null) {
       let owl6 = $('.owl6');
       owl6.owlCarousel({
           items:1,
           loop:true,
           margin:10,
           dots: false,
           autoplay:true,
           autoplayTimeout:5000,
           autoplayHoverPause:true,
           navContainer: '.customNavigation',
       });
   }

    if(document.querySelector('.owl7') != null) {
        let owl7 = $('.owl7');
        owl7.owlCarousel({
            items: 1,
            loop: true,
            margin: 10,
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            nav: true,
            stagePadding: 335,
            center: true,
        });
    }

    if (document.querySelector('.index_location') != null) {
        let openSelect = document.querySelector('.index_location');
        let locationSelect = document.querySelector('.location_select');
        /*let confirmationDelete = document.querySelector('.confirmation_delete_js');*/
        let mask = document.querySelector('.bg_mask');
        openSelect.onclick = function () {
            locationSelect.classList.toggle('d-none');
            mask.classList.toggle('d-none');
            mask.classList.toggle('mask_with_open_header');
            document.body.classList.toggle('overflow-y-hiddenGGK');
        };
        /*$(document).click(function() {
            $('.bg_mask').removeClass('mask_with_open_header');
            locationSelect.classList.toggle('d-none');
        });*/
    }
    if(document.querySelector('.slider-input') != null) {
        $('.slider-input').jRange({
            from: 0,
            to: 100,
            step: 1,
            /*scale: [0,25,50,75,100],*/
            format: '%s',
            width: 340,
            showLabels: true,
            isRange : true,
            theme: "theme-blue",
            showScale: false,
        });
    }
    if(document.getElementById('entrance') != null) {
        let openAuth = document.querySelector('.open_auth');
        let openEntrance = document.querySelectorAll('.open_entrance');
        let openForgotPassword = document.querySelector('.open_forgot_password');
        let comeBack = document.querySelector('.come_back');
        let modalEntrance = document.querySelector('.modal_entrance');
        let modalAuth = document.querySelector('.modal_auth');
        let modalForgotPassword = document.querySelector('.modal_forgot_password');
        openAuth.onclick = function() {
            modalEntrance.classList.toggle('d-none');
            modalAuth.classList.toggle('d-none');
        };
        for(let i=0; i<openEntrance.length; i++){
            openEntrance[i].onclick = function() {
                modalEntrance.classList.toggle('d-none');   
                modalAuth.classList.toggle('d-none');
            };
        }
        openForgotPassword.onclick = function() {
            modalEntrance.classList.toggle('d-none');
            modalForgotPassword.classList.toggle('d-none');
        };
        comeBack.onclick = function() {
            modalEntrance.classList.toggle('d-none');
            modalForgotPassword.classList.toggle('d-none');
        };
    }
    if (document.querySelectorAll('.view') != null && document.querySelectorAll('.view').length > null) {
        let view1 = document.querySelector('.view1');
        let view2 = document.querySelector('.view2');
        let viewItem1 = document.querySelectorAll('.view_item1');
        let viewItem2 = document.querySelectorAll('.view_item2');
        view1.onclick = function() {
            view1.classList.toggle('d-none');
            view2.classList.toggle('d-none');
            for(let i=0;i<viewItem1.length; i++) {
                viewItem1[i].classList.toggle('d-none');
            }
            for(let i=0;i<viewItem2.length; i++) {
                viewItem2[i].classList.toggle('d-none');
            }
        };
        view2.onclick = function() {
            view1.classList.toggle('d-none');
            view2.classList.toggle('d-none');
            for(let i=0;i<viewItem1.length; i++) {
                viewItem1[i].classList.toggle('d-none');
            }
            for(let i=0;i<viewItem2.length; i++) {
                viewItem2[i].classList.toggle('d-none');
            }
        };
    }
    if(document.querySelectorAll('.item') !=null) {
        /*let item = document.querySelectorAll('.item');*/
        /*let button = item.find('.add_item_basket');*/
        let buttonBasket =  $('.item').find('.add_item_basket');
        let count = $('.item').find('.count_item_basket');
        for (let i=0;i<buttonBasket.length;i++) {
            buttonBasket[i].onclick = function() {
                buttonBasket[i].classList.add('d-none');
                for(let j=0;j<count.length;j++){
                    count[i].classList.remove('d-none');
                }
            };

        }
    }
    if(document.querySelector('.add_favorite') != null) {
        let container =  document.querySelector('.add_favorite');
        let iconEmpty = document.querySelector('.iconFavorite');
        let iconFull = document.querySelector('.iconFavorite_active');
        container.onclick = function (){
            iconEmpty.classList.toggle('d-none');
            iconFull.classList.toggle('d-none');
        };
    }
    let popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
    let popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
        return new bootstrap.Popover(popoverTriggerEl, {
            trigger: 'hover click',

        })
    });

});
