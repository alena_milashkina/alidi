<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Услуги</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk margin_bottom_x2">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
        <div class="page-layout__content d-flex flex-column justify-content-start">
            <!--<div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right tabs">
                <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">BTL</span>
                <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Дистрибуция</span>
                <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center">Логистика</span>
                <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">HoReCa</span>
            </div>-->
            <div class="d-flex flex-row justify-content-between align-items-start margin_bottom style_border_radius_40 style_accordion  owl-carousel owl8 tabs">
                <div class="d-inline-block style_text_12_16 fw-bold text-center style_border_radius_40 style_padding_10_20 cursor_pointer active_tab style_width_max tab">BTL</div>
                <div class="d-inline-block  style_padding_10_20 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Дистрибуция</div>
                <div class="d-inline-block  style_padding_10_20 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Логистика</div>
                <div class="d-inline-block  style_text_12_16 fw-bold style_padding_10_20 cursor_pointer style_border_radius_40 text-center style_width_max tab">HoReCa</div>
            </div>
            <div class="contents">
                <div class="btl">
                    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">ГК АЛИДИ</span>
                    <div class="d-grid grid_layout_2_3 grid_layout_3_2 margin_bottom_x2">
                        <div class="bg_blue position_map padding_30_desk style_border_radius_40 color_white d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_map">
                            </div>
                            <div class="">
                                <span class="bg_index__heading">Более 120 000 торговых точек в покрытии</span>
                            </div>
                        </div>
                        <div class="bg_grey position_users_black padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_users">
                            </div>
                            <div class="">
                                <span class="bg_index__text">9000 квалифицированных полевых сотрудников</span>
                            </div>
                        </div>
                        <div class="bg_grey position_table padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_table ">
                            </div>
                            <div class="">
                                <span class="bg_index__text">180 000 м2 площадь складских помещений </span>
                            </div>
                        </div>
                        <div class="bg_grey position_ruble padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_ruble ">
                            </div>
                            <div class=" ">
                                <span class="bg_index__text">68 млрд. росс. рублей - годовой оборот в 2019 г.</span>
                            </div>
                        </div>
                    </div>
                    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom_x2">АЛИДИ Продвижение</span>
                    <div class="d-grid grid_promotion margin_bottom_x2">
                        <div class="bg_blue position_users padding_30_desk style_border_radius_40 color_white d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_users_white">
                            </div>
                            <div class="">
                                <span class="bg_index__heading">3000+человек — штат мерчендайзеров, консультантов, промоутеров </span>
                            </div>
                        </div>
                        <div class="bg_grey position_blank padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_blank">
                            </div>
                            <div class="">
                                <span class="bg_index__text">17+ лет — самый долгосрочный проект </span>
                            </div>
                        </div>
                        <div class="bg_grey position_micro padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_megaphone">
                            </div>
                            <div class="">
                                <span class="bg_index__text">650+ человек — самая крупная промо-акция</span>
                            </div>
                        </div>
                        <div class="bg_grey position_doc padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_docs">
                            </div>
                            <div class=" ">
                                <span class="bg_index__text">8+ лет — средний стаж управленческого персонала</span>
                            </div>
                        </div>
                        <div class="bg_grey position_globe padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                            <div class="bg_index__bg_icon icon_about_globe">
                            </div>
                            <div class=" ">
                                <span class="bg_index__text">Представлены в России, Беларуси и Казахстане</span>
                            </div>
                        </div>
                    </div>
                    <img src="img/map_services.png" width="100%" alt="map alidi" class="margin_bottom_x2">
                    <a href="https://alidi-btl.ru/" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold width_300_desk style_padding_10 d-grid align-content-center justify-content-center">Перейти на сайт ALIDI BTL</a>
                </div>
                <div class="distribution align-self-start">
                    <span class="d-inline-flex style_16_24 fw-bold margin_bottom_x2">АЛИДИ – одна из крупнейших дистрибьюторских компаний в России. Линейка продукции компании включает в себя бытовую химию, товары личной гигиены, продукты питания, детское питание, корма и товары для животных. Сегодня АЛИДИ имеет широкую дистрибуционную сеть, обслуживая более 120 000 торговых точек. Среди них – крупные торговые сети, оптовые компании, частные предприниматели.</span>
                </div>
                <div class="logistic margin_top_desk">
                     <span class="d-inline-flex style_16_24 fw-bold margin_bottom_x2">Компания АЛИДИ – один из ведущих российских логистических операторов, предлагающая своим партнерам полный комплекс 3PL услуг: складскую обработку, ответственное хранение и доставку товаров.
<br><br>
В настоящее время АЛИДИ оказывает логистические услуги в Москве, Санкт-Петербурге, Нижнем Новгороде на складских терминалах класса А, а также в регионах России. Общая площадь складских помещений АЛИДИ превышает 180 000 м2.
<br><br>
Складские комплексы компании оборудованы по международным стандартам и оснащены современными информационными системами: ERP система J.D.Edwards, WMS Manhattan.</span>
                    <a href="#" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold width_400_desk style_padding_10 d-grid align-content-center justify-content-center">На сайт АЛИДИ Логистика</a>
                </div>
                <div class="horeca margin_top_x2">
                    <span class="d-inline-block style_16_24 fw-bolder margin_bottom_x2">ALIDI RPOF — Поставщик продуктов питания для сегмента HoReCa в Санкт-Петербурге, Ленинградской области, Великом Новгороде, Пскове и Петрозаводске.
<br><br>
С 2020 года ALIDI RPOF открыла доставку для физических лиц на дом, продуктов питания и непродовольственных товаров по оптовым ценам.
<br><br>
Ключевое направление в компании АЛИДИ для HoReCa стартовало в 2016 году. На сегодняшний день, у нас более 1400 активных клиентов.
<br><br>
Мы охватили все направления данного сегмента:
Сетевые рестораны, бары, пиццерии, кафе, пекарни, производства, кулинарии, кейтеринг и службы доставки.
<br><br>
Ежегодный прирост новых клиентов составляет 20%.
Объем продаж за 2019 год вырос на 55%.
Эксклюзивные контракты с мировыми производителями и брендами. Возможность создания эксклюзивных рецептур по потребностям клиента.
В ассортименте более 3000 товаров от 80 производителей, таких как Unilever Food Solutions, Cargill, Solpro, Чудское озеро, Heinz, Sen Soy, Лимак и многие другие.<br>
АЛИДИ является одним из основных дистрибьюторов компании Nestle и подразделения Nestle Waters. Как поставщик, предлагаем полный ассортимент торговых марок Perrier, Vittel, Acqua Panna и S.Pellegrino.</span>
                    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom_x2">Складская логистика</span>
                    <div class="grid_layout_2_2 margin_bottom align-self-center margin_bottom_x2">
                        <div class="bg_grey padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between height_172">
                            <div class="bg_index__bg_icon icon_table margin_bottom">
                            </div>
                            <div class="">
                                <span class="bg_index__text">Площадь склада 13 000 кв.м.</span>
                            </div>
                        </div>
                        <div class="bg_grey padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between height_172">
                            <div class="bg_index__bg_icon icon_temperature_plus margin_bottom">
                            </div>
                            <div class="">
                                <span class="bg_index__text">Сухая отапливаемая зона (до +25°С)</span>
                            </div>
                        </div>
                        <div class="bg_grey padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between height_172">
                            <div class="bg_index__bg_icon icon_temperature_minus margin_bottom">
                            </div>
                            <div class="">
                                <span class="bg_index__text">Холодильная камера (от +2°C до +6°С)</span>
                            </div>
                        </div>
                        <div class="bg_grey padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between height_172">
                            <div class="bg_index__bg_icon icon_frige margin_bottom">
                            </div>
                            <div class="">
                                <span class="bg_index__text">Морозильная камера (до –21°С)</span>
                            </div>
                        </div>
                        <div class="bg_grey padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between height_172">
                            <div class="bg_index__bg_icon icon_conditioner margin_bottom">
                            </div>
                            <div class="">
                                <span class="bg_index__text">Кондиционированная зона для хранения шоколада (от +15°С до +21°С)</span>
                            </div>
                        </div>
                        <div class="bg_grey padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between height_172">
                            <div class="bg_index__bg_icon icon_sertif margin_bottom">
                            </div>
                            <div class="">
                                <span class="bg_index__text">Склад соответствует международным стандартам и имеет сертификат HACCP</span>
                            </div>
                        </div>
                    </div>
                    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Транспортная логистика</span>
                    <span class="d-inline-block style_16_24 margin_bottom_x2">Зона доставки распространяется по всему Санкт-Петербургу и Ленинградской области.
Помимо этого, мы доставляем продукты в республику Карелия, Великий Новгород и Псковскую область.
<br><br>
Собственный парк новых автомобилей с температурным мультирежимом, для перевозки охлажденной и замороженной продукции.
Бесплатная доставка на следующий день.</span>
                    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Интернет-портал</span>
                    <span class="d-inline-block style_16_24 margin_bottom_x2">Интернет - портал с полным каталогом товаров, где вы можете увидеть фото товара, ознакомиться с составом, сроками и условиями хранения.
<br><br>
После регистрации на сайте, клиент сможет увидеть в личном кабинете:<br>
- Какой товар, когда и по какой цене он брал<br>
- Загрузить матрицу товаров с согласованными ценами<br>
- В один клик выгрузить все сертификаты к поставке<br>
- Ознакомиться с финансами и кредитным лимитом<br>
- Найти и выгрузить любую накладную<br>
<br><br>
Мы принимаем заказы через сайт, торгового представителя, по почте или телефону, в любом удобном формате.
<br><br>
Работаем в системе EDI.</span>
                    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Шеф-повар и кухня-студия</span>
                    <span class="d-inline-block style_16_24 margin_bottom_x2">Для лучшего взаимопонимания мы создали специальную кухню — студию с новым современным оборудованием и большим экраном. Как и на любой профессиональной кухне, на нашей тоже есть свой шеф-повар.
<br><br>
Все продукты, появляющиеся в ассортименте АЛИДИ ПРОФ попадают на проработку к шеф-повару, где он тщательно проводит над ними эксперименты, готовит разнообразные блюда и проверяет по всем органолептическим показателям.
<br><br>
Каждую неделю шеф-повар приглашает на кухню-студию весь отдел направления HoReCа, где рассказывает про тонкости работы кухни в ресторанах, барах и гостиницах. Проводит мастер-класс по приготовлению популярных блюд из позиций нашего ассортимента и подробно рассказывает
о них.
<br><br>
Шеф-повар совместно с торговыми представителями выезжает на встречи с шеф-поварами заведений общественного питания, чтобы поучаствовать в проработке новых позиций и помочь в составлении меню из продуктов нашего ассортимента.
<br><br>
Если вы являетесь нашим клиентом и хотите проработать продукты совместно с нашим шеф-поваром, вам достаточно сообщить об этом вашему менеджеру или торговому представителю для уточнения проработки и согласования удобного времени.
<br><br>
Если вы еще не являетесь клиентом АЛИДИ, регистрируйтесь на сайте alidi.ru или позвоните<br>
по номеру 8-800-775-75-00.</span>
                </div>
            </div>

        </div>
        </div>
        <div class="page-layout__content_mob" >
            <div class="page-layout__content d-flex flex-column justify-content-between">
                <div class="d-flex flex-row justify-content-between align-items-start margin_bottom style_border_radius_40 style_accordion owl-carousel owl8 tabs">
                    <div class="d-inline-block style_text_12_16 fw-bold text-center style_border_radius_40 style_padding_10_20 cursor_pointer active_tab style_width_max tab">BTL</div>
                    <div class="d-inline-block  style_padding_10_20 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Дистрибуция</div>
                    <div class="d-inline-block  style_padding_10_20 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Логистика</div>
                    <div class="d-inline-block  style_text_12_16 fw-bold style_padding_10_20 cursor_pointer style_border_radius_40 text-center style_width_max tab">HoReCa</div>
                </div>
                <div class="contents">
                    <div class="btl">
                        <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">ГК АЛИДИ</span>
                        <div class="d-grid grid_layout_2_3 margin_bottom_x2">
                            <div class="bg_blue position_map style_padding_18_20 section_style height_150 d-flex flex-column justify-content-between margin_top_bottom_10 d-xl-none">
                                <div class="bg_index__bg_icon icon_map">
                                </div>
                                <div class="">
                                    <span class="bg_index__heading">Более 120 000 торговых точек в покрытии</span>
                                </div>
                            </div>
                            <div class="bg_grey position_users_black style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_users">
                                </div>
                                <div class="">
                                    <span class="bg_index__text">9000 квалифицированных полевых сотрудников</span>
                                </div>
                            </div>
                            <div class="bg_grey position_table style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_table ">
                                </div>
                                <div class="">
                                    <span class="bg_index__text">180 000 м2 площадь складских помещений </span>
                                </div>
                            </div>
                            <div class="bg_grey position_ruble style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_ruble ">
                                </div>
                                <div class=" ">
                                    <span class="bg_index__text">68 млрд. росс. рублей - годовой оборот в 2019 г.</span>
                                </div>
                            </div>
                        </div>
                        <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">АЛИДИ Продвижение</span>
                        <div class="d-grid grid_promotion margin_bottom_x2">
                            <div class="bg_blue position_users style_padding_18_20 section_style height_150 d-flex flex-column justify-content-between margin_top_bottom_10 d-xl-none">
                                <div class="bg_index__bg_icon icon_users_white">
                                </div>
                                <div class="">
                                    <span class="bg_index__heading">3000+человек — штат мерчендайзеров, консультантов, промоутеров </span>
                                </div>
                            </div>
                            <div class="bg_grey position_blank style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_blank">
                                </div>
                                <div class="">
                                    <span class="bg_index__text">17+ лет — самый долгосрочный проект </span>
                                </div>
                            </div>
                            <div class="bg_grey position_micro style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_megaphone">
                                </div>
                                <div class="">
                                    <span class="bg_index__text">650+ человек — самая крупная промо-акция</span>
                                </div>
                            </div>
                            <div class="bg_grey position_doc style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_docs">
                                </div>
                                <div class=" ">
                                    <span class="bg_index__text">8+ лет — средний стаж управленческого персонала</span>
                                </div>
                            </div>
                            <div class="bg_grey position_globe style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between">
                                <div class="bg_index__bg_icon icon_about_globe">
                                </div>
                                <div class=" ">
                                    <span class="bg_index__text">Представлены в России, Беларуси и Казахстане</span>
                                </div>
                            </div>
                        </div>
                        <img src="img/map_services.png" width="100%" alt="map alidi" class="margin_bottom_x2">
                        <a href="https://alidi-btl.ru/" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">Перейти на сайт ALIDI BTL</a>
                    </div>
                    <div class="distribution">
                        <span class="d-inline-flex style_16_24 fw-bold margin_bottom_x2">АЛИДИ – одна из крупнейших дистрибьюторских компаний в России. Линейка продукции компании включает в себя бытовую химию, товары личной гигиены, продукты питания, детское питание, корма и товары для животных. Сегодня АЛИДИ имеет широкую дистрибуционную сеть, обслуживая более 120 000 торговых точек. Среди них – крупные торговые сети, оптовые компании, частные предприниматели.</span>
                    </div>
                    <div class="logistic">
                     <span class="d-inline-flex style_16_24 fw-bold margin_bottom_x2">Компания АЛИДИ – один из ведущих российских логистических операторов, предлагающая своим партнерам полный комплекс 3PL услуг: складскую обработку, ответственное хранение и доставку товаров.
<br><br>
В настоящее время АЛИДИ оказывает логистические услуги в Москве, Санкт-Петербурге, Нижнем Новгороде на складских терминалах класса А, а также в регионах России. Общая площадь складских помещений АЛИДИ превышает 180 000 м2.
<br><br>
Складские комплексы компании оборудованы по международным стандартам и оснащены современными информационными системами: ERP система J.D.Edwards, WMS Manhattan.</span>
                        <a href="#" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">На сайт АЛИДИ Логистика</a>
                    </div>
                    <div class="horeca">
                    <span class="d-inline-block style_16_24 fw-bolder margin_bottom_x2">ALIDI RPOF — Поставщик продуктов питания для сегмента HoReCa в Санкт-Петербурге, Ленинградской области, Великом Новгороде, Пскове и Петрозаводске.
<br><br>
С 2020 года ALIDI RPOF открыла доставку для физических лиц на дом, продуктов питания и непродовольственных товаров по оптовым ценам.
<br><br>
Ключевое направление в компании АЛИДИ для HoReCa стартовало в 2016 году. На сегодняшний день, у нас более 1400 активных клиентов.
<br><br>
Мы охватили все направления данного сегмента:
Сетевые рестораны, бары, пиццерии, кафе, пекарни, производства, кулинарии, кейтеринг и службы доставки.
<br><br>
Ежегодный прирост новых клиентов составляет 20%.
Объем продаж за 2019 год вырос на 55%.
Эксклюзивные контракты с мировыми производителями и брендами. Возможность создания эксклюзивных рецептур по потребностям клиента.
В ассортименте более 3000 товаров от 80 производителей, таких как Unilever Food Solutions, Cargill, Solpro, Чудское озеро, Heinz, Sen Soy, Лимак и многие другие.<br>
АЛИДИ является одним из основных дистрибьюторов компании Nestle и подразделения Nestle Waters. Как поставщик, предлагаем полный ассортимент торговых марок Perrier, Vittel, Acqua Panna и S.Pellegrino.</span>
                        <span class="d-inline-block style_text_24_36 fw-bold margin_bottom_x2">Складская логистика</span>
                        <div class="page-layout__content__grid_layout margin_bottom align-self-center margin_bottom_x2">
                            <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
                                <div class="bg_index__bg_icon icon_table margin_bottom">
                                </div>
                                <div class="padding_bottom ">
                                    <span class="bg_index__text">Площадь склада 13 000 кв.м.</span>
                                </div>
                            </div>
                            <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
                                <div class="bg_index__bg_icon icon_temperature_plus margin_bottom">
                                </div>
                                <div class="padding_bottom">
                                    <span class="bg_index__text">Сухая отапливаемая зона (до +25°С)</span>
                                </div>
                            </div>
                            <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
                                <div class="bg_index__bg_icon icon_temperature_minus margin_bottom">
                                </div>
                                <div class="padding_bottom">
                                    <span class="bg_index__text">Холодильная камера (от +2°C до +6°С)</span>
                                </div>
                            </div>
                            <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
                                <div class="bg_index__bg_icon icon_frige margin_bottom">
                                </div>
                                <div class="padding_bottom">
                                    <span class="bg_index__text">Морозильная камера (до –21°С)</span>
                                </div>
                            </div>
                            <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
                                <div class="bg_index__bg_icon icon_conditioner margin_bottom">
                                </div>
                                <div class="padding_bottom">
                                    <span class="bg_index__text">Кондиционированная зона для хранения шоколада (от 15°С до +21°С)</span>
                                </div>
                            </div>
                            <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
                                <div class="bg_index__bg_icon icon_sertif margin_bottom">
                                </div>
                                <div class="padding_bottom">
                                    <span class="bg_index__text">Склад соответствует международным стандартам и имеет сертификат HACCP</span>
                                </div>
                            </div>
                        </div>
                        <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Транспортная логистика</span>
                        <span class="d-inline-block style_16_24 margin_bottom_x2">Зона доставки распространяется по всему Санкт-Петербургу и Ленинградской области.
Помимо этого, мы доставляем продукты в республику Карелия, Великий Новгород и Псковскую область.
<br><br>
Собственный парк новых автомобилей с температурным мультирежимом, для перевозки охлажденной и замороженной продукции.
Бесплатная доставка на следующий день.</span>
                        <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Интернет-портал</span>
                        <span class="d-inline-block style_16_24 margin_bottom_x2">Интернет - портал с полным каталогом товаров, где вы можете увидеть фото товара, ознакомиться с составом, сроками и условиями хранения.
<br><br>
После регистрации на сайте, клиент сможет увидеть в личном кабинете:<br>
- Какой товар, когда и по какой цене он брал<br>
- Загрузить матрицу товаров с согласованными ценами<br>
- В один клик выгрузить все сертификаты к поставке<br>
- Ознакомиться с финансами и кредитным лимитом<br>
- Найти и выгрузить любую накладную<br>
<br><br>
Мы принимаем заказы через сайт, торгового представителя, по почте или телефону, в любом удобном формате.
<br><br>
Работаем в системе EDI.</span>
                        <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Шеф-повар и кухня-студия</span>
                        <span class="d-inline-block style_16_24 margin_bottom_x2">Для лучшего взаимопонимания мы создали специальную кухню — студию с новым современным оборудованием и большим экраном. Как и на любой профессиональной кухне, на нашей тоже есть свой шеф-повар.
<br><br>
Все продукты, появляющиеся в ассортименте АЛИДИ ПРОФ попадают на проработку к шеф-повару, где он тщательно проводит над ними эксперименты, готовит разнообразные блюда и проверяет по всем органолептическим показателям.
<br><br>
Каждую неделю шеф-повар приглашает на кухню-студию весь отдел направления HoReCа, где рассказывает про тонкости работы кухни в ресторанах, барах и гостиницах. Проводит мастер-класс по приготовлению популярных блюд из позиций нашего ассортимента и подробно рассказывает
о них.
<br><br>
Шеф-повар совместно с торговыми представителями выезжает на встречи с шеф-поварами заведений общественного питания, чтобы поучаствовать в проработке новых позиций и помочь в составлении меню из продуктов нашего ассортимента.
<br><br>
Если вы являетесь нашим клиентом и хотите проработать продукты совместно с нашим шеф-поваром, вам достаточно сообщить об этом вашему менеджеру или торговому представителю для уточнения проработки и согласования удобного времени.
<br><br>
Если вы еще не являетесь клиентом АЛИДИ, регистрируйтесь на сайте alidi.ru или позвоните<br>
по номеру 8-800-775-75-00.</span>
                    </div>
                </div>

            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

