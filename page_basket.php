<div>
    <div class="d-flex d-xl-none flex-row justify-content-between margin_bottom">
        <span class="fw-bold">2 товара</span>
        <span class="fw-bold">306 713,60 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
        <span class="active_tab style_padding_10 style_width50 style_border_radius_40 text-center">Товары</span>
        <span class="d-inline-block icon_arrow_right_bg_white"></span>
        <span class="style_accordion style_padding_10 style_width50 style_border_radius_40 text-center">Оформление</span>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
        <a href="item.php" class="position-relative">
            <img class="margin_bottom width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
            <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
        </a>
        <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
            <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
            </div>
        </div>
        <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
            <div class="margin_bottom">
                <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                    <option selected>Заказ поштучно</option>
                    <option value="1">Заказ коробками (12 шт)</option>
                    <option value="2">Заказ блоками (36 шт)</option>
                </select>
            </div>
            <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
            </div>
            <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
        <a href="item.php" class="position-relative">
            <img class="margin_bottom width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
            <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
        </a>
        <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
            <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
            </div>
        </div>
        <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
            <div class="margin_bottom">
                <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                    <option selected>Заказ поштучно</option>
                    <option value="1">Заказ коробками (12 шт)</option>
                    <option value="2">Заказ блоками (36 шт)</option>
                </select>
            </div>
            <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
            </div>
            <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
        </div>
    </div>
</div>

<div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom height_max">
    <div class="d-flex flex-row justify-content-between fw-bold style_padding_bottom_10">
        <span>Сумма</span>
        <span class="text-decoration-line-through">362 556,00 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_padding_bottom_10">
        <span>2 товара</span>
        <span class="">362 556,00 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_padding_bottom_10">
        <span>Доставка</span>
        <span class="">бесплатно</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_border_bottom  padding_bottom">
        <span>Скидка</span>
        <span class="">15%</span>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_bottom__top_20 style_border_bottom margin_bottom ">
        <label class="fw-bold d-flex flex-row justify-content-start" for="bonus">
            <input type="checkbox" name="bonus" class="form-check-input margin_right" checked>
            <span>Списать <br class="d-none d-xl-block">1 459 баллов</span>
        </label>
    </div>
    <div class="fw-bold d-flex flex-row justify-content-between margin_bottom">
        <span>Итого </span>
        <span>306 713,60 ₽</span>
    </div>
    <a href="checkout.php" class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">Оформить заказ</a>
</div>
