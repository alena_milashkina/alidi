<div>
    <div class="d-flex d-xl-none flex-row justify-content-between margin_bottom">
        <span class="fw-bold">2 товара</span>
        <span class="fw-bold">306 713,60 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
        <span class="style_accordion  style_padding_10 style_width50 style_border_radius_40 text-center">Товары</span>
        <span class="d-inline-block icon_arrow_right_bg_white"></span>
        <span class="active_tab style_padding_10 style_width50 style_border_radius_40 text-center">Оформление</span>
    </div>
    <div class="d-xl-flex flex-xl-row justify-content-xl-between">
        <div class="d-xl-flex flex-xl-column justify-content-start width_40_desk">
            <span class="d-inline-block fw-bold style_text_20_30 margin_bottom style_text_24_36_desk">Способ получения</span>
            <select class="form-select style_padding_18_20 style_border_transparent fw-bold style_accordion style_border_radius_40 margin_bottom" aria-label=".form-select-lg">
                <option selected>Доставка курьером</option>
                <option value="1">Доставка роботом</option>
                <option value="2">Самовывоз завтра</option>
            </select>
        </div>
        <div class="d-xl-flex flex-xl-column justify-content-between width_40_desk">
            <span class="d-inline-block fw-bold style_text_20_30 margin_bottom style_text_24_36_desk">Дата доставки</span>
            <div class="d-flex flex-row justify-content-start grow_02 style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
                <img src="img/CalendarBlank.svg" width="20" alt="calendar" class="margin_right">
                <input class="style_16_24 fw-bold input_date_rang width_200_mob cursor_pointer me-auto datepicker4" placeholder="Выберите дату" type="text" id="datepicker4"/>
                <img src="img/arrowDropBig.svg" width="20" alt="dropDown">
            </div>
        </div>
    </div>
    <span class="d-inline-block fw-bold style_text_20_30 margin_bottom style_text_24_36_desk">Адрес и получатель</span>
    <div class="d-flex flex-row justify-content-start style_border_lightgray style_border_radius_20 style_border_radius_40_only_desk margin_bottom position-relative">
        <div class="border_right_lightgray style_padding_left_right_10 style_padding_left_right_20_desk d-grid align-content-center">
            <input type="radio" checked name="address">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_left_right_10 margin_bottom position-relative">
            <span class="d-inline-block margin_top style_text_12_16 color_gray">Получатель</span>
            <span class="style_16_24 margin_bottom">Овечкин Александр Михайлович</span>
            <span class="style_text_12_16 color_gray">Адрес</span>
            <span class="style_16_24 fw-bold margin_bottom style_text_24_36_desk">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
            <span class="style_text_12_16 color_gray">Телефон</span>
            <span class="style_16_24 padding_bottom style_border_bottom border_bottom_transparent_desk color_black">+7 925 982-33-77</span>
            <div class="d-flex flex-row align-content-center justify-content-center style_padding_top_18 d-xl-none">
                <a href="refactor_all.php"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            </div>
        </div>
        <div class="d-none d-xl-flex flex-row align-content-center justify-content-center style_padding_top_18 position-absolute top-0 end-2">
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_refactor_all"><img class="cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
        </div>
    </div>
    <div class="d-flex flex-row justify-content-start style_border_lightgray style_border_radius_20 style_border_radius_40_only_desk margin_bottom_x2 position-relative">
        <div class="border_right_lightgray style_padding_left_right_10 style_padding_left_right_20_desk d-grid align-content-center">
            <input type="radio" name="address">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_left_right_10 margin_bottom position-relative">
            <span class="d-inline-block margin_top style_text_12_16 color_gray">Получатель</span>
            <span class="style_16_24 margin_bottom">Овечкин Александр Михайлович</span>
            <span class="style_text_12_16 color_gray">Адрес</span>
            <span class="style_16_24 fw-bold margin_bottom style_text_24_36_desk">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
            <span class="style_text_12_16 color_gray">Телефон</span>
            <span class="style_16_24 padding_bottom style_border_bottom border_bottom_transparent_desk color_black">+7 925 982-33-77</span>
            <div class="d-flex flex-row align-content-center justify-content-center style_padding_top_18 d-xl-none">
                <a href="refactor_all.php"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            </div>
        </div>
        <div class="d-none d-xl-flex flex-row align-content-center justify-content-center style_padding_top_18 position-absolute top-0 end-2">
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_refactor_all"><img class="cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
        </div>
    </div>
    <button data-bs-toggle="modal" data-bs-target="#modal_add_address" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 width_50_desk">Добавить адрес получателя</button><br>
    <!--ниже контейнер с селектом для юр лиц-->
    <div class="d-xl-flex flex-xl-column justify-content-between style_border_lightgray style_border_radius_20 style_border_radius_40_only_desk padding_30_desk border_transparent_mob margin_bottom">
        <select class="form-select style_padding_12_20 style_border_lightgray style_border_radius_40 margin_bottom" aria-label=".form-select-lg">
            <option selected>Санкт-Петербург, ул Верейская, д. 47, кв, 45 </option>
            <option value="1">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </option>
            <option value="2">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </option>
        </select>
        <div class="d-flex flex-column justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom border_transparent_desk padding_0_desk margin_bottom_0_desk">
            <span class="d-inline-block style_text_12_16 color_gray">Получатель</span>
            <span class="style_16_24 margin_bottom">Овечкин Александр Михайлович</span>
            <span class="style_text_12_16 color_gray">Телефон</span>
            <span class="style_16_24 color_black">+7 925 982-33-77</span>
        </div>
    </div>

    <span class="d-inline-block fw-bold style_text_20_30 margin_bottom style_text_24_36_desk">Способ оплаты</span>
    <select class="form-select style_padding_12_20 style_border_transparent fw-bold style_accordion style_border_radius_40 margin_bottom" aria-label=".form-select-lg">
        <option selected>Картой при получении</option>
        <option value="1">Картой при получении</option>
        <option value="2">Картой при получении</option>
    </select>
    <span class="d-inline-block fw-bold style_text_20_30 margin_bottom style_text_24_36_desk">Комментарий</span>
    <textarea class="style_gray_radius style_padding_18_20 style_input margin_bottom_x2 style_width100" placeholder="Например, уточнить время доставки" rows="6"></textarea>
</div>

<div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom height_max">
    <div class="d-flex flex-row justify-content-between fw-bold style_padding_bottom_10">
        <span>Сумма</span>
        <span class="text-decoration-line-through">362 556,00 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_padding_bottom_10">
        <span>2 товара</span>
        <span class="">362 556,00 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_padding_bottom_10">
        <span>Доставка</span>
        <span class="">бесплатно</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_border_bottom  padding_bottom">
        <span>Скидка</span>
        <span class="">15%</span>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_bottom__top_20 style_border_bottom margin_bottom ">
        <label class="fw-bold d-flex flex-row justify-content-start" for="bonus">
            <input type="checkbox" name="bonus" class="form-check-input margin_right" checked>
            <span>Списать <br class="d-none d-xl-block">1 459 баллов</span>
        </label>
    </div>
    <div class="fw-bold d-flex flex-row justify-content-between margin_bottom">
        <span>Итого </span>
        <span>306 713,60 ₽</span>
    </div>
    <a href="page_thanks.php" class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center margin_bottom">Оформить заказ</a>
    <span class="style_text_12_16">Оформляя заказ на нашем сайте, Вы даете согласие с условиями <b>пользовательского соглашения</b></span>
</div>
<!--ниже контейнер для юр лиц на десктопе он встанет на свое место если контейнер выше d-none-->
<!--<div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom height_max">
    <div class="d-flex flex-row justify-content-between fw-bold style_padding_bottom_10">
        <span>Сумма</span>
        <span class="text-decoration-line-through">362 556,00 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_padding_bottom_10">
        <span>2 товара</span>
        <span class="">362 556,00 ₽</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_padding_bottom_10">
        <span>Доставка</span>
        <span class="">бесплатно</span>
    </div>
    <div class="d-flex flex-row justify-content-between style_border_bottom  padding_bottom margin_bottom">
        <span>Скидка</span>
        <span class="">15%</span>
    </div>
    <a href="page_thanks.php" class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center margin_bottom">Оформить заказ</a>
    <span class="style_text_12_16">Оформляя заказ на нашем сайте, Вы даете согласие с условиями <b>пользовательского соглашения</b></span>
</div>-->