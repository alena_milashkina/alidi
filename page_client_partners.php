<div class="position-relative">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion tabs">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center active_tab">Клиенты</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk style_text_18_30_desk fw-bold text-center">Партнеры</span>
    </div>
    <div class="d-flex flex-row justify-content-between justify-content-xl-start align-items-center margin_bottom">
        <span class="d-inline-block style_padding_10 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer accordion_active margin_right_desk">Все клиенты</span>
        <span class="d-inline-block style_padding_10 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_right_desk">BTL</span>
        <span class="d-inline-block style_padding_10 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_right_desk">Дистрибуция</span>
        <span class="d-inline-block style_padding_10 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_right_desk">Логистика</span>
        <span class="d-inline-block style_padding_10 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_right_desk">HoReCa</span>
    </div>
    <div class="contents">
        <div class="clients">
            <div class="d-grid grid_layout_client margin_bottom_x2">
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo-5ka_200.svg" alt="пятёрочка" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoGlobus_160.svg" alt="globus" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoGippo_200.svg" alt="гиппо" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoAzVk_160.svg" alt="азбука вкуса" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoBilla_160.svg" alt="billa" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoEvroOpt_160.svg" alt="евроопт" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoMila_200.svg" alt="мила" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoFixPrice_200.svg" alt="fix price" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoVernii_180.svg" alt="верный" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoDetskiyMir_200.svg" alt="детский мир" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoMetro_160.svg" alt="metro" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoYlRad_200.svg" alt="улыбка радуги" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoOstrov_160.svg" alt="остров чистоты" width="100%" class="width_105_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoWildberries_200.svg" alt="wildberries" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoMaksi_200.svg" alt="макси" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoSvetophor_200.svg" alt="светофор" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoComus_200.svg" alt="комус" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoYaMarket_200.svg" alt="яндекс маркет" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoMagnum_200.svg" alt="magnum" width="100%" class="width_130_mob">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logoOzon_200.svg" alt="ozon" width="100%" class="width_130_mob">
                </div>
            </div>
        </div>
        <div class="partners">
            <div class="d-grid grid_layout_partners margin_bottom_x2">
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/6sotok.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/agro.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/astra.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/baby_line.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/bagi_big.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/bayer.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/beaphar.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/ceva.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/chipita.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/coty.jpg" alt="" width="100%" class="">
                </div>

                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/danone.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/duracell.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/ecoprom.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/fater.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/ferero.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/freshstep.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/haribo.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/hayat.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/helvet.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/husqvarna.jpg" alt="" width="100%" class="">
                </div>

                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/jacobs.png" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/konti.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/kurnos.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/kuza.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/lek.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/lotte.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/maccoffe.png" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/mars.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/mondelez.png" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/msd.jpg" alt="" width="100%" class="">
                </div>

                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/nestle.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/nestlewaters.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/p&g.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/philips.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/pogreb.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/pomidor.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/purina.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/rieber.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/rosel.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/samoilov.jpg" alt="" width="100%" class="">
                </div>

                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/upeco.jpg" alt="" width="100%" class="">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/partners/vania.jpg" alt="" width="100%" class="">
                </div>
            </div>
        </div>
    </div>
</div>