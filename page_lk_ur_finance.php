<div>
    <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
        <img src="img/iconUserRectangle.svg" width="20" alt="search" class="margin_right">
        <select class="form-select form-select-sm style_border_transparent input_date_rang fw-bold" aria-label=".form-select-lg example">
            <option selected>Все плательщики</option>
            <option value="1">плательщик 1</option>
            <option value="2">плательщик 2</option>
            <option value="3">плательщик 3</option>
            <option value="4">плательщик 4</option>
            <option value="5">плательщик 5</option>
        </select>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_radius_40_only_desk style_border_lightgray">
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Номер договора</span>
            <span class="fw-bold">87234687423</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Лимит кредита</span>
            <span class="fw-bold">0,00 ₽</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50 width_80_desk">Задолженность <br class="d-xl-none"><span class="style_text_12_16">(включая просроченную задолженность и задолженность по отгруженным заказам)</span></span>
            <span class="color_red fw-bold">59 753,79 ₽</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Задолженность по отгруженным заказам</span>
            <span class="color_red fw-bold">67 348,52 ₽</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Просроченная задолженность</span>
            <span class="color_red fw-bold">7 595,33 ₽</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Свободный лимит кредита</span>
            <span class="fw-bold">59 753,79 ₽</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10">
            <span class="style_16_24 style_width50 d-inline-block ">Условия отсрочки</span>
            <span class="fw-bold text-end">Наличными по факту</span>
        </div>
    </div>
    <span class="style_text_24_36 d-inline-block fw-bold style_padding_18_20">Операции</span>
    <div class="d-flex flex-column flex-xl-row justify-content-between">
        <div class="d-flex flex-row justify-content-start grow_02 style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
            <img src="img/CalendarBlank.svg" width="20" alt="calendar" class="margin_right">
            <input class="style_16_24 fw-bold input_date_rang width_200_mob cursor_pointer me-auto datepicker3" placeholder="Выберите дату" type="text" id="datepicker3"/>
            <img src="img/arrowDropBig.svg" width="20" alt="dropDown">
        </div>
        <button class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 width_260_desk style_padding_10">Выгрузить сверку в .xls</button>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_radius_40_only_desk style_border_lightgray">
        <div class="d-none d-xl-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom fw-bold">
            <span>Дата</span>
            <span>Тип операции</span>
            <span>Сумма</span>
            <span>Описание</span>
        </div>
        <div class="d-flex flex-row flex-xl-column justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <div class="d-flex flex-column flex-xl-row justify-content-between ">
                <span class="style_16_24 fw-bold">17.01.2021</span>
                <span>Оплата</span>
                <span class="style_16_24 d-none d-xl-inline-block">3 008,60 ₽</span>
                <span>2210392</span>
            </div>
            <span class="style_16_24 fw-bold d-xl-none">3 008,60 ₽</span>
        </div>
        <div class="d-flex flex-row flex-xl-column justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <div class="d-flex flex-column flex-xl-row justify-content-between ">
                <span class="style_16_24 fw-bold">17.01.2021</span>
                <span>Оплата</span>
                <span class="style_16_24 d-none d-xl-inline-block">3 008,60 ₽</span>
                <span>2210392</span>
            </div>
            <span class="style_16_24 fw-bold d-xl-none">3 008,60 ₽</span>
        </div>
        <div class="d-flex flex-row flex-xl-column justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <div class="d-flex flex-column flex-xl-row justify-content-between ">
                <span class="style_16_24 fw-bold">17.01.2021</span>
                <span>Оплата</span>
                <span class="style_16_24 d-none d-xl-inline-block">3 008,60 ₽</span>
                <span>2210392</span>
            </div>
            <span class="style_16_24 fw-bold d-xl-none">3 008,60 ₽</span>
        </div>
        <div class="d-flex flex-row flex-xl-column justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <div class="d-flex flex-column flex-xl-row justify-content-between ">
                <span class="style_16_24 fw-bold">17.01.2021</span>
                <span>Оплата</span>
                <span class="style_16_24 d-none d-xl-inline-block">3 008,60 ₽</span>
                <span>2210392</span>
            </div>
            <span class="style_16_24 fw-bold d-xl-none">3 008,60 ₽</span>
        </div>
    </div>
</div>
