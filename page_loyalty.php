<div class="page-layout__content d-flex flex-column justify-content-between">
    <div class="d-block d-xl-none position-relative margin_bottom_x2">
        <div class="position-absolute position_text_banner_mob">
            <span class="color_white style_text_24_36 fw-bold">Дарим баллы<br> за каждую покупку</span><br>
            <span class="color_white fw-bolder">Покупай больше — плати меньше!</span>
        </div>
        <button class="style_border_transparent color_blue bg_white fw-bold style_padding_10_20 width_160_mob style_text_12_16 style_border_radius_15 position_button_mob_banner position-absolute" data-bs-target="#entrance" data-bs-toggle="modal">Стать участником</button>
        <img class="style_border_radius_20" src="img/banner_loyalty_mob.png" width="100%">
    </div>
    <div class="d-none d-xl-block position-relative margin_bottom_x2">
        <img src="img/banner_loyalty_desk.png" width="100%">
        <button class="d-none d-xl-block style_border_transparent color_blue bg_white fw-bold style_border_radius_40 style_padding_18_20 width_315_mob width_360_desk button_slider_index position-absolute" data-bs-target="#entrance" data-bs-toggle="modal">Стать участником</button>
    </div>
    <span class="style_text_24_36 style_text_40_50_desk fw-bold d-inline-block margin_bottom">Копите баллы!</span>
    <div class="d-flex flex-column flex-xl-row justify-content-between align-items-start bg_grey_without_dots style_border_radius_20 style_border_radius_40_only_desk padding_40_desk margin_bottom_x2">
        <div class="style_padding_18_20 d-flex flex-column">
            <span class="d-inline-block margin_bottom style_text_20_30 fw-bold">Как получить:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> Покупайте товары на сайте<br class="d-none d-xl-block"> и получайте баллы за заказы</span>
            </div>
        </div>
        <img class="d-none d-xl-block" src="img/image_loyalty_how_desk.png" width="560" alt="img Как получить баллы">
        <img class="margin_bottom_x2 d-block d-xl-none" src="img/image_loyalty_how.png" width="100%" alt="img Как получить баллы">
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between align-items-start bg_grey_without_dots style_border_radius_20 style_border_radius_40_only_desk padding_40_desk margin_bottom_x2">
        <div class="style_padding_18_20">
            <span class="d-inline-block margin_bottom style_text_20_30 fw-bold">Как потратить:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> Баллами можно оплатить часть заказа <br class="d-none d-xl-block"> на сайте и в приложении</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> Баллы не сгорают</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> Количество баллов видно<br class="d-none d-xl-block"> в Вашем личном кабинете</span>
            </div>
        </div>
        <img class="d-none d-xl-block" src="img/image_loyalty_spent_desk.png" width="560" alt="img Как потратить баллы">
        <img class="margin_bottom_x2 d-block d-xl-none" src="img/image_loyalty_spent.png" width="100%" alt="img Как потратить баллы">
    </div>
    <span class="style_text_24_36 style_text_40_50_desk fw-bold d-inline-block margin_bottom_x2">Заказывайте больше — покупайте дешевле!</span>
    <div class="owl-carousel owl3 margin_bottom_x2 d-xl-none">
        <div class="d-flex flex-column justify-content-between align-items-center bg_fox style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_fox style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Лиса</span>
            <span>Закупок в месяц</span>
            <span>до 120 000 ₽</span>
        </div>
        <div class="d-flex flex-column justify-content-between align-items-center bg_wolf style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_wolf style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Волк</span>
            <span>Закупок в месяц</span>
            <span>от 120 000 ₽ до 240 000 ₽</span>
        </div>
        <div class="d-flex flex-column justify-content-between align-items-center bg_bear style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_bear style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Медведь</span>
            <span>Закупок в месяц</span>
            <span> от 240 000 ₽ до 600 000 ₽</span>
        </div>
        <div class="d-flex flex-column justify-content-between align-items-center bg_elephant style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_elephant style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Слон</span>
            <span>Закупок в месяц</span>
            <span>от 600 000 ₽</span>
        </div>
    </div>
    <div class="d-none d-xl-grid grid_layout_4_1 margin_bottom_x2">
        <div class="d-flex flex-column justify-content-between align-items-center bg_fox style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_fox style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Лиса</span>
            <span>Закупок в месяц</span>
            <span>до 120 000 ₽</span>
        </div>
        <div class="d-flex flex-column justify-content-between align-items-center bg_wolf style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_wolf style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Волк</span>
            <span>Закупок в месяц</span>
            <span>от 120 000 ₽ до 240 000 ₽</span>
        </div>
        <div class="d-flex flex-column justify-content-between align-items-center bg_bear style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_bear style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Медведь</span>
            <span>Закупок в месяц</span>
            <span> от 240 000 ₽ до 600 000 ₽</span>
        </div>
        <div class="d-flex flex-column justify-content-between align-items-center bg_elephant style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20">
            <div class="icon_elephant style_border_radius_20 margin_bottom margin_top_x2"></div>
            <span class="style_text_24_36 fw-bold d-inline-block margin_bottom">Слон</span>
            <span>Закупок в месяц</span>
            <span>от 600 000 ₽</span>
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between align-items-start bg_fox style_border_radius_20 style_border_radius_40_only_desk padding_40_desk margin_bottom">
        <div class="style_padding_18_20">
            <span class="d-inline-block margin_bottom style_text_20_30 fw-bold">Лиса:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Доступна скидка 10% на первый заказ <br class="d-none d-xl-block"> и 7% на второй заказ</span>
            </div>
        </div>
        <img class="d-none d-xl-block" src="img/image_loyalty_fox_desk.png" width="567" alt="fox img loyalty">
        <img class="margin_bottom_x2 d-xl-none" src="img/image_loyalty_fox.png" width="100%" alt="fox img loyalty">
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-start align-items-start bg_bear style_border_radius_20 style_border_radius_40_only_desk padding_40_desk margin_bottom">
        <div class="style_padding_18_20">
            <span class="d-inline-block margin_bottom style_text_20_30 fw-bold">Волк и Медведь:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Укажите при регистрации Ваше направление <br class="d-none d-xl-block"> бизнеса и объем закупок и мы предоставим Вам <br class="d-none d-xl-block"> индивидуальные скидки до 10% на самые <br class="d-none d-xl-block"> востребованные товары.</span>
            </div>
        </div>
        <img class="d-none d-xl-block" src="img/image_loyalty_wolf_desk.png" width="440" alt="wolf and bear img loyalty" style="margin-left: 9.3rem;">
        <img class="margin_bottom_x2 d-xl-none" src="img/image_loyalty_wolf.png" width="100%" alt="wolf and bear img loyalty">
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between align-items-start bg_elephant_pic style_border_radius_20 style_border_radius_40_only_desk padding_40_desk margin_bottom_x2">
        <div class="style_padding_18_20">
            <span class="d-inline-block margin_bottom style_text_20_30 fw-bold">Слон:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Наш менеджер свяжется с Вами <br class="d-none d-xl-block"> и предоставим Вам индивидуальные цены</span>
            </div>
        </div>
        <!--<img class="d-none d-xl-block" src="img/image_loyalty_eleph_desk.png" width="500" alt="elephant img loyalty">-->
        <!--<img class="margin_bottom_x2 d-xl-none" src="img/image_loyalty_eleph.png" width="100%" alt="elephant img loyalty">-->
    </div>
    <div class="d-flex flex-column justify-content-center align-items-center style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20 padding_40_desk bg_blue_plus margin_bottom_x2">
        <span class="d-inline-block margin_bottom color_white style_text_20_30 style_text_24_36_desk fw-bold">Хотите получать скидки?</span>
        <button class="style_border_transparent color_blue bg_white fw-bold style_border_radius_40 style_padding_18_20 width_315_mob width_360_desk" data-bs-target="#entrance" data-bs-toggle="modal">Регистрация</button>
    </div>
    <span class="style_text_24_36 style_text_40_50_desk fw-bold d-inline-block margin_bottom_x2">Вопросы и ответы:</span>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40_only_desk style_padding_15_20 padding_30_desk cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Как стать участником программы лояльности?</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top fw-bolder">
                Вы автоматически становитесь участником системы лояльности после регистрации на сайте
            </span>
        </div>
    </div>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40_only_desk style_padding_15_20 padding_30_desk cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Сколько стоит балл?</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top fw-bolder">
                Вы автоматически становитесь участником системы лояльности после регистрации на сайте
            </span>
        </div>
    </div>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40_only_desk style_padding_15_20 padding_30_desk cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Как накопить баллы?</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top fw-bolder">
                Вы автоматически становитесь участником системы лояльности после регистрации на сайте
            </span>
        </div>
    </div>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40_only_desk style_padding_15_20 padding_30_desk cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Мои баллы сгорают?</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top fw-bolder">
                Вы автоматически становитесь участником системы лояльности после регистрации на сайте
            </span>
        </div>
    </div>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40_only_desk style_padding_15_20 padding_30_desk cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">В какой момент мне будут начислены баллы?</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top fw-bolder">
                Вы автоматически становитесь участником системы лояльности после регистрации на сайте
            </span>
        </div>
    </div>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40_only_desk style_padding_15_20 padding_30_desk cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">На что я могу потратить свои баллы?</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top fw-bolder">
                Вы автоматически становитесь участником системы лояльности после регистрации на сайте
            </span>
        </div>
    </div>
    <span class="style_text_24_36 style_text_40_50_desk fw-bold d-inline-block margin_bottom_x2 margin_top_x2">Продукты питания для профессионалов!</span>
    <div class="bg_blue bg_index__padding section_style height_150 d-flex flex-column justify-content-between margin_top_bottom_10 d-xl-none">
        <div class="bg_index__bg_icon icon_timer_white">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__heading margin_bottom">Заказы, сделанные до 18:00, доставляем на следующий день</span>
        </div>
    </div>
    <div class="page-layout__content__grid_layout page-layout__content__grid_layout_catalog_desk margin_bottom align-self-center margin_bottom_x2">
        <div class="bg_blue bg_index__padding section_style height_260_desk flex-column justify-content-between margin_top_bottom_10 style_border_radius_40_only_desk d-none d-xl-flex place_grid_truck">
            <div class="bg_index__bg_icon icon_timer_white margin_bottom_x2">
            </div>
            <div class="d-inline-block">
                <span class="bg_index__heading">Заказы, сделанные до 18:00,<br> доставляем на следующий день</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_docs">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Полный пакет документов для юридических и физических лиц</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_truck margin_bottom_x2">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Бесплатная доставка по Санкт-Петербургу и Ленобласти</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_receipt margin_bottom">
            </div>
            <div class="padding_bottom ">
                <span class="bg_index__text">Минимальная сумма заказа от 3000 рублей</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_shopping margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Более 3300 товаров  для профессионалов</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_target margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Индивидуальное ценообразование</span>
            </div>
        </div>
    </div>
    <span class="style_text_20_30 style_text_24_36_desk fw-bold d-inline-block margin_bottom_x2">Полные правила по системе лояльности можно посмотреть <span class="color_blue text-decoration-underline">здесь</span></span>
</div>