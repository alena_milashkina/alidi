<div class="width_55_desk">
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray">
        <label class="" for="notification_no-call">
            <input type="checkbox" name="notification_no-call" class="form-check-input margin_right" checked>
            Не звонить по заказу
        </label>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray">
        <label class="" for="notification_sms_only">
            <input type="checkbox" name="notification_sms_only" class="form-check-input margin_right">
            Получать рассылку SMS
        </label>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray">
        <label class="" for="notification_email_only">
            <input type="checkbox" name="notification_email_only" class="form-check-input margin_right" checked>
            Получать рассылку по email
        </label>
    </div>
</div>
