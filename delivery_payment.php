<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Доставка и оплата</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="">
            <div class="page-layout__content">
                <img class="d-none d-xl-block" src="img/img_delivery_desk.png" width="100%" alt="зона доставки">
                <img class="d-block d-xl-none" src="img/img_delivery_mob.png" width="100%" alt="зона доставки">
                <div class="bg_blue style_padding_18_20 padding_30_desk style_border_radius_20 style_border_radius_40_only_desk flex-column justify-content-between margin_top_x2 margin_bottom_x2 style_border_radius_40_only_desk d-flex place_grid_truck">
                    <span class="color_white"><span class="style_text_20_24">Оформить заказ на нашем портале Вы можете круглосуточно.</span>
<br><br>
С 9.00 до 18.00 по будням, с Вами свяжутся наши операторы, для подтверждения заказа.
<br><br>
Минимальная сумма заказа 2000 рублей по Санкт-Петербургу (В пределах КАД + Девяткино, Мурино, Кузьмолово, Токсово, Агалатово).
<br><br>
Минимальная сумма заказа для Санкт-Петербурга и Ленинградской области (За пределами КАД, кроме упомянутых выше) 3000 рублей.</span>
                </div>
            <span class="d-inline-block fw-bold style_text_24_36 style_text_40_50_desk margin_top margin_bottom_x2">Доставка</span>
            <div class="bg_grey style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20 padding_30_desk margin_bottom_x2">
                <span>
                    Доставка по Санкт-Петербургу и Ленинградской области (в пределах 30 км от КАД) входит в стоимость заказа.
<br><br>
Заказ, оформленный до 18.00, будет доставлен на следующий день.
<br><br>
Заказ, оформленный после 18.00, будет доставлен через день.
<br><br>
Доставка осуществляется с каждый день (кроме воскресенья) с 9.00 до 20.00.
<br><br>
Доставка в удаленные города Ленинградской области (более 30 км от КАД) и по Северо-Западному Федеральному округу за пределами Ленинградской области осуществляется по графику доставки, уточнить который вы можете по номеру 8-800-775-4-335.
<br><br>
При доставке в Северо-Западный Федеральный округ, цены могут отличаться от указанных на сайте. Уточнить цены можно у вашего Торгового Представителя или по телефону 8 800-775-4-335, звонок бесплатный.
                </span>
            </div>
            <span class="d-inline-block fw-bold style_text_24_36 style_text_40_50_desk margin_top margin_bottom_x2">Самовывоз</span>
            <div class="bg_grey style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20 padding_30_desk margin_bottom_x2">
                <span>
                    Самовывоз осуществляется на следующий день, после оформления заказа.
<br><br>
Забрать заказ можно на нашем складе по адресу:
<br><br>
Ленинградская область, Всеволожский р-он, ГП Кузьмоловский ул. Заводская д.3 к.361А с понедельника по субботу с 9.00 до 18.00, предварительно необходимо позвонить по телефону 8-800-775-4-335 и заказать пропуск на территорию склада.
                </span>
            </div>
            <span class="d-inline-block fw-bold style_text_24_36 style_text_40_50_desk margin_top margin_bottom_x2">Оплата</span>
            <div class="bg_grey style_border_radius_20 style_border_radius_40_only_desk style_padding_18_20 padding_30_desk margin_bottom_x2">
                <span>
                    Оплата осуществляется путем внесения наличных денежных средств в кассу, при самовывозе.
<br><br>
Оплата наличными денежными средствами курьеру, в случае доставки.
<br><br>
Оплата по банковской карте через терминал курьеру, в случае доставки.
<br><br>
Предоплата через безналичный расчет, осуществляется путем перечисления денежных средств на счет поставщика (только для юридических лиц).
<br><br>
Возможно предоставление отсрочки платежа. Условия по предоставлению отсрочки рассматриваются в индивидуальном порядке, после заключения договора поставки. Чтобы заключить договор, позвоните то телефону 8-800-775-4-335.
<br><br>
В случае самовывоза, для вашего удобства, на территории склада есть банкомат Сбербанк.
<br><br>
Все предложения и замечания по работе интернет-магазина вы можете направить нам на почту contact_center_gd@alidi.ru или по телефону 8-800-775-4-335.
                </span>
            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

