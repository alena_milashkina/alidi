<div class="position-relative ">
    <!--<div class="horizontal-scroll-wrapper tab_wrapper style_blue_radious style_accordion tabs">
        <span class="d-inline-block style_border_radius_40 style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">Пресс-центр</span>
        <span class="d-inline-block style_border_radius_40 style_padding_bottom__top_20 style_text_12_16 fw-bold text-center">Блог</span>
        <span class="d-inline-block style_border_radius_40 style_padding_bottom__top_20 style_text_12_16 fw-bold text-center">Отзывы</span>
    </div>-->
<!--    <div class=" margin_bottom style_blue_radious style_accordion margin_left_right tabs">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">Пресс-центр</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Блог</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Отзывы</span>
    </div>-->
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right tabs">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">Пресс-центр</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Блог</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Отзывы</span>
    </div>
    <div class="contents position-relative">
        <div class="press_center">
            <div class="horizontal-scroll-wrapper squares ">
                <span class="fw-bold">2021</span>
                <span class="">2020</span>
                <span>2019</span>
                <span>2018</span>
                <span>2017</span>
                <span>2016</span>
                <span>2015</span>
                <span>2014</span>
                <span>2013</span>
                <span>2012</span>
                <span>2011</span>
                <span>2010</span>
                <span>2009</span>
                <span>2008</span>
            </div>
            <div class="d-xl-grid grid_layout_press margin_bottom_x2 margin_top_x4">
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Итоги года 2020 для ресторанного бизнеса</span>
                    <span class="style_16_24 fw-bold ">30 декабря 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Новые ограничения для предприятий общественного питания в Санкт-Петербурге</span>
                    <span class="style_16_24 fw-bold ">23 ноября 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Новые ограничения для предприятий общественного питания в Санкт-Петербурге</span>
                    <span class="style_16_24 fw-bold ">30 декабря 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Итоги года 2020 для ресторанного бизнеса</span>
                    <span class="style_16_24 fw-bold ">30 декабря 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Акция недели: Продукты для приготовления суши и роллов</span>
                    <span class="style_16_24 fw-bold ">23 ноября 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Новые ограничения для предприятий общественного питания в Санкт-Петербурге</span>
                    <span class="style_16_24 fw-bold ">30 декабря 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Итоги года 2020 для ресторанного бизнеса</span>
                    <span class="style_16_24 fw-bold ">30 декабря 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Акция недели: Продукты для приготовления суши и роллов</span>
                    <span class="style_16_24 fw-bold ">23 ноября 2020</span>
                </a>
                <a href="press_center__single_news.php" class="bg_grey style_padding_18_20 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10 height_150">
                    <span class="d-inline-block style_16_24 margin_bottom">Новые ограничения для предприятий общественного питания в Санкт-Петербурге</span>
                    <span class="style_16_24 fw-bold ">30 декабря 2020</span>
                </a>
            </div>
        </div>
        <div class="blog">
            <div class="d-flex flex-row justify-content-start align-items-center flex-wrap">
                <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer accordion_active margin_bottom">Все категории</span>
                <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Видео рецепты</span>
                <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Здоровое питание</span>
                <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Стрид-фуд</span>
                <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Выпечка</span>
            </div>
            <a href="blog__single.php" class="bg_grey style_padding_10 padding_30_desk style_border_radius_40_only_desk style_border_radius_20 d-flex flex-column flex-xl-row justify-content-between margin_top_bottom_10 ">
                <img class="margin_bottom_x2 width_530_desk padding__right_40_desk" src="img/blog1.png" width="100%" alt="blog_promo">
                <div class="padding_bottom">
                    <span class="d-inline-block style_16_24 fw-bold margin_bottom width_50_desk">Профессиональные средства для уборки от Kiilto</span>
                </div>
            </a>
            <a href="blog__single.php" class="bg_grey style_padding_10 padding_30_desk style_border_radius_40_only_desk style_border_radius_20 d-flex flex-column flex-xl-row justify-content-between margin_top_bottom_10 ">
                <img class="margin_bottom_x2 width_530_desk padding__right_40_desk" src="img/blog2.png" width="100%" alt="blog_promo">
                <div class="padding_bottom">
                    <span class="d-inline-block style_16_24 fw-bold margin_bottom width_50_desk">Профессиональные средства для уборки от Kiilto</span>
                </div>
            </a>
            <a href="blog__single.php" class="bg_grey style_padding_10 padding_30_desk style_border_radius_40_only_desk style_border_radius_20 d-flex flex-column flex-xl-row justify-content-between margin_top_bottom_10 ">
                <img class="margin_bottom_x2 width_530_desk padding__right_40_desk" src="img/blog3.png" width="100%" alt="blog_promo">
                <div class="padding_bottom">
                    <span class="d-inline-block style_16_24 fw-bold margin_bottom width_50_desk">Профессиональные средства для уборки от Kiilto</span>
                </div>
            </a>
            <a href="blog__single.php" class="bg_grey style_padding_10 padding_30_desk style_border_radius_40_only_desk style_border_radius_20 d-flex flex-column flex-xl-row justify-content-between margin_top_bottom_10 ">
                <img class="margin_bottom_x2 width_530_desk padding__right_40_desk" src="img/blog4.png" width="100%" alt="blog_promo">
                <div class="padding_bottom">
                    <span class="d-inline-block style_16_24 fw-bold margin_bottom width_50_desk">Профессиональные средства для уборки от Kiilto</span>
                </div>
            </a>
            <nav aria-label="Page navigation-blog">
                <ul class="pagination justify-content-center">
                    <li class="d-grid page-item disabled margin_right">
                        <a class=" icon_arrow_left_small page-link" href="#" tabindex="-1" aria-disabled="true"></a>
                    </li>
                    <li class="d-grid page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="page-link" href="#">...</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="page-link" href="#">23</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="icon_arrow_right_small page-link" href="#"></a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="reviews">
            <div class="page-layout__content__desk layout_grid_1col">
                <div class="customNavigation position-absolute end-0 start-100"></div>
                <div class="owl-carousel owl7 margin_top_x4">
                    <div class="d-grid align-content-center justify-content-center style_border_radius_40 style_border_lightgray margin_bottom height_150">
                        <img src="img/logo_clients/selgros.svg" alt="selgros" width="96" class="">
                    </div>
                    <div class="d-grid align-content-center justify-content-center style_border_radius_40 style_border_lightgray margin_bottom height_150">
                        <img src="img/logo_clients/upeco.svg" alt="upeco" width="96" class="">
                    </div>
                    <div class="d-grid align-content-center justify-content-center style_border_radius_40 style_border_lightgray margin_bottom height_150">
                        <img src="img/logo_clients/bagi.svg" alt="bagi" width="96" class="">
                    </div>
                    <div class="d-grid align-content-center justify-content-center style_border_radius_40 style_border_lightgray margin_bottom height_150">
                        <img src="img/logo_clients/konti.svg" alt="upeco" width="96" class="">
                    </div>
                    <div class="d-grid align-content-center justify-content-center style_border_radius_40 style_border_lightgray margin_bottom height_150">
                        <img src="img/logo_clients/baby_line.svg" alt="baby_line" width="96" class="">
                    </div>
                </div>
                <!--<div class="style_gray_radius style_border_radius_40_only_desk padding_30_desk margin_bottom_x2">
        <span class="fw-bolder">ООО «Зельгрос» выражает признательность и благодарность своему партнеру компании АЛИДИ
за качественное предоставление услуг в сфере логистики, профессионализм команды и гибкость в реакции на изменяющиеся запросы. Также хотим отметить открытость и прозрачность во взаимодействии
с нашим партнером.
<br><br>
Стабильное качество в выполнении логистических операций позволяет нашей компании сохранять необходимый уровень обслуживания в цепи поставок, снабжении торговой сети товарами, что позитивно отражается на наших отношениях с клиентами.
<br><br>
Выражаем уверенность в сохранении сложившихся дружественных отношений и надеемся на дальнейшее взаимовыгодное и плодотворное сотрудничество.</span>
                </div>
                <div class="style_gray_radius style_border_radius_40_only_desk padding_30_desk margin_bottom_x2">
        <span class="fw-bolder">АЛИДИ - один из важнейших стратегических партнеров компании UPECO. На сегодняшний день мы можем смело утверждать, что за годы сотрудничества команда АЛИДИ заработала безупречную репутацию надежного партнера, в основе работы которого клиентоориентированный сервис и стремление
к лидерству в области логистики и дистрибуции.
<br><br>
В нашем активе – ряд успешно реализованных совместных проектов. Благодаря сотрудничеству мы расширили дистрибуцию продуктов UPECO не только на территории России, но и за ее пределами. При этом хочется отметить, что строить эффективный
и взаимовыгодный бизнес с АЛИДИ легко: бизнес-процессы выстроены и регламентированы,
а вся команда – профессионалы высокого уровня.  Мы благодарим команду АЛИДИ за постоянное совершенствование и эффективную работу. От всей души желаем укрепления лидерских позиций, процветания и расширения границ присутствия и надеемся на дальнейшее плодотворное сотрудничество!</span>
                </div>
                <div class="style_gray_radius style_border_radius_40_only_desk padding_30_desk margin_bottom_x2">
        <span class="fw-bolder">ГК БАГИ и компания АЛИДИ тесно сотрудничают, начиная с 2013 года. Сегодня АЛИДИ поставляет нашу продукцию
в более чем 5000 торговых точек в семнадцати регионах России и в Белоруссии, стабильно обеспечивая высокий уровень клиентского сервиса.
<br><br>
Экспертиза АЛИДИ в области построения продаж сыграла значительную роль в расширении дистрибьюции БАГИ на федеральном уровне, налаживании взаимоотношений с крупнейшими торговыми сетями.
<br><br>
Я хочу поблагодарить сотрудников АЛИДИ
за профессионализм в повседневной работе, оперативное и всегда результативное решение самых нестандартных задач.
<br><br>
АЛИДИ – компания-лидер, стратегическое партнерство с которым, открывает новые перспективы развития на российском рынке. </span>
                </div>
                <div class="style_gray_radius style_border_radius_40_only_desk padding_30_desk margin_bottom_x2">
        <span class="fw-bolder">Работа компании АЛИДИ характеризуется отлично налаженными процессами работы с партнерами, взаимовыгодными условиями сотрудничества. Сотрудники компании в любых рабочих ситуациях проявляют себя как профессионалы своего дела, всегда открыты к диалогу и готовы оперативно решить любую, даже самую нестандартную, задачу.
<br><br>
Оперативность реагирования на все запросы, нацеленность на результат, выполнение
и перевыполнение планов, наращивание объемов продаж характеризуют компанию АЛИДИ как надежного партнера и компанию-лидера.
<br><br>
АЛИДИ, работающая на рынке дистрибуции более 20 лет, является экспертом в области построения продаж на региональном и федеральном уровнях, налаживания взаимоотношений с торговыми сетями. </span>
                </div>
                <div class="style_gray_radius style_border_radius_40_only_desk padding_30_desk margin_bottom_x2">
        <span class="fw-bolder">Компания АЛИДИ является нашим деловым партнёром и дистрибьютором торговых марок BabyLine, FeedBack и Липуня с 2012 года.
<br><br>
За время совместной работы, сотрудники компании АЛИДИ показали себя высокопрофессиональными специалистами в сфере дистрибуции, трудолюбивыми
и ответственными, способными оперативно принимать решения, умеющими креативно мыслить и идти в ногу со временем.
<br><br>
Следует отметить особое отношение компании АЛИДИ
к своим деловым партнёрам и клиентам. Честность и прозрачность в работе делают эти отношения доверительными, широчайшая клиентская база даёт бескрайние возможности развития, стабильность и оснащение современными технологиями дают уверенность в завтрашнем дне, что всегда приводит к наилучшим результатам.</span>
                </div>-->
            </div>
            <div class="page-layout__content_mob">
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
                    <img src="img/logo_clients/selgros.svg" alt="selgros" width="96" class="">
                </div>
                <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">ООО «Зельгрос» выражает признательность и благодарность своему партнеру компании АЛИДИ
за качественное предоставление услуг в сфере логистики, профессионализм команды и гибкость в реакции на изменяющиеся запросы. Также хотим отметить открытость и прозрачность во взаимодействии
с нашим партнером.
<br><br>
Стабильное качество в выполнении логистических операций позволяет нашей компании сохранять необходимый уровень обслуживания в цепи поставок, снабжении торговой сети товарами, что позитивно отражается на наших отношениях с клиентами.
<br><br>
Выражаем уверенность в сохранении сложившихся дружественных отношений и надеемся на дальнейшее взаимовыгодное и плодотворное сотрудничество.</span>
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
                    <img src="img/logo_clients/upeco.svg" alt="upeco" width="96" class="">
                </div>
                <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">АЛИДИ - один из важнейших стратегических партнеров компании UPECO. На сегодняшний день мы можем смело утверждать, что за годы сотрудничества команда АЛИДИ заработала безупречную репутацию надежного партнера, в основе работы которого клиентоориентированный сервис и стремление
к лидерству в области логистики и дистрибуции.
<br><br>
В нашем активе – ряд успешно реализованных совместных проектов. Благодаря сотрудничеству мы расширили дистрибуцию продуктов UPECO не только на территории России, но и за ее пределами. При этом хочется отметить, что строить эффективный
и взаимовыгодный бизнес с АЛИДИ легко: бизнес-процессы выстроены и регламентированы,
а вся команда – профессионалы высокого уровня.  Мы благодарим команду АЛИДИ за постоянное совершенствование и эффективную работу. От всей души желаем укрепления лидерских позиций, процветания и расширения границ присутствия и надеемся на дальнейшее плодотворное сотрудничество!</span>
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
                    <img src="img/logo_clients/bagi.svg" alt="bagi" width="96" class="">
                </div>
                <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">ГК БАГИ и компания АЛИДИ тесно сотрудничают, начиная с 2013 года. Сегодня АЛИДИ поставляет нашу продукцию
в более чем 5000 торговых точек в семнадцати регионах России и в Белоруссии, стабильно обеспечивая высокий уровень клиентского сервиса.
<br><br>
Экспертиза АЛИДИ в области построения продаж сыграла значительную роль в расширении дистрибьюции БАГИ на федеральном уровне, налаживании взаимоотношений с крупнейшими торговыми сетями.
<br><br>
Я хочу поблагодарить сотрудников АЛИДИ
за профессионализм в повседневной работе, оперативное и всегда результативное решение самых нестандартных задач.
<br><br>
АЛИДИ – компания-лидер, стратегическое партнерство с которым, открывает новые перспективы развития на российском рынке. </span>
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
                    <img src="img/logo_clients/konti.svg" alt="upeco" width="96" class="">
                </div>
                <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">Работа компании АЛИДИ характеризуется отлично налаженными процессами работы с партнерами, взаимовыгодными условиями сотрудничества. Сотрудники компании в любых рабочих ситуациях проявляют себя как профессионалы своего дела, всегда открыты к диалогу и готовы оперативно решить любую, даже самую нестандартную, задачу.
<br><br>
Оперативность реагирования на все запросы, нацеленность на результат, выполнение
и перевыполнение планов, наращивание объемов продаж характеризуют компанию АЛИДИ как надежного партнера и компанию-лидера.
<br><br>
АЛИДИ, работающая на рынке дистрибуции более 20 лет, является экспертом в области построения продаж на региональном и федеральном уровнях, налаживания взаимоотношений с торговыми сетями. </span>
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
                    <img src="img/logo_clients/baby_line.svg" alt="baby_line" width="96" class="">
                </div>
                <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">Компания АЛИДИ является нашим деловым партнёром и дистрибьютором торговых марок BabyLine, FeedBack и Липуня с 2012 года.
<br><br>
За время совместной работы, сотрудники компании АЛИДИ показали себя высокопрофессиональными специалистами в сфере дистрибуции, трудолюбивыми
и ответственными, способными оперативно принимать решения, умеющими креативно мыслить и идти в ногу со временем.
<br><br>
Следует отметить особое отношение компании АЛИДИ
к своим деловым партнёрам и клиентам. Честность и прозрачность в работе делают эти отношения доверительными, широчайшая клиентская база даёт бескрайние возможности развития, стабильность и оснащение современными технологиями дают уверенность в завтрашнем дне, что всегда приводит к наилучшим результатам.</span>
                </div>
            </div>
        </div>
    </div>
</div>