<div class="d-flex flex-row justify-content-between">
    <a href="menu_mob.php" class="margin_right cursor_pointer d-xl-none d-block"><img src="img/iconBurger.svg" width="20" alt="menu"></a>
    <div><a href="index.php"><img src="img/logoBlue.png" width="65" alt="logo Alidi"></a></div>
</div>
<div class="desktop_menu d-none d-lg-flex flex-row justify-content-between align-items-end">
    <ul class="d-flex flex-row justify-content-between style_text_18_30 fw-bold margin_bottom_0">
        <li class="d-flex flex-row justify-content-between margin_right_x2  ">
                <div class="margin_right">
                    <img class="menu_catalog" src="img/iconBurger.svg" width="20" alt="menu">
                </div>
            <a href="catalog.php">Каталог</a>
            </li>
        <li class="margin_right_x2"><a href="delivery_payment.php">Доставка и оплата</a></li>
        <li class="margin_right_x2 cursor_pointer"><span data-bs-toggle="modal" data-bs-target="#entrance">Подключить точку</span></li>
    </ul>
    <div class="d-flex flex-column justify-content-start align-items-start">
        <span class="color_blue style_text_12_10 margin_left_right">8-800-775-75-00</span>
        <select class="form-select form-select-sm style_border_transparent margin_top_05 fw-bold" aria-label=".form-select-lg">
            <option selected>Выберите город</option>
            <option value="1">Санкт-Петербург</option>
            <option value="2">Петрозаводск</option>
            <option value="3">Псков</option>
            <option value="4">Нижний Новгород</option>
            <option value="5">Воронеж</option>
        </select>
    </div>
</div>
<div class="d-flex flex-row justify-content-between">
    <div class="margin_right_x15"><span class="cursor_pointer index_location"><img src="img/iconLocation.svg" width="20" alt="location"></span></div>
    <div class="margin_right_x15"><a href="search.php"><img src="img/iconSearch.svg" width="20" alt="search"></a></div>
    <div class="d-xl-flex flex-xl-row margin_right_x15"><a href="lk_fiz.php" class="fw-bold"><img src="img/iconAccount.svg" width="20" alt="lk" class="margin_right_desk"></a><span class="d-none d-xl-block fw-bold">Демидов А.</span></div>
    <div class="d-xl-flex flex-xl-row"><a href="basket.php"><img src="img/iconBag.svg" width="20" alt="basket" class="margin_right_desk"></a><span class="fw-bold d-none d-xl-block">4 851,90 ₽</span></div>
</div>
<div class="open_menu_catalog container position-absolute style_width100 d-flex flex-row">
    <ul class="">
        <li><a class="color_red" href="#">Распродажа</a></li>
        <li><a class="" href="#">Суши, Роллы, Вок</a></li>
        <li><a class="" href="#">StreetFood</a></li>
        <li><a class="" href="#">Пицца и паста</a></li>
        <li><a class="" href="#">Бакалея</a></li>
        <li><a class="" href="#">Орехи, сухофрукты</a></li>
        <li><a class="" href="#">Соусы</a></li>
        <li><a class="" href="#">Консервация</a></li>
        <li><a class="" href="#">Молочные продукты, сыры</a></li>
        <li><a class="" href="#">Мясо, птица</a></li>
        <li><a class="" href="#">Рыба, морепродукты</a></li>
        <li><a class="" href="#">Колбаса, мясные деликатесы</a></li>
        <li><a class="" href="#">Яйцо</a></li>
        <li><a class="" href="#">Полуфабрикаты</a></li>
        <li><a class="" href="#">Овощи, фрукты, ягоды, грибы</a></li>
        <li><a class="" href="#">Вода, напитки, чай, кофе</a></li>
        <li><a class="" href="#">Для кондитерского производства</a></li>
        <li><a class="" href="#">Хлеб, сладости, снеки</a></li>
        <li><a class="" href="#">Здоровое питание</a></li>
        <li><a class="" href="#">Непродовольственные товары</a></li>
        <li><a class="" href="#">Товары для доставки еды</a></li>
        <li><a class="" href="#">Детское питание</a></li>
        <li><a class="" href="#">Unilever Food Solutions</a></li>
    </ul>
    <ul>
        <li><a class="color_red" href="#">Распродажа </a></li>
        <li><a class="" href="#">Все продукты из раздела</a></li>
        <li><a class="" href="#">Масло сливочное</a></li>
        <li><a class="" href="#">Маргарины, спреды</a></li>
        <li><a class="" href="#">Молоко</a></li>
        <li><a class="" href="#">Сливки</a></li>
        <li><a class="" href="#">Сыры</a></li>
        <li><a class="" href="#">Кисломолочные продукты</a></li>
        <li><a class="" href="#">Молочные напитки</a></li>
        <li><a class="" href="#">Полуфабрикаты</a></li>
    </ul>
</div>



<div class="bg_mask d-none">
    <div class="location_select margin_top_x2 d-none style_border_radius_20">
        <select class="form-select form-select-lg style_border_transparent margin_top_05 fw-bold" aria-label=".form-select-lg example">
            <option selected>Выберите город</option>
            <option value="1">Санкт-Петербург</option>
            <option value="2">Петрозаводск</option>
            <option value="3">Псков</option>
            <option value="4">Нижний Новгород</option>
            <option value="5">Воронеж</option>
        </select>
    </div>
</div>
