<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20">
        <div>
            <span class="heading_24 margin_right">Редактирование</span>
        </div>
        <a href="lk_fiz_data.php" class="close_menu"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
    </div>
        <form method="post" class="d-flex flex-column justify-content-start align-items-center form_fiz">
            <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                <label for="name" class="style_text_12_16 color_gray">Получатель</label>
                <input name="name" type="text" class="style_border_transparent">
            </div>
            <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                <label for="address" class="style_text_12_16 color_gray">Адрес</label>
                <input name="address" type="text" class="style_border_transparent">
            </div>
            <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                <label for="tel" class="style_text_12_16 color_gray">Телефон</label>
                <input name="tel" type="tel" class="style_border_transparent">
            </div>
            <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Сохранить">
            <span class="style_16_24 fw-bold"><a href="lk_fiz_data.php">Отменить</a></span>
        </form>
    </div>
<?php require('js.php'); ?>
</body>
</html>
