<div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
        <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Пресс-центр</span>
        <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center"><a href="press_center.php">Пресс-центр</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="blog.php">Блог</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="reviews.php">Отзывы</a></span>
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center flex-wrap">
        <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer accordion_active margin_bottom">Все категории</span>
        <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Видео рецепты</span>
        <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Здоровое питание</span>
        <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Стрид-фуд</span>
        <span class="d-inline-block style_padding_5 style_text_12_16 fw-bold text-center style_gray_radius cursor_pointer margin_bottom">Выпечка</span>
    </div>
    <a href="blog__single.php" class="bg_grey style_padding_10 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10">
        <img class="margin_bottom_x2" src="img/blog1.png" width="335" alt="blog_promo">
        <div class="padding_bottom">
            <span class="d-inline-block style_16_24 fw-bold margin_bottom">Профессиональные средства для уборки от Kiilto</span>
        </div>
    </a>
    <a href="blog__single.php" class="bg_grey style_padding_10 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10">
        <img class="margin_bottom" src="img/blog2.png" width="335" alt="blog_promo">
        <div class="padding_bottom">
            <span class="d-inline-block style_16_24 fw-bold margin_bottom">Служба поддержки ALIDI PROF по работе с ветеринарными справками и системой Меркурий</span>
        </div>
    </a>
    <a href="blog__single.php" class="bg_grey style_padding_10 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10">
        <img class="margin_bottom_x2" src="img/blog3.png" width="335" alt="blog_promo">
        <div class="padding_bottom">
            <span class="d-inline-block style_16_24 fw-bold margin_bottom">Видео-рецепт вегетарианского бургера от ALIDI Prof</span>
        </div>
    </a>
    <a href="blog__single.php" class="bg_grey style_padding_10 style_border_radius_20 d-flex flex-column justify-content-between margin_top_bottom_10">
        <img class="margin_bottom" src="img/blog4.png" width="335" alt="blog_promo">
        <div class="padding_bottom">
            <span class="d-inline-block style_16_24 fw-bold margin_bottom">Служба поддержки ALIDI PROF по работе с ветеринарными справками и системой Меркурий</span>
        </div>
    </a>
    <nav aria-label="Page navigation-blog">
        <ul class="pagination justify-content-center">
            <li class="d-grid page-item disabled margin_right">
                <a class=" icon_arrow_left_small page-link" href="#" tabindex="-1" aria-disabled="true"></a>
            </li>
            <li class="d-grid page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">...</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">23</a>
            </li>
            <li class="d-grid page-item">
                <a class="icon_arrow_right_small page-link" href="#"></a>
            </li>
        </ul>
    </nav>
</div>