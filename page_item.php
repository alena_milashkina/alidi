<div class="d-flex flex-row justify-content-start align-items-center flex-wrap margin_bottom">
    <a class="d-inline-block style_padding_5 style_text_12_16 text-center style_gray_radius cursor_pointer margin_right" href="index.php">Главная</a>
    <a class="d-inline-block style_padding_5 style_text_12_16 text-center style_gray_radius cursor_pointer margin_right" href="catalog.php">Каталог</a>
    <a class="d-inline-block style_padding_5 style_text_12_16 text-center style_gray_radius cursor_pointer margin_right bread_active" href="#">Мясо и птица</a>
</div>
    <div class="page-layout__content__desk layout_grid_1col">
        <div class="margin_bottom">
            <div class="d-flex flex-column flex-xl-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 padding_30_desk item">
                <div class='position-relative in_stock_tip'>
                    <img src="img/img_items_catalog/без%20полей%201.jpg" width="530" alt="photo item">
                </div>
                <div class="flex-grow-1">
                    <div class="style_border_bottom">
                        <div class="d-flex flex-row justify-content-between align-items-start margin_bottom position-relative">
                            <div class="d-flex flex-column justify-content-between position-relative">
                                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_top-10px margin_bottom">Новинка</span>
                                <div>
                                    <b>ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</b><span class="d-inline-block icon_info_item" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Внимание! Фактический вес товара может отличаться до 20%" data-bs-custom-class="style_border_radius_40 input_date_rang"></span>
                                </div>
                                <span class="d-inline-block margin_top_bottom_10">Код: 4650118452039</span>
                                <span>Условия хранения: -18°С</span>
                                <span>Торговая марка: PrimeBeef</span>
                                <span>Страна производитель: Россия</span>
                                <span>Вид упаковки: Весовая</span>
                            </div>
                            <div class="d-flex flex-column justify-content-between">
                                <div class="d-flex flex-row justify-content-start margin_bottom_x2">
                                    <span class="iconStar iconStar_active"></span>
                                    <span class="iconStar iconStar_active"></span>
                                    <span class="iconStar iconStar_active"></span>
                                    <span class="iconStar"></span>
                                    <span class="iconStar"></span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center flex-nowrap margin_bottom_x2 add_favorite cursor_pointer">
                                    <span class="d-inline-block iconFavorite margin_right"></span>
                                    <span class="d-inline-block d-none iconFavorite_active margin_right"></span>
                                    <span class="d-inline-block fw-bold width_max_desk">В избранное</span>
                                </div>
                                <div class="d-flex flex-column justify-content-start">
                                    <span class="d-inline-block style_text_16_14 color_green fw-bold ">15% скидка</span>
                                    <span class="d-inline-block style_text_10_14 color_green fw-bold text-uppercase margin_bottom">на первый заказ</span>
                                </div>
                            </div>
                            <img class="position-absolute bottom-0 end-0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Внимание! Ограничение 100 товаров!" data-bs-custom-class="style_border_radius_40 input_date_rang overflow-hidden" src="img/iconWarning.svg" width="24" alt="warning">
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-between align-content-start style_border_bottom style_padding_bottom__top_20">
                        <div class="d-flex flex-column justify-content-between">
                            <span>Цена за кг: 1 790,12 ₽</span>
                            <b class="d-inline-block margin_bottom">Цена за блок: 181 278,00 ₽</b>
                            <span class="color_blue fw-bold d-inline-block padding_bottom">+ 1 812 баллов за покупку</span>
                        </div>
                        <div>
                            <div class="margin_bottom ">
                                <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                                    <option selected>Заказ поштучно</option>
                                    <option value="1">Заказ коробками (12 шт)</option>
                                    <option value="2">Заказ блоками (36 шт)</option>
                                </select>
                            </div>
                            <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                                <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                                    <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                                    <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                                    <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                                </div>
                                <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                            </div>
                            <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
<!--                            <span data-bs-toggle="modal" data-bs-target="#modal_item_in_stock" class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center cursor_pointer">Узнать  о поступлении</span>-->
                        </div>

                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_left_right_10">
                        <span data-bs-toggle="modal" data-bs-target="#modal_item_price_low" class="style_padding_top_18 cursor_pointer">
                            <img class="margin_right" src="img/iconBell.svg" width="20" alt="bell">
                            <span class="fw-bold">Узнать о снижении цены</span>
                        </span>
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_left_right_10 color_blue">
                        <span class="style_padding_top_18 cursor_pointer">
                            <img class="margin_right" src="img/iconBellBlue.svg" width="20" alt="bell">
                            <span class="fw-bold">Подписка на снижение цены</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="margin_bottom">
            <div class="d-flex flex-column justify-content-between style_gray_radius padding_30_desk cursor_pointer">
                <div class="d-flex flex-row justify-content-between header_accordion ">
                    <span class="d-inline-flex style_text_24_36 fw-bold">Характеристики товара</span>
                    <div class="icon_arrow_down_small"></div>
                </div>
                <span class="d-inline-block content_accordion d-none margin_top">
                <b class="d-inline-block margin_top_bottom_10">Описание</b><br>
                <span class="d-inline-block margin_bottom">Top Blade. Отруб отличается превосходной мраморностью, сочностью и ароматом. Разница между данным отрубом и флэт-айрон заключается в том, что соединительная ткань здесь не вырезается.</span>
                <b class="d-inline-block margin_bottom">Состав</b><br>
                <span>Говядина мраморная замороженная.</span>
            </span>
            </div>
        </div>
        <span class="d-inline-block style_text_40_50_desk padding_30_desk fw-bold margin_bottom">С этим товаром <br class="d-xl-none">покупают</span>
        <div class="d-flex flex-row justify-content-between margin_bottom align-items-stretch">
            <div class="margin_right">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk  style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative in_stock_tip">
                        <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
            <div class="margin_right">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative not_available_tip">
                        <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer invisible">В корзину</span>
                </div>
            </div>
            <div class="margin_right">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative sale_tip">
                        <img class="margin_bottom " src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom invisible">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
            <div class="">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative in_stock_tip">
                        <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom invisible">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
        <span class="d-inline-block style_text_40_50_desk padding_30_desk fw-bold margin_bottom">Похожие товары</span>
        <div class="d-flex flex-row justify-content-between margin_bottom">
            <div class="margin_right">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative in_stock_tip">
                        <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>

                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
            <div class="margin_right">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative not_available_tip">
                        <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer invisible">В корзину</span>
                </div>
            </div>
            <div class="margin_right">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative sale_tip">
                        <img class="margin_bottom " src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom invisible">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                   <!-- <div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
            <div class="">
                <div class="d-flex flex-column justify-content-start style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 item">
                    <a href="item.php" class="position-relative in_stock_tip">
                        <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                    </a>
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom invisible">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                    <div class="d-flex flex-column justify-content-between margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <!--<div class="margin_bottom ">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>-->
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
        <div class="layout_grid_basket margin_top_x2 margin_bottom_x2">
            <div class="d-flex flex-column justify-content-between">
            <div class="d-flex flex-row justify-content-between align-items-center ">
                <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Отзывы <b class="color__lightblue fw-bold">40</b></span>
                <div class="margin_bottom">
                    <select class="form-select form-select-lg style_border_transparent width_80_mob fw-bold" aria-label=".form-select-lg">
                        <option selected>Сначала популярные</option>
                        <option value="1">Сначала дороже</option>
                        <option value="2">Сначала дешевле</option>
                    </select>
                </div>
            </div>
            <div class="style_border_lightgray style_border_radius_40 style_padding_18_20 margin_bottom">
                <div class="d-flex flex-row justify-content-between">
                    <div class="d-flex flex-row justify-content-between">
                        <img class="margin_right" src="img/logo_review_a.svg" alt="logo" width="40">
                        <div class="d-flex flex-column justify-content-start">
                            <b>Андрей Е.</b>
                            <span class="style_text_12_16 color_gray">13 Ноября 2020</span>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-between">
                        <span class="iconStar iconStar_active"></span>
                        <span class="iconStar iconStar_active"></span>
                        <span class="iconStar iconStar_active"></span>
                        <span class="iconStar"></span>
                        <span class="iconStar"></span>
                    </div>
                </div>
                <div class="padding_left_3">
                    <span class="d-inline-block style_padding_bottom__top_20 style_border_bottom">За свои деньги хорошая вырезка для стейка.Жил минимум,одна-две по-середине проходят и очень заметны,можно легко вырезать,а можно и оставить,эти жилы можно без труда прожевать.Сравнивать с Рибаем смысла нет,но из всего остального-одна из лучших вырезок для бомж-стейка. </span>
                    <span class="d-inline-block fw-bold color__lightblue margin_top_bottom_10 margin_bottom_0">Полезный отзыв?</span>
                    <div class="d-flex flex-row justify-content-start margin_top_bottom_10">
                        <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20 margin_right ">Да <span>12</span></button>
                        <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20">Нет <span>3</span></button>
                    </div>
                </div>
            </div>

            <div class="style_border_lightgray style_border_radius_40 style_padding_18_20 margin_bottom">
                <div class="d-flex flex-row justify-content-between">
                    <div class="d-flex flex-row justify-content-between">
                        <img class="margin_right" src="img/logo_review_o.svg" alt="logo" width="40">
                        <div class="d-flex flex-column justify-content-start">
                            <b>Ольга С.</b>
                            <span class="style_text_12_16 color_gray">13 Ноября 2020</span>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-between">
                        <span class="iconStar iconStar_active"></span>
                        <span class="iconStar iconStar_active"></span>
                        <span class="iconStar iconStar_active"></span>
                        <span class="iconStar"></span>
                        <span class="iconStar"></span>
                    </div>
                </div>
                <div class="padding_left_3">
                    <span class="d-inline-block style_padding_bottom__top_20 style_border_bottom">Свежее,мягкое, быстро готовиться. В упаковке  3 штуки  3 стейка. Немного прожилок. Хорошие куски.</span>
                    <span class="d-inline-block fw-bold color__lightblue margin_top_bottom_10 margin_bottom_0">Полезный отзыв?</span>
                    <div class="d-flex flex-row justify-content-start margin_top_bottom_10">
                        <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20 margin_right ">Да <span>12</span></button>
                        <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20">Нет <span>3</span></button>
                    </div>
                    <div class="d-flex flex-column justify-content-start flex-wrap">
                        <div class="d-flex flex-row justify-content-start">
                            <img class="margin_right" src="img/logo_review_alidi.svg" alt="logo" width="40">
                            <b class="align-self-center">Ответ администратора</b>
                        </div>
                        <div class="padding_left_3">
                            <span class="d-grid align-items-center">Равным образом рамки и место обучения кадров влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям. Равным образом постоянный количественный рост и сфера нашей активности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям.</span>
                        </div>
                    </div>
                </div>
            </div>
            <nav aria-label="Page navigation-reviews">
                <ul class="pagination justify-content-center">
                    <li class="d-grid page-item disabled margin_right">
                        <a class=" icon_arrow_left_small page-link" href="#" tabindex="-1" aria-disabled="true"></a>
                    </li>
                    <li class="d-grid page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="page-link" href="#">...</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="page-link" href="#">23</a>
                    </li>
                    <li class="d-grid page-item">
                        <a class="icon_arrow_right_small page-link" href="#"></a>
                    </li>
                </ul>
            </nav>
        </div>
            <div>
                <div class="d-flex flex-column justify-content-start style_gray_radius style_border_radius_40_only_desk margin_bottom_x2 style_padding_30">
                    <div class="d-flex justify-content-between style_width100 margin_bottom_x2">
                        <span class="fw-bold">3,75 из 5</span>
                        <div data-bs-toggle="modal" data-bs-target="#modal_add_review" class="d-flex flex-row justify-content-between">
                            <span class="iconStar iconStar_active"></span>
                            <span class="iconStar iconStar_active"></span>
                            <span class="iconStar iconStar_active"></span>
                            <span class="iconStar"></span>
                            <span class="iconStar"></span>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-start margin_bottom">
                        <div class="d-flex justify-content-between style_width100">
                            <span>5 звезд</span>
                            <div class="">27</div>
                        </div>
                        <svg width="100%" height="6">
                            <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                            <line x1="0" y1="0" x2="140" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
                        </svg>
                    </div>
                    <div class="d-flex flex-column justify-content-start margin_bottom">
                        <div class="d-flex justify-content-between style_width100">
                            <span>4 звезды</span>
                            <div class="">7</div>
                        </div>
                        <svg width="100%" height="6">
                            <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                            <line x1="0" y1="0" x2="20" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
                        </svg>
                    </div>
                    <div class="d-flex flex-column justify-content-start margin_bottom">
                        <div class="d-flex justify-content-between style_width100">
                            <span>3 звезды</span>
                            <div class="">3</div>
                        </div>
                        <svg width="100%" height="6">
                            <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                            <line x1="0" y1="0" x2="20" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
                        </svg>
                    </div>
                    <div class="d-flex flex-column justify-content-start margin_bottom">
                        <div class="d-flex justify-content-between style_width100">
                            <span>2 звезды</span>
                            <div class="">3</div>
                        </div>
                        <svg width="100%" height="6">
                            <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                            <line x1="0" y1="0" x2="20" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
                        </svg>
                    </div>
                    <div class="d-flex flex-column justify-content-start margin_bottom">
                        <div class="d-flex justify-content-between style_width100">
                            <span>1 звезда</span>
                            <div class="">0</div>
                        </div>
                        <svg width="100%" height="6">
                            <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                            <line x1="0" y1="0" x2="0" y2="20" stroke-width="0" stroke="#1753FF" stroke-linecap="round"></line>
                            <!--if zero starts stroke-width="0" !important-->
                        </svg>
                    </div>
                    <a href="review.php" class="color_white color_bg_blue style_width100 style_border_radius_40 style_padding_bottom__top_10 text-center fw-bold">Оставить отзыв</a>
                </div>
            </div>
        </div>
    </div>

<div class="page-layout__content_mob">
    <div class="margin_bottom">
        <div class="d-flex flex-column flex-xl-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
            <div class='position-relative'>
                <img src="img/img_items_catalog/без%20полей%201.jpg" width="100%" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
                <span class="d-inline-block position-absolute top-0 end-0 iconFavorite"></span>
            </div>
            <div>
                <div class="d-flex flex-column justify-content-between style_border_bottom style_padding_bottom__top_20 position-relative">
                    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom">
                        <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span>
                        <div class="d-flex flex-row justify-content-between">
                            <span class="iconStar iconStar_active"></span>
                            <span class="iconStar iconStar_active"></span>
                            <span class="iconStar iconStar_active"></span>
                            <span class="iconStar"></span>
                            <span class="iconStar"></span>
                        </div>
                    </div>
                    <div>
                        <b>ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</b><span class="d-inline-block icon_info_item" data-bs-toggle="popover" data-bs-content="Внимание! Фактический вес товара может отличаться до 20%" data-bs-placement="top" data-bs-custom-class="style_border_radius_40 input_date_rang overflow-hidden"></span>
                    </div>
                    <span class="d-inline-block margin_top_bottom_10">Код: 4650118452039</span>
                    <span>Условия хранения: -18°С</span>
                    <span>Торговая марка: PrimeBeef</span>
                    <span>Страна производитель: Россия</span>
                    <span>Вид упаковки: Весовая</span>
                    <img class="position-absolute icon_warning_position" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Внимание! Ограничение 100 товаров!" data-bs-placement="top" data-bs-custom-class="style_border_radius_40 input_date_rang overflow-hidden" src="img/iconWarning.svg" width="24" alt="warning">
                </div>
                <div class="d-flex flex-column justify-content-between style_padding_top_18">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    <span class="style_text_16_14 color_green fw-bold ">15% скидка</span><br><span class="style_text_10_14 color_green fw-bold text-uppercase margin_bottom">на первый заказ</span>
                </div>
                <div class="margin_bottom">
            <span class="dropdown-el" id="drop3">
                <input type="radio" name="31" value="31" checked="checked" id="31"><label for="31">Заказ поштучно</label>
                <input type="radio" name="31" value="32" id="32"><label for="32">Заказ поштучно</label>
                <input type="radio" name="31" value="33" id="33"><label for="33">Заказ поштучно</label>
                <input type="radio" name="31" value="34" id="34"><label for="34">Заказ поштучно</label>
            </span>
                </div>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                <div class="d-flex flex-column justify-content-between style_padding_left_right_10">
                    <span class="color_blue fw-bold d-inline-block padding_bottom style_border_bottom margin_top">+ 1 812 баллов за покупку</span>
                    <a href="price_low.php" class="style_padding_top_18">
                        <img class="margin_right" src="img/iconBell.svg" width="20" alt="bell">
                        <span class="fw-bold">Узнать о снижении цены</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="margin_bottom">
        <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
            <div class="d-flex flex-row justify-content-between header_accordion ">
                <span class="d-inline-flex style_text_24_36 fw-bold">Характеристики товара</span>
                <div class="icon_arrow_down_small"></div>
            </div>
            <span class="d-inline-block content_accordion d-none margin_top">
            <b class="d-inline-block margin_top_bottom_10">Описание</b><br>
            <span class="d-inline-block margin_bottom">Top Blade. Отруб отличается превосходной мраморностью, сочностью и ароматом. Разница между данным отрубом и флэт-айрон заключается в том, что соединительная ткань здесь не вырезается.</span>
            <b class="d-inline-block margin_bottom">Состав</b><br>
            <span>Говядина мраморная замороженная.</span>
        </span>
        </div>
    </div>
    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">С этим товаром<br>покупают</span>
    <div class="owl-carousel owl4 margin_bottom_x2">
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative in_stock_tip">
                    <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative not_available_tip">
                    <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer invisible">В корзину</span>
            </div>
        </div>
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative sale_tip">
                    <img class="margin_bottom " src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative in_stock_tip">
                    <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>
    <span class="d-inline-block style_text_24_36 style_text_40_50_desk padding_30_desk fw-bold margin_bottom">Похожие товары</span>
    <div class="owl-carousel owl5 margin_bottom_x2">
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative in_stock_tip">
                    <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative not_available_tip">
                    <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer invisible">В корзину</span>
            </div>
        </div>
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative sale_tip">
                    <img class="margin_bottom " src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
        <div class="">
            <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20 item">
                <a href="item.php" class="position-relative in_stock_tip">
                    <img class="margin_bottom" src="img/img_items_catalog/с%20полями%201.jpg" width="220" alt="photo item">
                </a>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom">ГОВЯДИНА мраморная наружняя часть лопатки без кости (Top Blade) с/м вес 2,46 кг. PRIMEBEEF</a>
                <div class="d-flex flex-column justify-content-between margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <!--<div class="margin_bottom ">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>-->
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>
    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Отзывы <b class="color__lightblue fw-bold">40</b></span>
    <div class="d-flex flex-column justify-content-start style_gray_radius margin_bottom_x2 style_padding_30">
        <div class="d-flex justify-content-between style_width100 margin_bottom_x2">
            <span class="fw-bold">3,75 из 5</span>
            <div class="d-flex flex-row justify-content-between">
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar"></span>
                <span class="iconStar"></span>
            </div>
        </div>
        <div class="d-flex flex-column justify-content-start margin_bottom">
            <div class="d-flex justify-content-between style_width100">
                <span>5 звезд</span>
                <div class="">27</div>
            </div>
            <svg width="100%" height="6">
                <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                <line x1="0" y1="0" x2="140" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
            </svg>
        </div>
        <div class="d-flex flex-column justify-content-start margin_bottom">
            <div class="d-flex justify-content-between style_width100">
                <span>4 звезды</span>
                <div class="">7</div>
            </div>
            <svg width="100%" height="6">
                <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                <line x1="0" y1="0" x2="20" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
            </svg>
        </div>
        <div class="d-flex flex-column justify-content-start margin_bottom">
            <div class="d-flex justify-content-between style_width100">
                <span>3 звезды</span>
                <div class="">3</div>
            </div>
            <svg width="100%" height="6">
                <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                <line x1="0" y1="0" x2="20" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
            </svg>
        </div>
        <div class="d-flex flex-column justify-content-start margin_bottom">
            <div class="d-flex justify-content-between style_width100">
                <span>2 звезды</span>
                <div class="">3</div>
            </div>
            <svg width="100%" height="6">
                <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                <line x1="0" y1="0" x2="20" y2="0" stroke-width="6" stroke="#1753FF" stroke-linecap="round"></line>
            </svg>
        </div>
        <div class="d-flex flex-column justify-content-start margin_bottom">
            <div class="d-flex justify-content-between style_width100">
                <span>1 звезда</span>
                <div class="">0</div>
            </div>
            <svg width="100%" height="6">
                <line x1="0" y1="0" x2="295" y2="0" stroke-width="6" stroke="#cccccc" stroke-linecap="round"></line>
                <line x1="0" y1="0" x2="0" y2="20" stroke-width="0" stroke="#1753FF" stroke-linecap="round"></line>
                <!--if zero starts stroke-width="0" !important-->
            </svg>
        </div>
        <a href="review.php" class="color_white color_bg_blue style_width100 style_border_radius_40 style_padding_bottom__top_10 text-center fw-bold">Оставить отзыв</a>
    </div>
    <div class="margin_bottom">
        <select class="form-select form-select-lg style_border_transparent fw-bold" aria-label=".form-select-lg">
            <option selected>Сначала популярные</option>
            <option value="1">Сначала дороже</option>
            <option value="2">Сначала дешевле</option>
        </select>
    </div>
    <div class="style_border_lightgray style_border_radius_20 style_padding_18_20 margin_bottom">
        <div class="d-flex flex-row justify-content-between">
            <div class="d-flex flex-row justify-content-between">
                <img class="margin_right" src="img/logo_review_a.svg" alt="logo" width="40">
                <div class="d-flex flex-column justify-content-start">
                    <b>Андрей Е.</b>
                    <span class="style_text_12_16 color_gray">13 Ноября 2020</span>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-between">
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar"></span>
                <span class="iconStar"></span>
            </div>
        </div>
        <div class="padding_left_3">
            <span class="d-inline-block style_padding_bottom__top_20 style_border_bottom">За свои деньги хорошая вырезка для стейка.Жил минимум,одна-две по-середине проходят и очень заметны,можно легко вырезать,а можно и оставить,эти жилы можно без труда прожевать.Сравнивать с Рибаем смысла нет,но из всего остального-одна из лучших вырезок для бомж-стейка. </span>
            <span class="d-inline-block fw-bold color__lightblue margin_top_bottom_10 margin_bottom_0">Полезный отзыв?</span>
            <div class="d-flex flex-row justify-content-start margin_top_bottom_10">
                <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20 margin_right ">Да <span>12</span></button>
                <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20">Нет <span>3</span></button>
            </div>
        </div>
    </div>
    <div class="style_border_lightgray style_border_radius_20 style_padding_18_20 margin_bottom">
        <div class="d-flex flex-row justify-content-between">
            <div class="d-flex flex-row justify-content-between">
                <img class="margin_right" src="img/logo_review_o.svg" alt="logo" width="40">
                <div class="d-flex flex-column justify-content-start">
                    <b>Ольга С.</b>
                    <span class="style_text_12_16 color_gray">13 Ноября 2020</span>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-between">
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar iconStar_active"></span>
                <span class="iconStar"></span>
                <span class="iconStar"></span>
            </div>
        </div>
        <div class="padding_left_3">
            <span class="d-inline-block style_padding_bottom__top_20 style_border_bottom">Свежее,мягкое, быстро готовиться. В упаковке  3 штуки  3 стейка. Немного прожилок. Хорошие куски.</span>
            <span class="d-inline-block fw-bold color__lightblue margin_top_bottom_10 margin_bottom_0">Полезный отзыв?</span>
            <div class="d-flex flex-row justify-content-start margin_top_bottom_10">
                <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20 margin_right ">Да <span>12</span></button>
                <button class="style_border_radius_40 style_border_radius_color_lightblue style_padding_10_20 fw-bold style_text_16_20">Нет <span>3</span></button>
            </div>
            <div class="d-flex flex-column justify-content-start flex-wrap">
                <div class="d-flex flex-row justify-content-start">
                    <img class="margin_right" src="img/logo_review_alidi.svg" alt="logo" width="40">
                    <b class="align-self-center">Ответ администратора</b>
                </div>
                <div class="padding_left_3">
                    <span class="d-grid align-items-center">Спасибо за ваш отзыв!</span>
                </div>
            </div>
        </div>
    </div>
    <nav aria-label="Page navigation-reviews">
        <ul class="pagination justify-content-center">
            <li class="d-grid page-item disabled margin_right">
                <a class=" icon_arrow_left_small page-link" href="#" tabindex="-1" aria-disabled="true"></a>
            </li>
            <li class="d-grid page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">...</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">23</a>
            </li>
            <li class="d-grid page-item">
                <a class="icon_arrow_right_small page-link" href="#"></a>
            </li>
        </ul>
    </nav>
</div>


