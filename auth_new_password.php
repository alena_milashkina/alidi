<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20">
        <div>
            <span class="heading_24 margin_right">Восстановление пароля</span>
        </div>
        <a href="entrance.php" class="close_menu"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95 email_forgot_password">
            <label for="email" class="style_text_12_16 color_gray">Email</label>
            <input name="email" type="email" required class="style_border_transparent">
        </div>
        <span class="d-grid margin_bottom style_width95 text-center fw-bold text_success d-none">На адрес buyer@gmail.com отправлено письмо с новым паролем</span>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95 position-sticky button_filter bg_white" type="submit" value="Отправить новый пароль">
        <span class="style_16_24 fw-bold come_back"><a href="entrance.php">Вернуться назад</a></span>
    </form> <!--if form post .come_back and .email_forgot_password add class d-none and .text_success remove d-none-->
</div>
<?php require('js.php'); ?>
</body>
</html>


