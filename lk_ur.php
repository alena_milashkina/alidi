<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-baseline">
           <span class="d-inline-block heading_24 style_text_40_50_desk fw-bold margin_bottom margin_right flex-fit">Личный кабинет</span>
            <hr class="flex-fill align-self-start" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk">
            <div>
                <?php require('page_lk_ur.php'); ?>
            </div>
            <div>
                <?php require('page_lk_ur_orders.php'); ?>
            </div>
        </div>
        <div class="page-layout__content_mob">
            <?php require('page_lk_ur.php'); ?>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

