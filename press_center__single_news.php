<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Пресс-центр</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <div>
                <div class="page-layout__content">
                    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="press_center.php">Пресс-центр</a></span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="blog.php">Блог</a></span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="reviews.php">Отзывы</a></span>
                    </div>
                    <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">АЛИДИ успешно справилась со всеми вызовами COVID-19</span><br>
                    <span class="d-inline-block color_gray margin_bottom">30 июня 2020</span>
                    <span class="d-inline-block fw-bold margin_bottom">По итогам второго квартала можно с уверенностью сказать, что ГК АЛИДИ справилась с напряжённой ситуацией и с поставленными задачами по минимизации рисков распространения коронавирусной инфекции в филиалах компании.</span>
                    <span class="d-inline-block margin_bottom"> АЛИДИ предприняла ряд серьёзных мер, которые помогли удержать ситуацию под контролем и не допустить распространения COVID-19. Так, были отменены командировки и служебные поездки; сотрудники переведены на удалённый формат работы; введены жёсткие ограничения на посещение офисов; компания организовала закупку оборудования, СИЗ для сотрудников; был введён постоянный контроль за соблюдением мер безопасности на объектах компании, налажен системный контроль за выявлением случаев заболевания COVID-19 среди сотрудников и принимаемыми мерами в случае выявления.
<br><br>
Также АЛИДИ контролировала наличие СИЗ у полевых сотрудников, организовала дифференцированную доставку сотрудников, разделение смен складского и офисного персонала. Обязательным и важным элементом стало внедрение регламентирующих документов с учётом законодательства страны присутствия – стандартов безопасной деятельности. Были разработаны инструкции для сотрудников по противодействию распространения инфекции, защите, правилах поведения. Проведены тренинги и вебинары для руководителей и сотрудников.
<br><br>
ГК АЛИДИ продолжает внимательно следить за ситуацией с COVID-19 во всех странах своего присутствия. Решение о снятии ограничений принимается руководством компании в соответствии с рекомендациями местных властей и текущей ситуацией в макрорегионах. </span>
                    <a href="press_center.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_padding_10 d-grid align-content-center justify-content-center width_200_desk">Все новости</a>
                </div>
            </div>
        </div>
        <div class="page-layout__content_mob">
            <div class="page-layout__content">
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                    <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="press_center.php">Пресс-центр</a></span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="blog.php">Блог</a></span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="reviews.php">Отзывы</a></span>
                </div>
                <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">АЛИДИ успешно справилась со всеми вызовами COVID-19</span>
                <span class="d-inline-block color_gray margin_bottom">30 июня 2020</span>
                <span class="d-inline-block fw-bold margin_bottom">По итогам второго квартала можно с уверенностью сказать, что ГК АЛИДИ справилась с напряжённой ситуацией и с поставленными задачами по минимизации рисков распространения коронавирусной инфекции в филиалах компании.</span>
                <span class="d-inline-block margin_bottom"> АЛИДИ предприняла ряд серьёзных мер, которые помогли удержать ситуацию под контролем и не допустить распространения COVID-19. Так, были отменены командировки и служебные поездки; сотрудники переведены на удалённый формат работы; введены жёсткие ограничения на посещение офисов; компания организовала закупку оборудования, СИЗ для сотрудников; был введён постоянный контроль за соблюдением мер безопасности на объектах компании, налажен системный контроль за выявлением случаев заболевания COVID-19 среди сотрудников и принимаемыми мерами в случае выявления.
<br><br>
Также АЛИДИ контролировала наличие СИЗ у полевых сотрудников, организовала дифференцированную доставку сотрудников, разделение смен складского и офисного персонала. Обязательным и важным элементом стало внедрение регламентирующих документов с учётом законодательства страны присутствия – стандартов безопасной деятельности. Были разработаны инструкции для сотрудников по противодействию распространения инфекции, защите, правилах поведения. Проведены тренинги и вебинары для руководителей и сотрудников.
<br><br>
ГК АЛИДИ продолжает внимательно следить за ситуацией с COVID-19 во всех странах своего присутствия. Решение о снятии ограничений принимается руководством компании в соответствии с рекомендациями местных властей и текущей ситуацией в макрорегионах. </span>
                <a href="press_center.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">Все новости</a>
            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

