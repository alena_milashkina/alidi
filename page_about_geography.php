<div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
        <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">О компании</span>
        <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right tabs">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center">Информация</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Миссия</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center">Руководство</span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab">География</span>
    </div>
    <div class="contents">
        <div class="info">
            <span class="d-inline-flex fw-bold style_text_20_30 style_padding_bottom__top_20">Компания АЛИДИ является дистрибьютором №1 и одним из крупнейших поставщиков в торговые сети продукции известных международных производителей:</span>
            <div class="d-grid grid_layout_info margin_bottom_x2">
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray">
                    <img src="img/logo_clients/logo-pg.svg" alt="p&g" width="100%">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray">
                    <img src="img/logo_clients/logo-nestle.svg" alt="nestle" width="100%">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray">
                    <img src="img/logo_clients/logo-NestlePurina.svg" alt="purina" width="100%">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray">
                    <img src="img/logo_clients/logo-nestlewaters.svg" alt="nestle waters" width="105">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray">
                    <img src="img/logo_clients/logo_mars.svg" alt="mars" width="100%">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray">
                    <img src="img/logo_clients/logo-wrigley.svg" alt="wrigley" width="105">
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">Факты о АЛИДИ</span>
            <div class="bg_blue bg_index__padding section_style height_150 d-flex flex-column justify-content-between margin_top_bottom_10">
                <div class="bg_index__bg_icon icon_alidi_white">
                </div>
                <div class="padding_bottom">
                    <span class="bg_index__heading margin_bottom">Основана в 1992 году в Нижнем Новгороде</span>
                </div>
            </div>
            <div class="page-layout__content__grid_layout margin_bottom align-self-center">
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_ruble">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">Оборот в 2020 году 80 млрд рублей</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_about_globe margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">Представлены в России, Беларуси и Казахстане</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_crosshair margin_bottom">
                    </div>
                    <div class="padding_bottom ">
                        <span class="bg_index__text">54 региона, 18 складов , 30 филиалов</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_5operators margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">ТОП-5 крупнейших логистических операторов России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_forbs margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">ТОП-200 крупнейших частных компаний России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between">
                    <div class="bg_index__bg_icon icon_users margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">9000 квалифицированных сотрудников</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_superJob margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">ТОП-15 лучших работодателей России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_hh margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">Лучший работадатель</span>
                    </div>
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">Экосистема АЛИДИ включает в себя несколько направлений</span>
            <div class="page-layout__content__grid_layout margin_bottom align-self-center height_max">
                <div class="bg_index bg_index__padding section_style d-flex flex-column justify-content-between bg_index_add_business height_max">
                    <div class="bg_index__bg_icon icon_globe margin_bottom_x2">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Дистрибуция</span>
                        <span class="bg_index__text">Продукты питания, бытовая химия, косметика, корма и товары для животных крупнейших мировых производителей и товары для HoReCa</span>
                    </div>
                </div>
                <div class="bg_index__padding section_style d-flex flex-column justify-content-between bg_index_logistic height_max">
                    <div class="bg_index__bg_icon icon_truck margin_bottom_x2">
                    </div>
                    <div class="padding_bottom">
                        <span class="d-inline-flex bg_index__heading margin_bottom">Логистические услуги</span>
                        <span class="bg_index__text margin_bottom">Полный комплекс 3PL услуг. Складское хранение, грузообработка, доставка</span>
                    </div>
                </div>
                <div class="bg_index__padding section_style d-flex flex-column justify-content-between bg_about_sklad margin_top-10px">
                    <div class="bg_index__bg_icon icon_package">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Складские услуги</span>
                        <span class="bg_index__text">Общая площадь складских помещений АЛИДИ превышает 180 000м2</span>
                    </div>
                </div>
                <div class="bg_index bg_index__padding section_style d-flex flex-column justify-content-between bg_index_advance_business margin_top-10px">
                    <div class="bg_index__bg_icon icon_megaphone">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Маркетинговое агентство</span>
                        <span class="bg_index__text">Предоставляет своим партнерам полный цикл рекламно-маркетинговых услуг</span>
                    </div>
                </div>
                <div class="bg_index bg_index__padding section_style d-flex flex-column justify-content-between bg_about_marketplace">
                    <div class="bg_index__bg_icon icon_handshake">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Маркетплейс</span>
                        <span class="bg_index__text">Цифровая платформа для любых поставщиков</span>
                    </div>
                </div>
                <div class="bg_index bg_index__padding section_style d-flex flex-column justify-content-between bg_index_horeca">
                    <div class="bg_index__bg_icon icon_bag">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Professional</span>
                        <span class="bg_index__text">Поставщик продуктов питания для сегмента HoReCa</span>
                    </div>
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">История компании</span>
            <div class="bg_blue bg_index__padding section_style height_150 d-flex flex-column justify-content-between margin_top_bottom_10">
                <div class="bg_index__bg_icon">
                    <span class="style_text_40_40">1992</span>
                </div>
                <div class="padding_bottom">
                    <span class="bg_index__heading margin_bottom">Основана в 1992 году в Нижнем Новгороде</span>
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">Документы</span>
            <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray style_padding_30">
                <div>
                    <a href="#" class="d-flex flex-row justify-content-between justify-content-center margin_bottom_x2">
                        <img class="align-self-start margin_right" src="img/iconFile.svg" alt="file" width="22">
                        <span class="style_16_24 fw-bold">Принципы, декларируемые ГК АЛИДИ при осуществлении своей деятельности</span>
                    </a>
                </div>
                <div>
                    <a href="#" class="d-flex flex-row justify-content-between margin_bottom_x2">
                        <img class="align-self-start margin_right" src="img/iconFile.svg" alt="file" width="22">
                        <span class="style_16_24 fw-bold">06 апреля 2020 года в компании утвержден «стандарт безопасной деятельности организации в целях противодействия распространения новой коронавирусной инфекции (covid-19)»</span>
                    </a>
                </div>
                <div>
                    <a href="#" class="d-flex flex-row justify-content-between">
                        <img class="align-self-start margin_right" src="img/iconFile.svg" alt="file" width="22">
                        <span class="style_16_24 fw-bold">15 мая 2020 года в компании утверждена «новая редакция стандарта безопасной деятельности гк АЛИДИ»</span>
                    </a>
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">Отзывы</span>
            <div class="d-flex flex-column justify-content-between">
                <div class="d-flex flex-row justify-content-start margin_bottom">
                    <div class="d-flex align-content-center justify-content-center style_border_radius_20 style_border_lightgray style_padding_18_20 margin_right_x2">
                        <img src="img/logo_selgros.svg" alt="logo_selgros" width="96">
                    </div>
                    <div class="d-flex flex-row align-self-center">
                        <a href="#">
                            <div class="style_gray_radius icon_arrow_left margin_right_x2"></div>
                        </a>
                        <a href="#">
                            <div class="style_gray_radius icon_arrow_right"></div>
                        </a>
                    </div>
                </div>
                <div class="style_gray_radius fw-bold style_padding_18_20 margin_bottom">
                <span>ООО «Зельгрос» выражает признательность и благодарность своему партнеру компании АЛИДИ
    за качественное предоставление услуг в сфере логистики, профессионализм команды и гибкость в реакции на изменяющиеся запросы. Также хотим отметить открытость и прозрачность во взаимодействии
    с нашим партнером.

    Стабильное качество в выполнении логистических операций позволяет нашей компании сохранять необходимый уровень обслуживания в цепи поставок, снабжении торговой сети товарами, что позитивно отражается на наших отношениях с клиентами.

    Выражаем уверенность в сохранении сложившихся дружественных отношений и надеемся на дальнейшее взаимовыгодное и плодотворное сотрудничество.</span>
                </div>
                <div class="d-flex flex-row align-self-center margin_bottom">
                    <a href="#">
                        <div class="style_gray_radius icon_arrow_left margin_right_x2"></div>
                    </a>
                    <a href="#">
                        <div class="style_gray_radius icon_arrow_right"></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="mission">
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom_x2">Цель нашей работы – предоставить партнерам высокий уровень сервиса, конкурентные цены, разнообразный ассортимент. Мы развиваем и улучшаем бизнес наших партнеров благодаря широкому масштабу операций и современным технологиям работы.</span>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Мы работаем, чтобы:</span>
            <div class="d-flex flex-row justify-content-between margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> предоставлять ведущим мировым производителям потребительских товаров услуги по логистической обработке, дистрибуции и продвижению</span>
            </div>
            <div class="d-flex flex-row justify-content-between margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">обеспечивать предприятия розничной торговли и конечных потребителей широким спектром товаров </span>
            </div>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">ЧТО НАС ВДОХНОВЛЯЕТ на эффективную работу и достижение целей – наши корпоративные ценности.</span>
            <img class="" src="img/mission.svg" alt="picture_mission" width="350">
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Команда</span>
            <span class="d-inline-flex style_padding_18_20 bg_blue style_border_radius_20 color_white margin_bottom">Главная ценность АЛИДИ – это люди. Профессионализм и отношение сотрудников к своей работе – залог долгосрочного успеха компании.</span>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Развитие</span>
            <span class="d-inline-flex style_padding_18_20 bg_blue style_border_radius_20 color_white margin_bottom">Мы работаем в бизнесе с высокой конкуренцией и всегда ставим перед собой высокие цели, с каждым днем решая все более сложные задачи. Мы должны постоянно искать пути для совершенствования, чтобы оставаться конкурентоспособными. Поэтому мы приветствуем желание сотрудников развиваться, чтобы соответствовать самым высоким профессиональным требованиям. </span>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Партнерство</span>
            <span class="d-inline-flex style_padding_18_20 bg_blue style_border_radius_20 color_white margin_bottom">Общаясь и взаимодействуя друг с другом, мы находим лучшие бизнес-решения, достигаем лучших результатов, обеспечиваем развитие компании и профессиональную преемственность сотрудников. </span>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Принципы нашей работы:</span>
            <div class="d-flex flex-row justify-content-between margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Расти бизнес сегодня! Завтра будет сложнее!</span>
            </div>
            <div class="d-flex flex-row justify-content-between margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">Всегда будь внимателен к нуждам клиентов</span>
            </div>
            <div class="d-flex flex-row justify-content-between margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Старайся сделать свою работу быстро и качественно, тогда ты вправе требовать того же от других</span>
            </div>
            <div class="d-flex flex-row justify-content-between margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">Уважение – ключ к пониманию коллег, клиентов и партнеров. Без понимания нет эффективности.
    Без эффективности нет прибыли</span>
            </div>
            <div class="d-flex flex-row justify-content-between margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Мы открыты для партнеров. Мы ценим их идеи и рассматриваем свой бизнес как неотъемлемую часть глобального бизнеса наших партнеров</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">Эффективно используй активы компании</span>
            </div>
            <div class="d-flex flex-row justify-content-between margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> Много зарабатывает тот, кто достигает поставленных целей, а не тот, кто много работает. Находи время для отдыха! Находи время, чтобы подумать. Находи время для шутки!</span>
            </div>
        </div>
        <div class="leadership">
            <a href="about_leadeship_Demchenkov.php" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-11.png" alt="Пётр Демченков" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Пётр Демченков</span>
                    <span class="style_text_12_16">Генеральный директор ГК АЛИДИ</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-10.png" alt="Иван Сычев" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Иван Сычев</span>
                    <span class="style_text_12_16">Коммерческий директор макрорегион «АЛИДИ Москва»</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-9.png" alt="Алексей Колегов" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Алексей Колегов</span>
                    <span class="style_text_12_16">Коммерческий директор макрорегиона «АЛИДИ Первый»</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-7.png" alt="Денис Лобков" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Денис Лобков</span>
                    <span class="style_text_12_16">Директор по развитию розничных проектов</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-6.png" alt="Иван Солин" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Иван Солин</span>
                    <span class="style_text_12_16">Директор по логистике</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-5.png" alt="Олег Сурков" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Олег Сурков</span>
                    <span class="style_text_12_16">Исполнительный директор Макрорегиона «АЛИДИ Москва»</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-4.png" alt="Константин Старовойтов" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Константин Старовойтов</span>
                    <span class="style_text_12_16">Директор по информационным технологиям</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/13.png" alt="Максим Глазунов" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Максим Глазунов</span>
                    <span class="style_text_12_16">Заместитель генерального директора по безопасности и управлению рисками</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-3.png" alt="Павел Овчинников" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Павел Овчинников</span>
                    <span class="style_text_12_16">Финансовый директор</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-2.png" alt="Константин Минин" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Константин Минин</span>
                    <span class="style_text_12_16">Исполнительный директор макрорегиона «АЛИДИ Первый»</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15-1.png" alt="Петр Цубер" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Петр Цубер</span>
                    <span class="style_text_12_16">Исполнительный директор макрорегионов «АЛИДИ Беларусь» и «АЛИДИ Казахстан»</span>
                </div>
            </a>
            <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
                <img src="/img/leadership/15.png" alt="Иван Варенников" width="152" class="margin_right">
                <div class="d-flex flex-column justify-content-start">
                    <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Иван Варенников</span>
                    <span class="style_text_12_16">Директор по организационному развитию</span>
                </div>
            </a>
        </div>
        <div class="geography">
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom_x2">АЛИДИ – дистрибьюторская и логистическая компания
в России, Беларуси, Казахстане</span>
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right tabs1">
                <span class="d-inline-block auth_fiz style_padding_18_20 style_text_12_16 fw-bold text-center active_tab">Россия</span>
                <span class="d-inline-block auth_ur style_padding_18_20 style_text_12_16 fw-bold text-center">Беларусь</span>
                <span class="d-inline-block auth_ur style_padding_18_20 style_text_12_16 fw-bold text-center">Казахстан</span>
            </div>
            <div class="contents1">
                <div class="russia">
                    <div class="margin_bottom">
                        <a href="about_geography_msk.php" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Москва</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Санкт-Петербург</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Нижний Новгород</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Казань</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Воронеж</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Архангельск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Белгород</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Брянск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Великий Новгород</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                </div>
                <div class="belarus">
                    <div class="margin_bottom">
                        <a href="about_geography_msk.php" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Минск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Гомель</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Витебск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Могилёв</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                </div>
                <div class="kazakhstan">
                    <div class="margin_bottom">
                        <a href="about_geography_msk.php" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Актау</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Павлодар</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Актобе</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Атырау</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
