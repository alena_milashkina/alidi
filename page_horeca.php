<div>
    <div class="d-flex flex-row justify-content-between align-items-baseline">
       <span class="d-inline-block heading_24 style_text_40_50_desk fw-bold margin_bottom margin_right flex-fit">Услуги</span>
        <hr class="flex-fill align-self-start" style="opacity: 1; height: 2px; color: #000000;">
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center"><a href="services.php">BTL</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="distribution.php">Дистрибуция</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="logistic.php">Логистика</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="horeca.php">HoReCa</a></span>
    </div>
    <span class="d-inline-block style_16_24 fw-bolder margin_bottom_x2">ALIDI RPOF — Поставщик продуктов питания для сегмента HoReCa в Санкт-Петербурге, Ленинградской области, Великом Новгороде, Пскове и Петрозаводске.
<br><br>
С 2020 года ALIDI RPOF открыла доставку для физических лиц на дом, продуктов питания и непродовольственных товаров по оптовым ценам.
<br><br>
Ключевое направление в компании АЛИДИ для HoReCa стартовало в 2016 году. На сегодняшний день, у нас более 1400 активных клиентов.
<br><br>
Мы охватили все направления данного сегмента:
Сетевые рестораны, бары, пиццерии, кафе, пекарни, производства, кулинарии, кейтеринг и службы доставки.
<br><br>
Ежегодный прирост новых клиентов составляет 20%.
Объем продаж за 2019 год вырос на 55%.
Эксклюзивные контракты с мировыми производителями и брендами. Возможность создания эксклюзивных рецептур по потребностям клиента.
В ассортименте более 3000 товаров от 80 производителей, таких как Unilever Food Solutions, Cargill, Solpro, Чудское озеро, Heinz, Sen Soy, Лимак и многие другие.<br>
АЛИДИ является одним из основных дистрибьюторов компании Nestle и подразделения Nestle Waters. Как поставщик, предлагаем полный ассортимент торговых марок Perrier, Vittel, Acqua Panna и S.Pellegrino.</span>
    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom_x2">Складская логистика</span>
    <div class="page-layout__content__grid_layout margin_bottom align-self-center margin_bottom_x2">
        <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_table margin_bottom">
            </div>
            <div class="padding_bottom ">
                <span class="bg_index__text">Площадь склада 13 000 кв.м.</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_temperature_plus margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Сухая отапливаемая зона (до +25°С)</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_temperature_minus margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Холодильная камера (от +2°C до +6°С)</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_frige margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Морозильная камера (до –21°С)</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_conditioner margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Кондиционированная зона для хранения шоколада (от 15°С до +21°С)</span>
            </div>
        </div>
        <div class="bg_grey bg_index__padding style_border_radius_20 d-flex flex-column justify-content-between height_172">
            <div class="bg_index__bg_icon icon_sertif margin_bottom">
            </div>
            <div class="padding_bottom">
                <span class="bg_index__text">Склад соответствует международным стандартам и имеет сертификат HACCP</span>
            </div>
        </div>
    </div>
    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Транспортная логистика</span>
    <span class="d-inline-block style_16_24 margin_bottom_x2">Зона доставки распространяется по всему Санкт-Петербургу и Ленинградской области.
Помимо этого, мы доставляем продукты в республику Карелия, Великий Новгород и Псковскую область.
<br><br>
Собственный парк новых автомобилей с температурным мультирежимом, для перевозки охлажденной и замороженной продукции.
Бесплатная доставка на следующий день.</span>
    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Интернет-портал</span>
    <span class="d-inline-block style_16_24 margin_bottom_x2">Интернет - портал с полным каталогом товаров, где вы можете увидеть фото товара, ознакомиться с составом, сроками и условиями хранения.
<br><br>
После регистрации на сайте, клиент сможет увидеть в личном кабинете:<br>
- Какой товар, когда и по какой цене он брал<br>
- Загрузить матрицу товаров с согласованными ценами<br>
- В один клик выгрузить все сертификаты к поставке<br>
- Ознакомиться с финансами и кредитным лимитом<br>
- Найти и выгрузить любую накладную<br>
<br><br>
Мы принимаем заказы через сайт, торгового представителя, по почте или телефону, в любом удобном формате.
<br><br>
Работаем в системе EDI.</span>
    <span class="d-inline-block style_text_24_36 fw-bold margin_bottom">Шеф-повар и кухня-студия</span>
    <span class="d-inline-block style_16_24 margin_bottom_x2">Для лучшего взаимопонимания мы создали специальную кухню — студию с новым современным оборудованием и большим экраном. Как и на любой профессиональной кухне, на нашей тоже есть свой шеф-повар.
<br><br>
Все продукты, появляющиеся в ассортименте АЛИДИ ПРОФ попадают на проработку к шеф-повару, где он тщательно проводит над ними эксперименты, готовит разнообразные блюда и проверяет по всем органолептическим показателям.
<br><br>
Каждую неделю шеф-повар приглашает на кухню-студию весь отдел направления HoReCа, где рассказывает про тонкости работы кухни в ресторанах, барах и гостиницах. Проводит мастер-класс по приготовлению популярных блюд из позиций нашего ассортимента и подробно рассказывает
о них.
<br><br>
Шеф-повар совместно с торговыми представителями выезжает на встречи с шеф-поварами заведений общественного питания, чтобы поучаствовать в проработке новых позиций и помочь в составлении меню из продуктов нашего ассортимента.
<br><br>
Если вы являетесь нашим клиентом и хотите проработать продукты совместно с нашим шеф-поваром, вам достаточно сообщить об этом вашему менеджеру или торговому представителю для уточнения проработки и согласования удобного времени.
<br><br>
Если вы еще не являетесь клиентом АЛИДИ, регистрируйтесь на сайте alidi.ru или позвоните<br>
по номеру 8-800-775-75-00.</span>
</div>
