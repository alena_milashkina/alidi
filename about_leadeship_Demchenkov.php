<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">О компании</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk margin_bottom_x2">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <div class="page-layout__content d-flex flex-column justify-content-between">
                <div class="d-flex flex-column flex-xl-row justify-content-between">
                    <img src="/img/leadership/15-11_lg.png" alt="Пётр Демченков" width="260" class="margin_bottom_x2 align-self-start margin_right_40_desk">
                    <div class="d-flex flex-column justify-content-between">
                        <span class="d-inline-flex style_text_20_30 fw-bold">Пётр Демченков</span>
                        <span class=" d-inline-flex style_16_24 fw-bold margin_bottom_x2">Генеральный директор ГК АЛИДИ</span>
                        <span class="d-inline-flex style_16_24 margin_bottom_x2">Петр Демченков возглавляет Компанию с 2006 года. Занимается вопросами развития бизнеса, стратегического менеджмента и развитием системы корпоративного управления. За годы руководства г-на Демченкова АЛИДИ стала лидером дистрибьюторского и логистического рынка России. В 2009 и 2010 годах Петр вошел в число лучших ТОП-менеджеров Санкт-Петербурга и Ленинградской области по версии издания «Деловой Петербург». В 2011 году стал победителем конкурса компании Ernst & Young «Предприниматель года» в России. В 2014 году занял первое место в рейтинге ТОП-менеджеров логистических компаний по версии Финам. Петр Демченков родился в 1973 году в Ленинграде. В 1996 году окончил Санкт-Петербургский Политехнический Университет по специальностям «Автоматика и управление в технических системах» и «Маркетинг, менеджмент и внешнеэкономическая деятельность предприятий». С 1997 по 2004 год работал в департаменте развития бизнеса компании Procter&Gamble, в 2004 - 2005 годах занимал пост директора по развитию Инвестиционного банка «КИТ Финанс».</span>
                    </div>
                </div>
                <a href="about_leadership.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 width_300_desk style_padding_10 d-grid align-content-center justify-content-center">К списку руководителей</a>
            </div>
        </div>
        <div class="page-layout__content_mob" >
            <div class="page-layout__content d-flex flex-column justify-content-between">
                <img src="/img/leadership/15-11_lg.png" alt="Пётр Демченков" width="345" class="margin_bottom_x2 align-self-center">
                <span class="d-inline-flex style_text_20_30 fw-bold">Пётр Демченков</span>
                <span class=" d-inline-flex style_16_24 fw-bold margin_bottom_x2">Генеральный директор ГК АЛИДИ</span>
                <span class="d-inline-flex style_16_24 margin_bottom_x2">Петр Демченков возглавляет Компанию с 2006 года. Занимается вопросами развития бизнеса, стратегического менеджмента и развитием системы корпоративного управления. За годы руководства г-на Демченкова АЛИДИ стала лидером дистрибьюторского и логистического рынка России. В 2009 и 2010 годах Петр вошел в число лучших ТОП-менеджеров Санкт-Петербурга и Ленинградской области по версии издания «Деловой Петербург». В 2011 году стал победителем конкурса компании Ernst & Young «Предприниматель года» в России. В 2014 году занял первое место в рейтинге ТОП-менеджеров логистических компаний по версии Финам. Петр Демченков родился в 1973 году в Ленинграде. В 1996 году окончил Санкт-Петербургский Политехнический Университет по специальностям «Автоматика и управление в технических системах» и «Маркетинг, менеджмент и внешнеэкономическая деятельность предприятий». С 1997 по 2004 год работал в департаменте развития бизнеса компании Procter&Gamble, в 2004 - 2005 годах занимал пост директора по развитию Инвестиционного банка «КИТ Финанс».</span>
                <a href="about_leadership.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">К списку руководителей</a>
            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>
