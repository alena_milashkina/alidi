<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="page-layout__content d-grid align-content-center justify-content-center">
            <div class="bg_grey style_border_radius_20 d-grid align-content-center justify-content-center text-center height_200 margin_bottom_x2 width_560_desk width_355_mob">
                <span class="d-inline-block fw-bold style_text_24_36">Корзина пуста</span>
                <span class="d-inline-block fw-bold">Вы ничего ещё не добавляли<br>в корзину</span>
            </div>
            <a href="catalog.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100  style_padding_10 d-grid align-content-center justify-content-center">Перейти в каталог товаров</a>
        </div>
    </div>
    <div class="">
        <?php require('footer.php'); ?>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

