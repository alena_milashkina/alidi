<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" integrity="sha512-+EoPw+Fiwh6eSeRK7zwIKG2MA8i3rV/DGa3tdttQGgWyatG/SkncT53KHQaS5Jh9MNOT3dmFL0FjTY08And/Cw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/lightpick.css">   <!--input date range-->
    <link rel="stylesheet" href="/css/stylesheet.css<?='?n='.time()?>"/>   <!--шрифт codec_coldbold-->
   <!-- <link rel="stylesheet" href="/css/fontawesome/css/all.css"/>-->  <!--шрифт awesome-->
    <link rel="stylesheet" href="css/owl.carousel.min.css">              <!--slider owl-->
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/all.css"/>                          <!--bootstrap стили-->
    <link rel="stylesheet" href="/css/jquery.range.css"/>
    <link rel="stylesheet" href="/css/style.css<?='?n='.time()?>"/>
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css">  <!--map style liberary-->
    <!--<meta name="msapplication-TileColor" content="#">-->
    <!--<meta name="theme-color" content="#">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <title>Alidi</title>
</head>