<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Карьера</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk margin_bottom_x2">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <div class="page-layout__content d-flex flex-column justify-content-between">
                <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">АЛИДИ — команда мечты!</span>
                <span class="d-inline-block style_16_24 margin_bottom_x2 fw-bold">
•   У нас надежно и стабильно. Всегда
<br><br>
•   Мы входим в Топ дистрибьюторов Procter&Gamble, Nestle, Nestle Waters, Mars, М.Видео
<br><br>
•   Мы обеспечим вас интересными задачами
<br><br>
•   И работой в разных городах России и за рубежом
<br><br>
•   Мы любим то, что делаем и делаем это хорошо
<br><br>
•   Те, кто разделяют такой подход – получают хорошую заработную плату и карьерный рост
<br><br>
•   Добро пожаловать!
</span>
                <a href="https://job.alidi.ru/" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold width_300_desk style_padding_10 d-grid align-content-center justify-content-center">Перейти на сайт ALIDI JOB</a>
            </div>
        </div>
        <div class="page-layout__content_mob">
            <div class="page-layout__content d-flex flex-column justify-content-between">
                <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">АЛИДИ — команда мечты!</span>
                <span class="d-inline-block style_16_24 margin_bottom_x2 fw-bold">
•   У нас надежно и стабильно. Всегда
<br><br>
•   Мы входим в Топ дистрибьюторов Procter&Gamble, Nestle, Nestle Waters, Mars, М.Видео
<br><br>
•   Мы обеспечим вас интересными задачами
<br><br>
•   И работой в разных городах России и за рубежом
<br><br>
•   Мы любим то, что делаем и делаем это хорошо
<br><br>
•   Те, кто разделяют такой подход – получают хорошую заработную плату и карьерный рост
<br><br>
•   Добро пожаловать!
</span>
                <a href="https://job.alidi.ru/" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">Перейти на сайт ALIDI JOB</a>
            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>
