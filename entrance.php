<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 position-relative">
            <div class="d-flex flex-row style_text_24_36 justify-content-between header_accordion align-items-center style_width100">
                <div class="d-flex flex-row align-items-center">
                    <span class="d-inline-flex fw-bold margin_right">Вход</span>
                    <div class="icon_arrow_down_small icon_arrow_down_small_bg_white"></div>
                </div>
                <a href="index.php" class="close_menu margin_top_0 align-self-start"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
            </div>
            <div class="d-flex flex-column justify-content-between style_gray_radius style_text_20_30 fw-bold style_padding_15_20 content_accordion d-none position-absolute top-100 start-05 style_width95">
                <a href="entrance.php" class="style_border_bottom d-inline-block">Вход</a>
                <a href="auth.php" class="d-inline-block">Регистрация</a>
            </div>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="email" placeholder="Email">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="password" placeholder="Пароль">
        <span class="style_16_24 fw-bold margin_bottom"><a href="auth_new_password.php">Забыли пароль?</a></span>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Войти">
        <span class="style_16_24 fw-bold"><a href="auth.php">Зарегистироваться</a></span>
    </form>
</div>
<?php require('js.php'); ?>
</body>
</html>


