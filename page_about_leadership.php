<div>
    <a href="about_info.php">Информация</a>
    <a href="about_mission.php">Миссия</a>
    <a href="about_leadership.php">Руководство</a>
    <a href="about_geography.php">География</a>
    <a href="about_leadeship_Demchenkov.php" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-11.png" alt="Пётр Демченков" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Пётр Демченков</span>
            <span class="style_text_12_16">Генеральный директор ГК АЛИДИ</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-10.png" alt="Иван Сычев" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Иван Сычев</span>
            <span class="style_text_12_16">Коммерческий директор макрорегион «АЛИДИ Москва»</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-9.png" alt="Алексей Колегов" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Алексей Колегов</span>
            <span class="style_text_12_16">Коммерческий директор макрорегиона «АЛИДИ Первый»</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-7.png" alt="Денис Лобков" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Денис Лобков</span>
            <span class="style_text_12_16">Директор по развитию розничных проектов</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-6.png" alt="Иван Солин" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Иван Солин</span>
            <span class="style_text_12_16">Директор по логистике</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-5.png" alt="Олег Сурков" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Олег Сурков</span>
            <span class="style_text_12_16">Исполнительный директор Макрорегиона «АЛИДИ Москва»</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-4.png" alt="Константин Старовойтов" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Константин Старовойтов</span>
            <span class="style_text_12_16">Директор по информационным технологиям</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/13.png" alt="Максим Глазунов" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Максим Глазунов</span>
            <span class="style_text_12_16">Заместитель генерального директора по безопасности и управлению рисками</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-3.png" alt="Павел Овчинников" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Павел Овчинников</span>
            <span class="style_text_12_16">Финансовый директор</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-2.png" alt="Константин Минин" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Константин Минин</span>
            <span class="style_text_12_16">Исполнительный директор макрорегиона «АЛИДИ Первый»</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15-1.png" alt="Петр Цубер" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Петр Цубер</span>
            <span class="style_text_12_16">Исполнительный директор макрорегионов «АЛИДИ Беларусь» и «АЛИДИ Казахстан»</span>
        </div>
    </a>
    <a href="#" class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom">
        <img src="/img/leadership/15.png" alt="Иван Варенников" width="152" class="margin_right">
        <div class="d-flex flex-column justify-content-start">
            <span class="style_text_14_18 fw-bold margin_bottom margin_top_05">Иван Варенников</span>
            <span class="style_text_12_16">Директор по организационному развитию</span>
        </div>
    </a>
</div>