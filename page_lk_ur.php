<div class="page-layout__content_mob">
    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom footer_text_mob">
        <ul class="open_menu__style_text_mob">
            <li><a href="lk_fiz_favorites.php">Избранное</a></li>
            <li><a href="lk_ur_orders.php">Заказы</a></li>
            <li><a href="lk_ur_product.php">Товары из заказов</a></li>
            <li><a href="lk_ur_finance.php">Финансы</a></li>
            <li><a href="lk_ur_delivery.php">Доставка</a></li>
            <li><a href="lk_ur_users.php">Пользователи</a></li>
            <li><a href="lk_ur_data.php">Мои данные</a></li>
        </ul>
    </div>
</div>
<div class="page-layout__content_desk">
    <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40 style_padding_18_20">
        <ul class="open_menu__style_text_desk">
            <li><a href="lk_ur_favorites.php">Избранное</a></li>
            <li><a href="lk_ur.php">Заказы</a></li>
            <li><a href="lk_ur_product.php">Товары из заказов</a></li>
            <li><a href="lk_ur_finance.php">Финансы</a></li>
            <li><a href="lk_ur_delivery.php">Доставка</a></li>
            <li><a href="lk_ur_users.php">Пользователи</a></li>
            <li><a href="lk_ur_data.php">Мои данные</a></li>
        </ul>
    </div>
</div>