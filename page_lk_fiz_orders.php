<div>
    <div class="d-flex flex-column flex-xl-row justify-content-between">
        <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill grow_02">
            <img src="img/CalendarBlank.svg" width="20" alt="calendar" class="margin_right">
            <input class="style_16_24 fw-bold input_date_rang width_200_mob cursor_pointer me-auto datepicker" placeholder="Выберите дату" type="text" id="datepicker2"/>
            <img src="img/arrowDropBig.svg" width="20" alt="dropDown">
        </div>
        <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
            <img class="margin_right" src="img/iconSearchBlack.svg" width="20" alt="search">
            <input class="style_gray_radius style_border_transparent" type="text" placeholder="Поиск">
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <a  href="lk_orders_info.php" class="d-flex flex-column">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_shipping d-xl-none">Отправлен 25 января 2021</span>
            <span class="style_16_24">23 января 2021</span>
            <span class="style_text_24_36 fw-bold">№ <span>378-5929-3378</span></span>
            <span class="style_16_24">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
            <span  class="style_text_24_36 fw-bold d-xl-none">24 578,90 ₽</span>
        </a>
        <div class="d-none d-xl-flex flex-column justify-content-between align-items-end">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_shipping">Отправлен 25 января 2021</span>
            <span  class="style_text_24_36 fw-bold">24 578,90 ₽</span>
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <a  href="lk_orders_info.php" class="d-flex flex-column">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered d-xl-none">Доставлен 25 января 2021</span>
            <span class="style_16_24">23 января 2021</span>
            <span class="style_text_24_36 fw-bold">№ <span>378-5929-3378</span></span>
            <span class="style_16_24">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
            <span  class="style_text_24_36 fw-bold d-xl-none">24 578,90 ₽</span>
        </a>
        <div class="d-none d-xl-flex flex-column justify-content-between align-items-end">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered">Доставлен 25 января 2021</span>
            <span  class="style_text_24_36 fw-bold">24 578,90 ₽</span>
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <a  href="lk_orders_info.php" class="d-flex flex-column">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered d-xl-none">Доставлен 25 января 2021</span>
            <span class="style_16_24">23 января 2021</span>
            <span class="style_text_24_36 fw-bold">№ <span>378-5929-3378</span></span>
            <span class="style_16_24">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
            <span  class="style_text_24_36 fw-bold d-xl-none">24 578,90 ₽</span>
        </a>
        <div class="d-none d-xl-flex flex-column justify-content-between align-items-end">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered">Доставлен 25 января 2021</span>
            <span  class="style_text_24_36 fw-bold">24 578,90 ₽</span>
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <a  href="lk_orders_info.php" class="d-flex flex-column">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered d-xl-none">Доставлен 25 января 2021</span>
            <span class="style_16_24">23 января 2021</span>
            <span class="style_text_24_36 fw-bold">№ <span>378-5929-3378</span></span>
            <span class="style_16_24">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
            <span  class="style_text_24_36 fw-bold d-xl-none">24 578,90 ₽</span>
        </a>
        <div class="d-none d-xl-flex flex-column justify-content-between align-items-end">
            <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered">Доставлен 25 января 2021</span>
            <span  class="style_text_24_36 fw-bold">24 578,90 ₽</span>
        </div>
    </div>
</div>