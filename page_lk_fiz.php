<div class="page-layout__content_mob">
    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom footer_text_mob">
        <ul class="open_menu__style_text_mob">
            <li><a href="lk_fiz_favorites.php">Избранное</a></li>
            <li><a href="lk_fiz_orders.php">Заказы</a></li>
            <li><a href="lk_fiz_data.php">Мои данные</a></li>
            <li><a href="lk_fiz_address.php">Адреса и получатели</a></li>
            <li><a href="lk_fiz_notification.php">Уведомления</a></li>
        </ul>
    </div>
</div>
<div class="page-layout__content_desk">
    <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40 style_padding_18_20">
        <ul class="open_menu__style_text_desk">
            <li><a href="lk_fiz_favorites.php">Избранное</a></li>
            <li><a href="lk_fiz.php">Заказы</a></li>
            <li><a href="lk_fiz_data.php">Мои данные</a></li>
            <li><a href="lk_fiz_address.php">Адреса и получатели</a></li>
            <li><a href="lk_fiz_notification.php">Уведомления</a></li>
        </ul>
    </div>
</div>