<div class="d-grid layout_grid__catalog margin_bottom_x2">
    <div class="banner_catalog ">
        <img src="img/banner.png" alt="banner alidi coin" width="100%" class="style_border_radius_20">
    </div>
    <a href="sale.php" class="color_white style_border_radius_20 sale_catalog margin_bottom_x2 margin_bottom_0_mob">
        <span class="style_text_24_36 fw-bold style_padding_30 style_padding_bottom__top_20 d-inline-block">Распродажа</span>
    </a>
    <a href="catalog_next.php" class="bg_catalog_sushi">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-block position-absolute">Суши, Роллы и Вок</span>
        <img src="img/bg_catalog/m-image-sushi.jpg" width="100%" alt="sushi" class="d-xl-none style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_street">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Street Food</span>
        <img src="img/bg_catalog/m-image-street.jpg" width="100%" alt="street food" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_pizza">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Пицца и паста</span>
        <img src="img/bg_catalog/m-image-pizza.jpg" width="100%" alt="pizza" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_bakaley">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Бакалея</span>
        <img src="img/bg_catalog/m-image-bakaleya.jpg" width="100%" alt="bakaleya" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_meat">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Мясо и птица</span>
        <img src="img/bg_catalog/image-meat.jpg" width="100%" alt="meat and chicken" class=" style_border_radius_20 d-none d-xl-block">
        <img src="img/bg_catalog/m-image-meat.jpg" width="100%" alt="meat" class="d-xl-none style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_sauces">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Соусы</span>
        <img src="img/bg_catalog/m-image-sause.jpg" width="100%" alt="sause" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_conserf">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Консервация</span>
        <img src="img/bg_catalog/m-image-konserva.jpg" width="100%" alt="konserva" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_milk">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-block position-absolute">Молочные<br>продукты и сыры</span>
        <img src="img/bg_catalog/m-image-milk.jpg" width="100%" alt="milk" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_nuts">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Орехи и сухофрукты</span>
        <img src="img/bg_catalog/m-image-nuts.jpg" width="100%" alt="nuts" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_fish">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Рыба и морепродукты</span>
        <img src="img/bg_catalog/m-image-fish.jpg" width="100%" alt="fish" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_egg">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Яйцо</span>
        <img src="img/bg_catalog/m-image-eggs.jpg" width="100%" alt="egg" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_vareniki">
        <span class="style_text_14_18 style_padding_left_top_20 fw-bold d-inline-flex position-absolute">Полуфабрикаты</span>
        <img src="img/bg_catalog/m-image-vareniki.jpg" width="100%" alt="vareniki" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_salami">
        <span class="style_text_24_36 style_padding_18_20 fw-bold d-inline-flex color_white position-absolute">Колбаса и мясные деликатесы</span>
        <img src="img/bg_catalog/image-salami.jpg" width="100%" alt="salami" class=" style_border_radius_20 d-none d-xl-block">
        <img src="img/bg_catalog/m-image-salami.jpg" width="100%" alt="salami" class="d-xl-none  style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_fruit">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Овощи, фрукты,<br>ягоды и грибы</span>
        <img src="img/bg_catalog/image-fruits.jpg" width="100%" alt="fruits" class="style_border_radius_20 d-none d-xl-block">
        <img src="img/bg_catalog/m-image-fruits.jpg" width="100%" alt="fruits" class="d-xl-none style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_water">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Вода, напитки, чай и кофе</span>
        <img src="img/bg_catalog/image-water.jpg" width="100%" alt="water" class="style_border_radius_20 d-none d-xl-block">
        <img src="img/bg_catalog/m-image-water.jpg" width="100%" alt="water" class="d-xl-none style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_bread">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Хлеб, сладости<br>и  снеки</span>
        <img src="img/bg_catalog/m-image-bread.jpg" width="100%" alt="bread" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_healthy">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Здоровое питание</span>
        <img src="img/bg_catalog/m-image-healthy.jpg" width="100%" alt="healthy food" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_conditer">
        <span class="style_text_14_18 style_padding_left_top_20 fw-bold d-inline-flex position-absolute">Для<br>кондитерского<br> производства</span>
        <img src="img/bg_catalog/m-image-meal.jpg" width="100%" alt="meal" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_kids">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Детское питание</span>
        <img src="img/bg_catalog/image-kids.jpg" width="100%" alt="kids" class="style_border_radius_20 d-none d-xl-block">
        <img src="img/bg_catalog/m-image-kids.jpg" width="100%" alt="kids" class="d-xl-none style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_notfood">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Непродовольственные<br></br>товары</span>
        <img src="img/bg_catalog/m-image-nonfood.jpg" width="100%" alt="notfood" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_package">
        <span class="style_text_14_18 style_padding_18_20 fw-bold d-inline-flex position-absolute">Товары для доставки еды</span>
        <img src="img/bg_catalog/m-image-package.jpg" width="100%" alt="notfood" class=" style_border_radius_20">
    </a>
    <a href="#" class="bg_catalog_uni">
        <img src="/img/bg_catalog/m-unileverLogo.jpg" width="100%" alt="sertificat" class=" style_border_radius_20">
    </a>
</div>
<h1 class="d-block style_text_24_36 style_text_40_50_desk fw-bold margin_bottom">Продукты питания <br class="d-none d-xl-block">для профессионалов!</h1>
<div class="bg_blue bg_index__padding section_style height_150 d-flex flex-column justify-content-between margin_top_bottom_10 d-xl-none">
    <div class="bg_index__bg_icon icon_timer_white">
    </div>
    <div class="padding_bottom">
        <span class="bg_index__heading margin_bottom">Заказы, сделанные до 18:00, доставляем на следующий день</span>
    </div>
</div>
<div class="page-layout__content__grid_layout page-layout__content__grid_layout_catalog_desk margin_bottom align-self-center margin_bottom_x2">
    <div class="bg_blue bg_index__padding section_style height_260_desk flex-column justify-content-between  style_border_radius_40_only_desk d-none d-xl-flex place_grid_truck">
        <div class="bg_index__bg_icon icon_timer_white margin_bottom_x2">
        </div>
        <div class="d-inline-block">
            <span class="bg_index__heading">Заказы, сделанные до 18:00,<br> доставляем на следующий день</span>
        </div>
    </div>
    <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
        <div class="bg_index__bg_icon icon_docs">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__text">Полный пакет документов для юридических и физических лиц</span>
        </div>
    </div>
    <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
        <div class="bg_index__bg_icon icon_truck margin_bottom_x2">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__text">Бесплатная доставка по Санкт-Петербургу и Ленобласти</span>
        </div>
    </div>
    <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
        <div class="bg_index__bg_icon icon_receipt margin_bottom">
        </div>
        <div class="padding_bottom ">
            <span class="bg_index__text">Минимальная сумма заказа от 3000 рублей</span>
        </div>
    </div>
    <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
        <div class="bg_index__bg_icon icon_shopping margin_bottom">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__text">Более 3300 товаров  для профессионалов</span>
        </div>
    </div>
    <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between height_172">
        <div class="bg_index__bg_icon icon_target margin_bottom">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__text">Индивидуальное ценообразование</span>
        </div>
    </div>
</div>
<div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom">
        <span class="style_text_24_36 fw-bold">Пресс-центр</span>
        <a href="press_center.php" class="color_blue style_text_14_18 fw-bold">Показать все</a>
    </div>
    <div class="owl-carousel owl3">
        <div class="d-flex flex-column justify-content-between style_border_radius_20 bg_white style_padding_18_20">
            <span class="d-inline-block margin_bottom_x2 text-ellipsis">Итоги года 2020 для ресторанного бизнеса</span>
            <span class="fw-bold">30 декабря 2020</span>
        </div>
        <div class="d-flex flex-column justify-content-between style_border_radius_20 bg_white style_padding_18_20">
            <span class="d-inline-block margin_bottom_x2 text-ellipsis">Новые ограничения для предприятий общественного питания в Санкт-Петербурге или как государство окрасило в черный цвет будни мелких предпринемателей</span>
            <span class="fw-bold">1 ноября 2020</span>
        </div>
        <div class="d-flex flex-column justify-content-between style_border_radius_20 bg_white style_padding_18_20">
            <span class="d-inline-block margin_bottom_x2 text-ellipsis">Новые ограничения для предприятий общественного питания в Санкт-Петербурге</span>
            <span class="fw-bold">3 декабря 2020</span>
        </div>
    </div>
</div>