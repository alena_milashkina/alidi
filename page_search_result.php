<div class="d-flex flex-row justify-content-between align-items-baseline">
    <span class="d-inline-block heading_24  margin_right flex-fit">Результаты поиска</span>
    <hr class="flex-fill align-self-start" style="opacity: 1; height: 2px; color: #000000;">
</div>
<span class="d-inline-block margin_bottom_x2 fw-bold">10 товаров по запросу «мясо»</span>
<div class="page-layout__content__desk_catalog_next grid_layout_sale">
    <div class="filter_sale">
        <form method="post" class="d-flex flex-column justify-content-between align-items-center">
            <div class="margin_bottom style_width95">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion ">
                        <span class="d-inline-flex fw-bold">Бренд</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <div class="d-inline-block content_accordion d-none margin_top">
                        <div class="style_border_lightgray style_border_radius_20 style_padding_10">
                            <img src="img/iconSearchBlack.svg" width="20">
                            <input class="style_gray_radius style_border_transparent" type="text">
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter1">
                                <input type="checkbox" name="filter1" class="form-check-input margin_right" checked>
                                Без бренда
                            </label>
                            <span>16</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter2">
                                <input type="checkbox" name="filter2" class="form-check-input margin_right">
                                PrimeBeef
                            </label>
                            <span>14</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter3">
                                <input type="checkbox" name="filter3" class="form-check-input margin_right">
                                Мит Кинг
                            </label>
                            <span>7</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter4">
                                <input type="checkbox" name="filter4" class="form-check-input margin_right">
                                Мираторг
                            </label>
                            <span>14</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter5">
                                <input type="checkbox" name="filter5" class="form-check-input margin_right">
                                Concepcion
                            </label>
                            <span>14</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="margin_bottom style_width95">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion ">
                        <span class="d-inline-flex fw-bold">Страна производитель</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <div class="d-inline-block content_accordion d-none margin_top">
                        <div class="style_border_lightgray style_border_radius_20 style_padding_10">
                            <img src="img/iconSearchBlack.svg" width="20">
                            <input class="style_gray_radius style_border_transparent" type="text">
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter1">
                                <input type="checkbox" name="filter1" class="form-check-input margin_right" checked>
                                Россия
                            </label>
                            <span>16</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter2">
                                <input type="checkbox" name="filter2" class="form-check-input margin_right">
                                Италия
                            </label>
                            <span>14</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter3">
                                <input type="checkbox" name="filter3" class="form-check-input margin_right">
                                Беларусь
                            </label>
                            <span>7</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter4">
                                <input type="checkbox" name="filter4" class="form-check-input margin_right">
                                Украина
                            </label>
                            <span>14</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                            <label class="" for="filter5">
                                <input type="checkbox" name="filter5" class="form-check-input margin_right">
                                Казахстан
                            </label>
                            <span>14</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="items_sale">
        <div class="d-xl-block margin_bottom view_item2 d-none item">
            <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
                <a href="item.php" class="position-relative">
                    <img class="margin_bottom width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                    <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
                </a>
                <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start ">
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
                    <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                    <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                        <span>Старая цена за кг: 1 790,12 ₽</span>
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <span class="style_text_16_14 color_green fw-bold ">15% скидка</span><br><span class="style_text_10_14 color_green fw-bold text-uppercase margin_bottom">на первый заказ</span>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-xl-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
        <div class="d-xl-block margin_bottom view_item2 d-none item">
            <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
                <a href="item.php" class="position-relative">
                    <img class="margin_bottom width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                    <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
                </a>
                <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
                    <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                    <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                        <span>Старая цена за кг: 1 790,12 ₽</span>
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                    <div class="position-relative">
                        <span class="d-inline-block style_text_12_16 color_green fw-bold margin_right">Возможная цена - 1 700,61 ₽/кг</span>
                        <img class="position-absolute icon_Info_position_slider" src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
                    </div>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-xl-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
        <div class="d-xl-block margin_bottom view_item2 d-none item">
            <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
                <a href="item.php" class="position-relative">
                    <img class="margin_bottom width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                    <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
                </a>
                <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
                    <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                    <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                        <span>Старая цена за кг: 1 790,12 ₽</span>
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    </div>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                    <div class="d-none d-flex flex-xl-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                </div>
            </div>
        </div>
        <nav class="margin_bottom_x2 margin_top_x2" aria-label="Page navigation-catalog">
            <ul class="pagination justify-content-center">
                <li class="d-grid page-item disabled margin_right">
                    <a class=" icon_arrow_left_small page-link" href="#" tabindex="-1" aria-disabled="true"></a>
                </li>
                <li class="d-grid page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="d-grid page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="d-grid page-item">
                    <a class="page-link" href="#">...</a>
                </li>
                <li class="d-grid page-item">
                    <a class="page-link" href="#">23</a>
                </li>
                <li class="d-grid page-item">
                    <a class="icon_arrow_right_small page-link" href="#"></a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="page-layout__content_mob">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom">
        <select class="d-xl-none form-select form-select-sm style_border_transparent width_80_mob fw-bold" aria-label=".form-select-lg">
            <option selected>Сначала популярные</option>
            <option value="1">Сначала дороже</option>
            <option value="2">Сначала дешевле</option>
        </select>
        <div class="d-flex flex-row justify-content-evenly">
            <img class="d-xl-none view view1 d-none margin_right cursor_pointer" src="img/iconView1.svg" width="20" alt="view catalog1">
            <img class="d-xl-none view view2 cursor_pointer margin_right" src="img/iconView2.svg" width="20" alt="view catalog2">
            <a href="filter.php" class="d-xl-none"><img src="img/iconFilter.svg" width="20" alt="filter Catalog"></a>
        </div>
    </div>

    <!--view2-->
    <!--<div class="d-xl-block margin_bottom view_item2 d-none item">
        <div class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%206.jpg" width="125" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
            </a>
            <div class="d-flex flex-column justify-content-between style_text_12_16">
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Старая цена за кг: 1 790,12 ₽</span>
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    <div class="d-flex flex-row justify-content-between style_width100 style_gray_radius style_padding_10">
                        <span class="color_green fw-bold">Возможная цена <br> 1 700,61 ₽/кг</span>
                        <img class="icon_Info_position_slider " src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
                    </div>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-xl-block margin_bottom view_item2 d-none item">
        <div class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%206.jpg" width="125" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span><br>
            </a>
            <div class="d-flex flex-column justify-content-between style_text_12_16">
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Старая цена за кг: 1 790,12 ₽</span>
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    <div class="d-flex flex-row justify-content-between style_width100 style_gray_radius style_padding_10">
                        <span class="color_green fw-bold">Возможная цена <br> 1 700,61 ₽/кг</span>
                        <img class="icon_Info_position_slider " src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
                    </div>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
    </div>-->
    <div class="d-xl-block margin_bottom view_item2 d-none item">
        <div class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%206.jpg" width="125" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <div class="d-flex flex-column justify-content-between style_text_12_16">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    <div class="d-flex flex-row justify-content-between style_width100 style_gray_radius style_padding_10">
                        <span class="color_green fw-bold">Возможная цена <br> 1 700,61 ₽/кг</span>
                        <img class="icon_Info_position_slider " src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
                    </div>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-xl-block margin_bottom view_item2 d-none item">
        <div class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%205.jpg" width="125" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <div class="d-flex flex-column justify-content-between style_text_12_16">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                    <span class="style_text_16_14 color_green fw-bold ">15% скидка</span><br><span class="style_text_10_14 color_green fw-bold text-uppercase margin_bottom">на первый заказ</span>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-xl-block margin_bottom view_item2 d-none item">
        <div class="d-flex flex-row justify-content-start style_border_radius_20 style_border_lightgray style_padding_10">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%205.jpg" width="125" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <div class="d-flex flex-column justify-content-between style_text_12_16">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
                <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                    <div class="margin_bottom">
                        <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                            <option selected>Заказ поштучно</option>
                            <option value="1">Заказ коробками (12 шт)</option>
                            <option value="2">Заказ блоками (36 шт)</option>
                        </select>
                    </div>
                    <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                        <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                            <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                            <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                        </div>
                        <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                    </div>
                    <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                </div>
            </div>
        </div>
    </div>
    <!--view1-->
   <!-- <div class="d-inline-block margin_bottom view_item1 item">
        <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span><br>
            <a href="item.php" class="fw-bold margin_top_bottom_10">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Старая цена за кг: 1 790,12 ₽</span>
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                <span class="style_text_12_16 color_green fw-bold">Возможная цена - 1 700,61 ₽/кг</span>
                <img class="position-absolute icon_Info_position_slider" src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>
    <div class="d-inline-block margin_bottom view_item1 item">
        <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span><br>
            <a href="item.php" class="fw-bold margin_top_bottom_10">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Старая цена за кг: 1 790,12 ₽</span>
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                <span class="style_text_12_16 color_green fw-bold">Возможная цена - 1 700,61 ₽/кг</span>
                <img class="position-absolute icon_Info_position_slider" src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>-->
    <div class="d-inline-block margin_bottom view_item1 item">
        <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span>
            <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                <span class="style_text_12_16 color_green fw-bold">Возможная цена - 1 700,61 ₽/кг</span>
                <img class="position-absolute icon_Info_position_slider" src="img/iconInfo.svg" width="20" alt="info" style="width: 20px;">
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>
    <div class="d-inline-block margin_bottom view_item1 item">
        <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%205.jpg" width="100%" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span>
            <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                <span class="style_text_16_14 color_green fw-bold ">15% скидка</span><br><span class="style_text_10_14 color_green fw-bold text-uppercase margin_bottom">на первый заказ</span>
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>
    <div class="d-inline-block margin_bottom view_item1 item">
        <div class="d-flex flex-column justify-content-start style_border_radius_20 style_border_lightgray style_padding_18_20">
            <a href="item.php" class="position-relative">
                <img class="margin_bottom" src="img/img_items_catalog/с%20полями%205.jpg" width="100%" alt="photo item">
                <span class="d-inline-block position-absolute top-0 start-0 style_padding_5 style_padding_left_right_10 color_white bg_green style_text_10_10 style_border_radius_20 text-uppercase">В наличии</span>
            </a>
            <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase">Новинка</span>
            <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
            <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                <span>Цена за кг: 1 790,12 ₽</span>
                <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
            </div>
        </div>
    </div>
    <nav class="margin_bottom_x2 margin_top_x2" aria-label="Page navigation-catalog">
        <ul class="pagination justify-content-center">
            <li class="d-grid page-item disabled margin_right">
                <a class=" icon_arrow_left_small page-link" href="#" tabindex="-1" aria-disabled="true"></a>
            </li>
            <li class="d-grid page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">...</a>
            </li>
            <li class="d-grid page-item">
                <a class="page-link" href="#">23</a>
            </li>
            <li class="d-grid page-item">
                <a class="icon_arrow_right_small page-link" href="#"></a>
            </li>
        </ul>
    </nav>
</div>
<div class="seo d-flex flex-column justify-content-start style_gray_radius style_padding_18_20 margin_bottom_x2">
    <span class="d-inline-block fw-bold style_text_24_36 margin_bottom">Мясо, птица</span>
    <span>Продукты животного происхождения обладают высокой пищевой ценностью, они обогащены белками, содержат витамины, углеводы и разнообразные ферменты.
<br><br>
Необходимый для человеческого организма животный белок, содержащийся в таких продуктах, в пище заменить ничем нельзя.
<br><br>
Различные виды мяса включены в рацион почти каждого человека.
<br><br>
Блюда из мяса присутствуют в каждом меню и пользуются большим спросом у гостей.
<br><br>
Хорошо прожаренный кусочек мяса с небольшим гарниром, является полноценным приемом пищи и прекрасным поводом зайти в именно в Ваш ресторан.
<br><br>
Вкус блюда нарпямую зависит от качества используемого мяса. Поэтому, очень важно выбирать мясо по соотношению цены и качества.
<br><br>
Мясо птицы обладает меньшей жирностью, и приволирует в приготовлении легких и диетических блюд.
<br><br>
В нашем каталоге вы сможете найти мясо птицы, говядину, свинину, субпродукты. </span>
</div>

