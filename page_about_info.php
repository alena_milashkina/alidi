<div class="">
<!--    <div class="d-flex flex-row justify-content-between align-items-start margin_bottom style_border_radius_40 style_accordion  owl-carousel owl8 tabs">
        <div class="d-inline-block style_padding_bottom__top_20 style_text_12_16 fw-bold text-center style_border_radius_40 style_padding_left_right_10 cursor_pointer active_tab style_width_max tab">Информация</div>
        <div class="d-inline-block style_padding_bottom__top_20 style_padding_left_right_10 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Миссия</div>
        <div class="d-inline-block style_padding_bottom__top_20 style_padding_left_right_10 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Руководство</div>
        <div class="d-inline-block style_padding_bottom__top_20 style_text_12_16 fw-bold style_padding_left_right_10 cursor_pointer style_border_radius_40 text-center style_width_max tab">География</div>
    </div>-->
    <div class="d-inline-block style_border_radius_40 style_accordion tabs margin_bottom_x2">
        <div class="d-inline-block style_text_12_16 fw-bold text-center style_border_radius_40 style_padding_10_20 cursor_pointer active_tab style_width_max tab">Информация</div>
        <div class="d-inline-block  style_padding_10_20 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Миссия</div>
        <div class="d-inline-block  style_padding_10_20 style_text_12_16 fw-bold cursor_pointer style_border_radius_40 text-center style_width_max tab">Руководство</div>
        <div class="d-inline-block  style_text_12_16 fw-bold style_padding_10_20 cursor_pointer style_border_radius_40 text-center style_width_max tab">География</div>
    </div>
    <div class="frame" id="frame">
        <ul class="slidee">
            <li class="">Информация</li>
            <li class="">Миссия</li>
            <li class="">Руководство</li>
            <li class="">География</li>
        </ul>
    </div>
    <div class="contents margin_top_x2">
        <div class="info">
            <span class="d-inline-flex fw-bold style_text_20_30 style_padding_bottom__top_20">Компания АЛИДИ является дистрибьютором №1 и одним из крупнейших поставщиков в торговые сети продукции известных международных производителей:</span>
            <div class="d-grid grid_layout_info margin_bottom_x2">
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo-pg.svg" srcset="img/logo_clients/logo-pg.svg 90w,
             img/logo_clients/logo-pg.svg 140w" sizes="(max-width: 1199px) 90px, 140px" alt="p&g">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo-nestle.svg" srcset="img/logo_clients/logo-nestle.svg 132w,
            img/logo_clients/logo-nestle.svg 200w" sizes="(max-width: 1199px) 132px, 200px" alt="nestle">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo-NestlePurina.svg" srcset="img/logo_clients/logo-NestlePurina.svg 132w,
            img/logo_clients/logo-NestlePurina.svg 200w" sizes="(max-width: 1199px) 132px, 200px" alt="purina">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo-nestlewaters.svg" srcset="img/logo_clients/logo-nestlewaters.svg 145w,
            img/logo_clients/logo-nestlewaters.svg 96w" sizes="(max-width: 1199px) 145px, 96px" alt="nestle waters">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo_mars.svg" srcset="img/logo_clients/logo_mars.svg 106w,
            img/logo_clients/logo_mars.svg 160w" sizes="(max-width: 1199px) 106px, 160px" alt="mars" width="100%">
                </div>
                <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray">
                    <img src="img/logo_clients/logo-wrigley.svg" srcset="img/logo_clients/logo-wrigley.svg 132w,
            img/logo_clients/logo-wrigley.svg 200w" sizes="(max-width: 1199px) 132px, 200px" alt="wrigley">
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">Факты о АЛИДИ</span>
            <div class="bg_blue bg_index__padding style_border_radius_20 color_white style_border_radius_40_only_desk height_150 d-flex flex-column justify-content-between margin_top_bottom_10 d-xl-none">
                <div class="bg_index__bg_icon icon_alidi_white">
                </div>
                <div class="padding_bottom">
                    <span class="bg_index__heading margin_bottom">Основана в 1992 году в Нижнем Новгороде</span>
                </div>
            </div>
            <div class="page-layout__content__grid_layout margin_bottom align-self-center">
                <div class="bg_blue bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-none d-xl-flex flex-column justify-content-between">
                    <div class="bg_index__bg_icon icon_alidi_white">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text color_white">Основана в 1992 году в Нижнем Новгороде</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between">
                    <div class="bg_index__bg_icon icon_ruble">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">Оборот в 2020 году 80 млрд рублей</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between">
                    <div class="bg_index__bg_icon icon_about_globe margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">Представлены в России, Беларуси и Казахстане</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between">
                    <div class="bg_index__bg_icon icon_crosshair margin_bottom">
                    </div>
                    <div class="padding_bottom ">
                        <span class="bg_index__text">54 региона, 18 складов , 30 филиалов</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_5operators margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">ТОП-5 крупнейших логистических операторов России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_forbs margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">ТОП-200 крупнейших частных компаний России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between">
                    <div class="bg_index__bg_icon icon_users margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">9000 квалифицированных сотрудников</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_superJob margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">ТОП-15 лучших работодателей России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 style_border_radius_40_only_desk d-flex flex-column justify-content-between ">
                    <div class="bg_index__bg_icon icon_hh margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__text">Лучший работадатель</span>
                    </div>
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20">Экосистема АЛИДИ включает в себя несколько направлений</span>
            <div class="page-layout__content__grid_layout margin_bottom align-self-center height_max">
                <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white  d-flex flex-column justify-content-between bg_index_add_business ">
                    <div class="bg_index__bg_icon icon_globe margin_bottom">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_top_bottom_10">Дистрибуция</span>
                        <span class="bg_index__text">Продукты питания, бытовая химия, косметика, корма и товары для животных крупнейших мировых производителей и товары для HoReCa</span>
                    </div>
                </div>
                <div class="bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_logistic  ">
                    <div class="bg_index__bg_icon icon_truck margin_bottom">
                    </div>
                    <div class="">
                        <span class="d-inline-flex bg_index__heading margin_bottom">Логистические услуги</span>
                        <span class="bg_index__text margin_bottom">Полный комплекс 3PL услуг. Складское хранение, грузообработка, доставка</span>
                    </div>
                </div>
                <div class="bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_about_sklad ">
                    <div class="bg_index__bg_icon icon_package">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Складские услуги</span>
                        <span class="bg_index__text">Общая площадь складских помещений АЛИДИ превышает 180 000м2</span>
                    </div>
                </div>
                <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_advance_business">
                    <div class="bg_index__bg_icon icon_megaphone">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Маркетинговое агентство</span>
                        <span class="bg_index__text">Предоставляет своим партнерам полный цикл рекламно-маркетинговых услуг</span>
                    </div>
                </div>
                <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_about_marketplace">
                    <div class="bg_index__bg_icon icon_handshake">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Маркетплейс</span>
                        <span class="bg_index__text">Цифровая платформа для любых поставщиков</span>
                    </div>
                </div>
                <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_horeca">
                    <div class="bg_index__bg_icon icon_bag">
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Professional</span>
                        <span class="bg_index__text">Поставщик продуктов питания для сегмента HoReCa</span>
                    </div>
                </div>
            </div>
            <span class="d-inline-block fw-bold style_text_24_36 padding_30_desk">История компании</span>
            <div class="grid_layout_history margin_bottom_x2 d-none d-xl-grid">
                <div class="bg_blue position_1992 padding_30_desk position_1992 style_border_radius_40  d-flex flex-column justify-content-between color_white">
                    <div class="">
                        <span class="style_text_60_60">1992</span>
                    </div>
                    <div>
                        <span>Основание компании АЛИДИ в Нижнем Новгороде, Россия</span>
                    </div>
                </div>
                <div class="bg_lightblue position_1993 padding_30_desk style_border_radius_40 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">1993</span>
                    </div>
                    <div>
                        <span>Начало сотрудничества с P&G</span>
                    </div>
                </div>
                <div class="bg_red_dots position_2001 padding_30_desk style_border_radius_40  d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2001</span>
                    </div>
                    <div class="">
                        <span>Начало сотрудничества с Nestle</span>
                    </div>
                </div>
                <div class="bg_grey position_2003 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2003 </span>
                    </div>
                    <div class="">
                        <span>10 филиалов в России</span>
                    </div>
                </div>
                <div class="bg_grey position_2005 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2005 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Строительство Логистического центра класса А в Нижнем Новгороде</span>
                    </div>
                </div>
                <div class="bg_grey position_2006 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2006 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Начало операций в Северо-Западном федеральном округе (приобретение компании «Союз-Квадро»)</span>
                    </div>
                </div>
                <div class="bg_blue_more_light position_2007 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2007 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Начало сотрудничества с Wrigley</span>
                    </div>
                </div>
                <div class="bg_grey position_2008 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2008 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">20 филиалов в России</span>
                    </div>
                </div>
                <div class="bg_grey position_2009 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2009 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Открытие федерального распределительного центра М.Видео в Нижнем Новгороде</span>
                    </div>
                </div>
                <div class="bg_violet position_2010 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2010 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Начало сотрудничества с компанией Mars</span>
                    </div>
                </div>
                <div class="bg_blue position_2011 padding_30_desk style_border_radius_40 color_white height_172 d-flex flex-column justify-content-between ">
                    <div class="">
                        <span class="style_text_60_60">2011 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Открытие филиала в Москве. Начало операций по обслуживанию Центрального распределительного центра М.Видео в Москве</span>
                    </div>
                </div>
                <div class="bg_grey position_2012 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2012 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Открытие филиала в Беларуси, АЛИДИ - эксклюзивный дистрибьютор P&G и Nestle на территории республики.</span>
                    </div>
                </div>
                <div class="bg_grey position_2014 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2014</span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Начало оказания услуг консультирования для P&G в 112 городах России</span>
                    </div>
                </div>
                <div class="bg_grey position_2015 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2015 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Старт федеральной промо-программ по продукции P&G в 50 городах России</span>
                    </div>
                </div>
                <div class="bg_grey position_2016 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2016 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Открытие филиала в Казахстане, АЛИДИ дистрибьютор – Mars и Wrigley в городах Алматы и Талдыкорган. Начало работы в сегменте HoReCа.</span>
                    </div>
                </div>
                <div class="bg_blue_more_light position_2017 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2017 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block style_border_radius_40 ">Начало работы по контрактам Nestle Waters</span>
                    </div>
                </div>
                <div class="bg_grey position_2018 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2018 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Интеграция с компанией ЦДК, открытие 12 новых филиалов</span>
                    </div>
                </div>
                <div class="bg_grey position_2019 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2019 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Интеграция с компанией Трансазия Лоджистик, расширение на 10 новых регионов.</span>
                    </div>
                </div>
                <div class="bg_grey position_2020 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2020 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Пережили 2020</span>
                    </div>
                </div>
                <div class="bg_grey position_2021 padding_30_desk style_border_radius_40 height_172 d-flex flex-column justify-content-between">
                    <div class="">
                        <span class="style_text_60_60">2021 </span>
                    </div>
                    <div class="">
                        <span class="d-inline-block ">Открывает первый цифровой дистрибьютор для бизнеса.</span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl1 d-xl-none">
                <div class="bg_blue bg_index__padding color_white style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">1992</span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Основана в Нижнем Новгороде</span>
                    </div>
                </div>
                <div class="bg_lightblue bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">1993</span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало сотрудничества с P&G</span>
                    </div>
                </div>
                <div class="bg_red_dots bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2001</span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало сотрудничества с Nestle</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2003 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">10 филиалов в России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2005 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Строительство Логистического центра класса А в Нижнем Новгороде</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2006 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало операций в Северо-Западном федеральном округе (приобретение компании «Союз-Квадро»)</span>
                    </div>
                </div>
                <div class="bg_blue_more_light bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2007 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало сотрудничества с Wrigley</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2008 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">20 филиалов в России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2009 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Открытие федерального распределительного центра М.Видео в Нижнем Новгороде</span>
                    </div>
                </div>
                <div class="bg_violet bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2010 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало сотрудничества с компанией Mars</span>
                    </div>
                </div>
                <div class="bg_blue bg_index__padding style_border_radius_20 color_white height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2011 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Открытие филиала в Москве. Начало операций по обслуживанию Центрального распределительного центра М.Видео в Москве</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2012 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Открытие филиала в Беларуси, АЛИДИ - эксклюзивный дистрибьютор P&G и Nestle на территории республики.</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2014</span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало оказания услуг консультирования для P&G в 112 городах России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2015 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Старт федеральной промо-программ по продукции P&G в 50 городах России</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2016 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Открытие филиала в Казахстане, АЛИДИ дистрибьютор – Mars и Wrigley в городах Алматы и Талдыкорган. Начало работы в сегменте HoReCа.</span>
                    </div>
                </div>
                <div class="bg_blue_more_light bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2017 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Начало работы по контрактам Nestle Waters</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2018 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Интеграция с компанией ЦДК, открытие 12 новых филиалов</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2019 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Интеграция с компанией Трансазия Лоджистик, расширение на 10 новых регионов.</span>
                    </div>
                </div>
                <div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2020 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Пережили 2020</span>
                    </div>
                </div><div class="bg_grey bg_index__padding style_border_radius_20 height_172 d-flex flex-column justify-content-between margin_top_bottom_10">
                    <div class="bg_index__bg_icon">
                        <span class="style_text_40_40">2021 </span>
                    </div>
                    <div class="padding_bottom">
                        <span class="bg_index__heading margin_bottom">Открывает первый цифровой дистрибьютор для бизнеса.</span>
                    </div>
                </div>
            </div>

            <span class="d-inline-block fw-bold style_text_24_36 style_padding_bottom__top_20">Документы</span>
            <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray style_padding_30">
                <div>
                    <span class="d-flex flex-row justify-content-start justify-content-start margin_bottom_x2">
                        <img class="align-self-start margin_right" src="img/iconFile.svg" alt="file" width="22">
                        <a href="documents_pdf/princhip.pdf" target="_blank" class="style_16_24 fw-bold">Принципы, декларируемые ГК АЛИДИ при осуществлении своей деятельности</a>
                    </span>
                </div>
                <div>
                    <span class="d-flex flex-row justify-content-between margin_bottom_x2">
                        <img class="align-self-start margin_right" src="img/iconFile.svg" alt="file" width="22">
                        <a href="documents_pdf/alidi-covid-19.pdf" target="_blank" class="style_16_24 fw-bold">06апреля 2020 года в компании утвержден «стандарт безопасной деятельности организации в целях противодействия распространения новой коронавирусной инфекции (covid-19)»</a>
                    </span>
                </div>
                <div>
                    <span class="d-flex flex-row justify-content-between">
                        <img class="align-self-start margin_right" src="img/iconFile.svg" alt="file" width="22">
                        <a href="documents_pdf/prikaz-new-alidi-15-05.pdf" target="_blank" class="style_16_24 fw-bold">15 мая 2020 года в компании утверждена «новая редакция стандарта безопасной деятельности гк АЛИДИ»</a>
                    </span>
                </div>
            </div>
            <span class="d-inline-flex fw-bold style_text_24_36 style_padding_bottom__top_20 margin_top_desk margin_bottom_1_desk">Отзывы</span>
            <div class="owl-carousel owl2">
                <div class="d-flex flex-column flex-xl-row justify-content-between">
                    <div class="d-flex flex-row justify-content-start margin_bottom height_max">
                        <div class="d-flex align-content-center justify-content-center style_border_radius_20 style_border_lightgray style_padding_18_20 margin_right_x2">
                            <img class="width_160_desk" src="img/logo_clients/upeco.svg" alt="upeco" width="96px">
                        </div>
                    </div>
                    <div class="style_gray_radius fw-bold style_padding_18_20 margin_bottom">
                <span>АЛИДИ - один из важнейших стратегических партнеров компании UPECO. На сегодняшний день мы можем смело утверждать, что за годы сотрудничества команда АЛИДИ заработала безупречную репутацию надежного партнера, в основе работы которого клиентоориентированный сервис и стремление
к лидерству в области логистики и дистрибуции.

В нашем активе – ряд успешно реализованных совместных проектов. Благодаря сотрудничеству мы расширили дистрибуцию продуктов UPECO не только на территории России, но и за ее пределами. При этом хочется отметить, что строить эффективный
и взаимовыгодный бизнес с АЛИДИ легко: бизнес-процессы выстроены и регламентированы,
а вся команда – профессионалы высокого уровня.  Мы благодарим команду АЛИДИ за постоянное совершенствование и эффективную работу. От всей души желаем укрепления лидерских позиций, процветания и расширения границ присутствия и надеемся на дальнейшее плодотворное сотрудничество!</span>
                    </div>
                </div>
                <div class="d-flex flex-column flex-xl-row justify-content-between">
                    <div class="d-flex flex-row justify-content-start margin_bottom height_max">
                        <div class="d-flex align-content-center justify-content-center style_border_radius_20 style_border_lightgray style_padding_18_20 margin_right_x2">
                            <img class="width_160_desk" src="img/logo_selgros.svg" alt="logo_selgros" width="96">
                        </div>
                    </div>
                    <div class="style_gray_radius fw-bold style_padding_18_20 margin_bottom">
                <span>ООО «Зельгрос» выражает признательность и благодарность своему партнеру компании АЛИДИ
    за качественное предоставление услуг в сфере логистики, профессионализм команды и гибкость в реакции на изменяющиеся запросы. Также хотим отметить открытость и прозрачность во взаимодействии
    с нашим партнером.

    Стабильное качество в выполнении логистических операций позволяет нашей компании сохранять необходимый уровень обслуживания в цепи поставок, снабжении торговой сети товарами, что позитивно отражается на наших отношениях с клиентами.

    Выражаем уверенность в сохранении сложившихся дружественных отношений и надеемся на дальнейшее взаимовыгодное и плодотворное сотрудничество.</span>
                    </div>
                </div>
                <div class="d-flex flex-column flex-xl-row justify-content-between">
                    <div class="d-flex flex-row justify-content-start margin_bottom height_max">
                        <div class="d-flex align-content-center justify-content-center style_border_radius_20 style_border_lightgray style_padding_18_20 margin_right_x2">
                            <img class="width_160_desk" src="img/logo_clients/bagi.svg" alt="bagi" width="96">
                        </div>
                        <!--верхние кнопки-->
                        <!--<div class="d-flex flex-row align-self-center">
                            <a href="#">
                                <div class="style_gray_radius icon_arrow_left margin_right_x2"></div>
                            </a>
                            <a href="#">
                                <div class="style_gray_radius icon_arrow_right"></div>
                            </a>
                        </div>-->
                    </div>
                    <div class="style_gray_radius fw-bold style_padding_18_20 margin_bottom">
                <span>ГК БАГИ и компания АЛИДИ тесно сотрудничают, начиная с 2013 года. Сегодня АЛИДИ поставляет нашу продукцию
в более чем 5000 торговых точек в семнадцати регионах России и в Белоруссии, стабильно обеспечивая высокий уровень клиентского сервиса.

Экспертиза АЛИДИ в области построения продаж сыграла значительную роль в расширении дистрибьюции БАГИ на федеральном уровне, налаживании взаимоотношений с крупнейшими торговыми сетями.

Я хочу поблагодарить сотрудников АЛИДИ
за профессионализм в повседневной работе, оперативное и всегда результативное решение самых нестандартных задач.

АЛИДИ – компания-лидер, стратегическое партнерство с которым, открывает новые перспективы развития на российском рынке. </span>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-evenly margin_bottom customNavigation">
            </div>
        </div>
        <div class="mission">
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom_x2">Цель нашей работы – предоставить партнерам высокий уровень сервиса, конкурентные цены, разнообразный ассортимент. Мы развиваем и улучшаем бизнес наших партнеров благодаря широкому масштабу операций и современным технологиям работы.</span>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Мы работаем, чтобы:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> предоставлять ведущим мировым производителям потребительских товаров услуги по логистической обработке, дистрибуции и продвижению</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">обеспечивать предприятия розничной торговли и конечных потребителей широким спектром товаров </span>
            </div>
            <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">ЧТО НАС ВДОХНОВЛЯЕТ на эффективную работу и достижение целей – наши корпоративные ценности.</span>
            <img class="width_335_mob" src="img/mission.svg" alt="picture_mission" width="660px"><br>
            <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">Команда</span>
            <span class="d-inline-block style_padding_18_20 bg_blue style_border_radius_20 style_border_radius_40_only_desk color_white margin_bottom">Главная ценность АЛИДИ – это люди. Профессионализм и отношение сотрудников к своей работе – залог долгосрочного успеха компании.</span>
            <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">Развитие</span>
            <span class="d-inline-block style_padding_18_20 bg_blue style_border_radius_20 style_border_radius_40_only_desk color_white margin_bottom">Мы работаем в бизнесе с высокой конкуренцией и всегда ставим перед собой высокие цели, с каждым днем решая все более сложные задачи. Мы должны постоянно искать пути для совершенствования, чтобы оставаться конкурентоспособными. Поэтому мы приветствуем желание сотрудников развиваться, чтобы соответствовать самым высоким профессиональным требованиям. </span>
            <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">Партнерство</span>
            <span class="d-inline-block style_padding_18_20 bg_blue style_border_radius_20 style_border_radius_40_only_desk color_white margin_bottom">Общаясь и взаимодействуя друг с другом, мы находим лучшие бизнес-решения, достигаем лучших результатов, обеспечиваем развитие компании и профессиональную преемственность сотрудников. </span>
            <span class="d-inline-block style_text_20_30 fw-bold margin_bottom">Принципы нашей работы:</span>
            <div class="d-flex flex-row justify-content-start margin_bottom_x2">
                <span class="d-inline-block style_16_24  margin_right">—</span>
                <span class="d-inline-block style_16_24">Расти бизнес сегодня! Завтра будет сложнее!</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">Всегда будь внимателен к нуждам клиентов</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Старайся сделать свою работу быстро и качественно, тогда ты вправе требовать того же от других</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">Уважение – ключ к пониманию коллег, клиентов и партнеров. Без понимания нет эффективности.
    Без эффективности нет прибыли</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24">Мы открыты для партнеров. Мы ценим их идеи и рассматриваем свой бизнес как неотъемлемую часть глобального бизнеса наших партнеров</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom">
                <span class="d-inline-flex style_16_24 margin_right">—</span>
                <span class="d-inline-flex style_16_24">Эффективно используй активы компании</span>
            </div>
            <div class="d-flex flex-row justify-content-start margin_bottom_x2">
                <span class="d-inline-flex style_16_24  margin_right">—</span>
                <span class="d-inline-flex style_16_24"> Много зарабатывает тот, кто достигает поставленных целей, а не тот, кто много работает. Находи время для отдыха! Находи время, чтобы подумать. Находи время для шутки!</span>
            </div>
        </div>
        <div class="leadership">
            <div class="d-xl-grid grid_layout_leadership margin_bottom">
                <a href="about_leadeship_Demchenkov.php" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk  style_border_radius_40_only_desk">
                    <img src="/img/leadership/15-11.png" alt="Пётр Демченков" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Пётр Демченков</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Генеральный директор ГК АЛИДИ</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-10.png" alt="Иван Сычев" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Иван Сычев</span>
                        <span class="d-inline block width_200_desk style_text_12_16 style_text_16_24_desk">Коммерческий директор макрорегион «АЛИДИ Москва»»</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-9.png"alt="Алексей Колегов" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Алексей Колегов</span>
                        <span class="d-inline block width_200_desk style_text_12_16 style_text_16_24_desk">Коммерческий директор макрорегиона
«АЛИДИ Первый» </span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-8.png" alt="Сергей Леоненко" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Сергей Леоненко</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Исполнительный директор направления «HoReCa»</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-7.png" alt="Денис Лобков" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Денис Лобков</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Директор по развитию розничных проектов</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-6.png" alt="Иван Солин" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Иван Солин</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Директор по логистике</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-5.png" alt="Олег Сурков" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Олег Сурков</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Исполнительный директор Макрорегиона «АЛИДИ Москва»</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-4.png" alt="Константин Старовойтов" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Константин Старовойтов</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Директор по информационным технологиям</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/13.png" alt="Максим Глазунов" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-between width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Максим Глазунов</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Заместитель генерального директора по безопасности и управлению рисками</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-3.png" alt="Павел Овчинников" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Павел Овчинников</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Финансовый директор</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-12.png" alt="Константин Минин" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Константин Минин</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Исполнительный директор макрорегиона «АЛИДИ Первый»</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-2.png" alt="Владимир Лотоцкий" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Владимир Лотоцкий</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Исполнительный директор макрорегиона «АЛИДИ Юг»</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15-1.png" alt="Петр Цубер" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-between width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Петр Цубер</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Исполнительный директор макрорегионов «АЛИДИ Беларусь» и «АЛИДИ Казахстан»</span>
                    </div>
                </a>
                <a href="#" class="d-flex flex-row flex-xl-column align-items-start align-items-xl-center justify-content-start style_border_radius_20 style_border_lightgray style_padding_10 margin_bottom padding_30_desk style_border_radius_40_only_desk ">
                    <img src="/img/leadership/15.png" alt="Иван Варенников" width="200" class="margin_right margin_right_0_desk margin_bottom_1_desk width_152_mob">
                    <div class="d-flex flex-column justify-content-start width_200_desk">
                        <span class="style_text_14_18 style_text_16_24_desk fw-bold margin_bottom">Иван Варенников</span>
                        <span class="style_text_12_16 style_text_16_24_desk">Директор по организационному развитию</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="geography">
            <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold margin_top_x2 margin_bottom_x2">АЛИДИ – дистрибьюторская и логистическая компания в России, Беларуси, Казахстане</span>
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right tabs1">
                <span class="d-inline-block auth_fiz style_padding_18_20 style_text_12_16 fw-bold text-center active_tab">Россия</span>
                <span class="d-inline-block auth_ur style_padding_18_20 style_text_12_16 fw-bold text-center">Беларусь</span>
                <span class="d-inline-block auth_ur style_padding_18_20 style_text_12_16 fw-bold text-center">Казахстан</span>
            </div>
            <div class="contents1">
                <div class="russia">
                    <!--<img src="img/mapRussia.png" alt="map with point" width="100%" class="margin_top_x2 margin_bottom_x2">-->
<!--нужен плагин с добавлением точек, карту можно задать через img в html, есть зум и на точки можно повесить переход на другую страницу-->
                    <div class="margin_bottom">
                        <a href="about_geography_msk.php" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Москва</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Санкт-Петербург</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Нижний Новгород</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Казань</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Воронеж</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Архангельск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Белгород</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Брянск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Великий Новгород</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                </div>
                <div class="belarus">
                    <img src="img/mapBelarus.png" alt="map with point" width="100%" class="margin_top_x2 margin_bottom_x2">
                    <div class="margin_bottom">
                        <a href="about_geography_msk.php" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Минск</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                </div>
                <div class="kazakhstan">
                    <img src="img/mapKazakhstan.png" alt="map with point" width="100%" class="margin_top_x2 margin_bottom_x2">
                    <div class="margin_bottom">
                        <a href="about_geography_msk.php" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Алматы</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                    <div class="margin_bottom">
                        <a href="#" class="d-flex flex-row justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Талдыкорган</span>
                            <div class="icon_arrow_right_small"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
