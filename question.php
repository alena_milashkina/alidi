<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20">
        <div>
            <span class="heading_24 margin_right">Задать вопрос</span>
        </div>
        <a href="contacts.php" class="close_menu margin_top_0"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <!--<span class="dropdown-el margin_bottom select_question" id="drop1">
            <input type="radio" name="cities" value="cities1" checked="checked" id="cities1"><label for="cities1">Тема вопроса</label>
            <input type="radio" name="cities" value="cities1" id="theme1"><label for="theme1">тема1</label>
            <input type="radio" name="cities" value="cities1" id="theme2"><label for="theme2">тема2</label>
            <input type="radio" name="cities" value="cities1" id="theme3"><label for="theme3">тема3</label>
            <input type="radio" name="cities" value="cities1" id="theme4"><label for="theme4">тема4</label>
            <input type="radio" name="cities" value="cities1" id="theme5"><label for="theme5">тема5</label>
         </span>-->
        <div class="d-flex flex-row justify-content-between margin_bottom style_width95">
            <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select">
                <option selected>Тема вопроса</option>
                <option value="1">тема1</option>
                <option value="2">тема2</option>
                <option value="3">тема3</option>
                <option value="4">тема4</option>
            </select>
        </div>
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Имя">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="tel" placeholder="Телефон для связи">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="email" placeholder="Email для связи">
        <textarea class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" rows="6" placeholder="Задайте вопрос"></textarea>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Получить ответ">
    </form>
</div>
<?php require('js.php'); ?>
</body>
</html>



