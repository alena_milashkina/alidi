
<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="open_menu d-flex flex-column justify-content-between">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="d-flex flex-row justify-content-between align-items-center">
                <a href="search.php" class="style_padding_30 "><img src="img/iconSearch.svg" width="20" alt="search"></a>
                <div class="margin_right_x15 style_gray_line fw-bold">
                    <img src="img/iconAccount.svg" width="20" alt="lk">
                    <span><a href="entrance.php">Войти</a></span>
                </div>
            </div>
            <a href="index.php" class="align-items-center close_menu"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
        </div>
        <div class="margin_bottom">
            <span class="dropdown-el" id="drop1">
                <input type="radio" name="cities" value="cities1" checked="checked" id="cities1"><label for="cities1">Выберите город</label>
                <input type="radio" name="cities" value="cities1" id="spb1"><label for="spb1">Санкт-Петербург</label>
                <input type="radio" name="cities" value="cities1" id="petroz1"><label for="petroz1">Петрозаводск</label>
                <input type="radio" name="cities" value="cities1" id="pskov1"><label for="pskov1">Псков</label>
                <input type="radio" name="cities" value="cities1" id="downNov1"><label for="downNov1">Нижний Новгород</label>
                <input type="radio" name="cities" value="cities1" id="voronesh1"><label for="voronesh1">Воронеж</label>
             </span>
        </div>
        <div class="d-flex flex-row justify-content-between style_gray_radius style_padding_30 margin_bottom fw-bold">
            <img src="img/iconBag.svg" class="margin_right" width="20" alt="basket">
            <span class="d-inline-flex me-auto">Корзина</span>
            <span><span>3895,70</span> &#8381</span>
        </div>
        <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom footer_text_mob">
            <ul class="open_menu__style_text_mob">
                <li><a href="catalog.php">Каталог</a></li>
                <li><a href="about_company.php">О компании</a></li>
                <li><a href="delivery_payment.php">Доставка и оплата</a></li>
                <li><a href="services.php">Услуги</a></li>
            </ul>
        </div>
        <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom footer_text_mob">
            <ul class="open_menu__style_text_mob">
                <li><a href="contacts.php">Контакты</a></li>
                <li><a href="press_center.php">Пресс-центр</a></li>
                <li><a href="help.php">Помощь</a></li>
                <li><a href="career.php">Карьера</a></li>
                <li><a href="question.php">Задать вопрос</a></li>
            </ul>
        </div>
        <div class="d-flex flex-column justify-content-between style_gray_radius color_bg_blue style_padding_30 open_menu__style_footer ">
            <div class="margin_bottom_x2">
                <span class="footer_phone_tip">Бесплатный звонок по России</span><br>
                <span class="footer_phone"><a href="tel:88007757500">8-800-775-75-00</a></span>
            </div>
            <div class="margin_bottom_x2">
                <div class="fab fa-instagram icon_style_white icon_size margin_right"></div>
                <div class="fab fa-facebook-f icon_style_white icon_size margin_right"></div>
                <div class="fab fa-tiktok icon_style_white icon_size margin_right"></div>
            </div>
        </div>
</div>
</div>
<?php require('js.php'); ?>
</body>
</html>

