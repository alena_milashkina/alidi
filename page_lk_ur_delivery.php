<div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
    <img src="img/iconUserRectangle.svg" width="20" alt="search" class="margin_right">
    <select class="form-select form-select-sm style_border_transparent input_date_rang fw-bold" aria-label=".form-select-lg example">
        <option selected>Все плательщики</option>
        <option value="1">адрес 1</option>
        <option value="2">адрес 2</option>
        <option value="3">адрес 3</option>
        <option value="4">адрес 4</option>
        <option value="5">адрес 5</option>
    </select>
</div>
<span class="style_16_24 style_text_24_36_desk d-inline-block fw-bold style_padding_18_20">Адреса</span>
<div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_radius_40_only_desk padding_30_desk style_border_lightgray">
    <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
        <span class="style_16_24 fw-bold">Кузьмоловский гп, заводская ул., д.3, корп. 2</span>
    </div>
    <div class="d-flex flex-row justify-content-start style_padding_bottom__top_20 ">
        <img class="align-self-baseline margin_right" src="img/iconAmbassador.svg" width="20" alt="Торговый представитель">
        <div class="d-flex flex-column justify-content-start style_width100">
            <span>Торговый представитель</span>
            <span class="style_16_24 fw-bold">Мацук Николай Николаевич</span>
            <span>+7 916 348-99-00</span>
            <span>matsuk@gmail.com</span>
        </div>
    </div>
</div>
<a href="add_address.php" class="d-block d-xl-none text-center style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10">Добавить адрес</a>
<a href="add_address.php" class="d-none d-xl-block style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold width_50_desk text-xl-center style_padding_10">Добавить адрес</a>
