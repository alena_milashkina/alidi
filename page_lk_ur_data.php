    <div class="d-flex flex-column justify-content-start margin_bottom">
        <span>Ваш статус</span>
        <span class="style_text_24_36 fw-bold color_blue">Мелкий оптовик</span>
        <span class="fw-bold">У Вас скидка 10%</span>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray width_55_desk">
        <label class="" for="notification_no-call">
            <input type="checkbox" name="notification_no-call" class="form-check-input margin_right" checked>
            Не звонить по заказу
        </label>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray width_55_desk">
        <label class="" for="notification_sms_only">
            <input type="checkbox" name="notification_sms_only" class="form-check-input margin_right" checked>
            Получать рассылку по email
        </label>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom width_55_desk">
        <select class="form-select form-select-lg style_16_24 style_border_radius_20 style_padding_18_20" aria-label=".form-select">
            <option selected>Временная зона</option>
            <option value="1">(+03:00 UTC) Moscow</option>
            <option value="2">(+04:00 UTC) Moscow</option>
            <option value="3">(+05:00 UTC) Moscow</option>
        </select>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 padding_30_desk margin_bottom_x2 style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">Организационно-правовая форма</span>
            <span>ИП</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">Наименование</span>
            <span>ИП Варнаков</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">ОГРН</span>
            <span>314505309900027</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">ИНН</span>
            <span>7811142520</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">КПП</span>
            <span>781401001</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">Юридический адрес</span>
            <span>Санкт-Петербург, Заозёрная ул., 8</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">Фактический адрес</span>
            <span>Санкт-Петербург, Заозёрная ул., 8</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24">Телефон</span>
            <span>+7 963 884-12-12</span>
        </div>
        <div class="d-flex flex-column flex-xl-row justify-content-between style_padding_bottom__top_10">
            <span class="style_16_24">Email</span>
            <span>varnakov_a@gmail.com</span>
        </div>
    </div>
    <span class="d-inline-block style_padding_10 margin_bottom_x2">Если Вы увидели ошибку, пожалуйста, свяжитесь с нашим контактным центром по номеру:
<a href="tel:88007757500">8-800-775-75-00</a></span>