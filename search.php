<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout__content__desk layout_grid_1col  input_date_rang">
    <div class="page-layout d-flex flex-column justify-content-between align-items-center style_padding_bottom_20">
        <div class="bg_white style_width100">
            <div class="d-flex flex-row justify-content-start margin_bottom margin_top_x2 style_padding_18_20 align-self-baseline  container">
                <div class="margin_right"><img src="img/iconSearch.svg" width="20" alt="search"></div>
                <input type="text" name="search" placeholder="Поиск" class="style_border_transparent align-self-start style_width95">
                <a href="index.php" class="close_menu margin_top_0 ms-auto"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
            </div>
        </div>
        <div class="input_date_rang container">
            <div class="d-flex flex-column justify-content-between margin_bottom style_padding_18_20 align-self-baseline style_width100 ">
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО куриное пищевое С1 экстра мытое 180 шт. Роскар</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО куриное пищевое С1 10 шт. Птицефабрика Оршанская</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО перепелиное 1*20 шт. Солигорская Птицефабрика</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО куриное пищевое С1 Ярково 25 шт. Роскар</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО куриное пищевое С1 экстра мытое 180 шт. Роскар</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО куриное пищевое С1 10 шт. Птицефабрика Оршанская</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО перепелиное 1*20 шт. Солигорская Птицефабрика</span>
                <span class="d-inline-block margin_bottom fw-bold">ЯЙЦО куриное пищевое С1 Ярково 25 шт. Роскар</span>
                <span class="d-inline-block margin_bottom fw-bold">Категория ЯЙЦО</span>
                <span class="d-inline-block margin_bottom fw-bold">Категория ЯЙЦО</span>
                <span class="d-inline-block margin_bottom fw-bold">Категория ЯЙЦО</span>
            </div>
            <span class="d-flex justify-content-center style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold width_300_desk"><a href="search_result.php">Смотреть все результаты</a></span>
        </div>
    </div>
</div>
<div class="page-layout__content_mob">
    <div class="page-layout d-flex flex-column justify-content-between align-items-center">
        <div class="d-flex flex-row justify-content-between margin_bottom style_padding_18_20 align-self-baseline style_width100 style_border_bottom">
            <div class="margin_right"><img src="img/iconSearch.svg" width="20" alt="search"></div>
            <input type="text" name="search" placeholder="Поиск" class="style_border_transparent align-self-start">
            <a href="index.php" class="close_menu margin_top_0"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
        </div>
        <div class="d-flex flex-column justify-content-between margin_bottom style_padding_18_20 align-self-baseline style_width100">
            <span class="d-inline-block margin_bottom">ЯЙЦО куриное пищевое С1 экстра мытое 180 шт. Роскар</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО куриное пищевое С1 10 шт. Птицефабрика Оршанская</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО перепелиное 1*20 шт. Солигорская Птицефабрика</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО куриное пищевое С1 Ярково 25 шт. Роскар</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО куриное пищевое С1 экстра мытое 180 шт. Роскар</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО куриное пищевое С1 10 шт. Птицефабрика Оршанская</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО перепелиное 1*20 шт. Солигорская Птицефабрика</span>
            <span class="d-inline-block margin_bottom">ЯЙЦО куриное пищевое С1 Ярково 25 шт. Роскар</span>
            <span class="d-inline-block margin_bottom">Категория ЯЙЦО</span>
            <span class="d-inline-block margin_bottom">Категория ЯЙЦО</span>
            <span class="d-inline-block margin_bottom">Категория ЯЙЦО</span>
        </div>
        <span class="d-flex justify-content-center style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95"><a href="search_result.php">Смотреть все результаты</a></span>
    </div>
</div>

<?php require('js.php'); ?>
</body>
</html>
