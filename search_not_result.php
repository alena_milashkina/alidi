<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class=" layout_grid_1col">
            <div class="">
                <div class="d-flex flex-row justify-content-between align-items-baseline">
                    <span class="d-inline-block heading_24  margin_right flex-fit">Результаты поиска</span>
                    <hr class="flex-fill align-self-start" style="opacity: 1; height: 2px; color: #000000;">
                </div>
                <span class="d-inline-block margin_bottom_x2 fw-bold">По вашему запросу ничего не найдено</span>
            </div>
<!--            <div class="">
                <?php /*require('footer.php'); */?>
            </div>-->
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

