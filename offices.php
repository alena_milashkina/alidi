<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?php
$result = Array();
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && CModule::IncludeModule("iblock")) {
  if (strpos($_SERVER["HTTP_REFERER"], '/en/') !== false || strpos($_SERVER["HTTP_REFERER"], '.com') !== false) {
    $lang_is_en = true;
  } else {
    $lang_is_en = false;
  }

  $rsData = CIBlockElement::GetList(array("SORT" => "ASC"),
    array("IBLOCK_ID" => 7),
    false,
    false,
    array("ID", "PREVIEW_PICTURE", "PROPERTY_OFFICE_NAME", "PROPERTY_OFFICE_NAME_EN", "PROPERTY_OFFICE_ADDRESS", "PROPERTY_OFFICE_ADDRESS_EN")
  );

  while ($rsItem = $rsData->GetNext()) {
    $tmp = Array();
    if ($lang_is_en) {
      if ($rsItem["PREVIEW_PICTURE"] && $rsItem["PROPERTY_OFFICE_ADDRESS_EN_VALUE"]) {
        $result[] = Array($rsItem["ID"], $rsItem["PREVIEW_PICTURE"], $rsItem["PROPERTY_OFFICE_NAME_EN_VALUE"], $rsItem["~PROPERTY_OFFICE_ADDRESS_EN_VALUE"]["TEXT"]);
      }
    } else {
      if ($rsItem["PREVIEW_PICTURE"] && $rsItem["PROPERTY_OFFICE_ADDRESS_VALUE"]) {
        $result[] = Array($rsItem["ID"], $rsItem["PREVIEW_PICTURE"], $rsItem["PROPERTY_OFFICE_NAME_VALUE"], $rsItem["~PROPERTY_OFFICE_ADDRESS_VALUE"]["TEXT"]);
      }
    }
  }
}

?>

<? if (count($result)): ?>
<div class="data-dynamic">

	<div id="alidi-map-hints">
    <? foreach ($result as $v): ?>
		<div class="alidi-hint alidi-map-point__hint" data-point-id="<?=$v[0]?>">
			<div class="alidi-hint__pic"><img src="<?=CFile::GetPath($v[1]);?>" alt="" title="" class="img alidi-hint__img"/></div>
			<div class="alidi-hint__cont">
        <? if ($v[2]): ?>
			    <div class="alidi-hint__title"><?=$v[2]?></div>
        <? endif ?>
				<div class="alidi-hint__text">
					<?=$v[3]?>
				</div>
			</div>
			<a href="#" class="alidi-hint__btn-close"><span class="icon"></span></a>
		</div>
  <? endforeach ?>
	</div>

</div>
<? endif ?>
