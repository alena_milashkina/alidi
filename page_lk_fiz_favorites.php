<div>
    <div class="d-block margin_bottom view_item2 item">
        <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
            <a href="item.php" class="position-relative in_stock_tip">
                <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                <img src="img/iconFavoritesActive.svg" alt="icon favorite active" width="20" class="d-block d-xl-none position-absolute top-0 end-0 align-self-end">
            </a>
            <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <img src="img/iconFavoritesActive.svg" alt="icon favorite active" width="20" class="d-none d-xl-block position_icon_fav_active_lk_fav align-self-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-block margin_bottom view_item2 item">
        <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
            <a href="item.php" class="position-relative in_stock_tip">
                <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                <img src="img/iconFavoritesActive.svg" alt="icon favorite active" width="20" class="d-block d-xl-none position-absolute top-0 end-0 align-self-end">
            </a>
            <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <img src="img/iconFavoritesActive.svg" alt="icon favorite active" width="20" class="d-none d-xl-block position_icon_fav_active_lk_fav align-self-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-block margin_bottom view_item2 item">
        <div class="d-flex flex-column flex-xl-row justify-content-between style_border_radius_20 style_border_radius_40_only_desk style_border_lightgray style_padding_18_20 margin_bottom">
            <a href="item.php" class="position-relative in_stock_tip">
                <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                <img src="img/iconFavoritesActive.svg" alt="icon favorite active" width="20" class="d-block d-xl-none position-absolute top-0 end-0 align-self-end">
            </a>
            <div class="d-xl-flex flex-xl-column justify-content-start align-content-xl-start">
                <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span><br>
                <a href="item.php" class="fw-bold margin_bottom_x2">СВИНИНА шея без кости с/м вес 3 кг.</a>
                <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                    <span>Цена за кг: 1 790,12 ₽</span>
                    <b class="d-inline-block margin_bottom">Цена за штуку: 5 370,36 ₽</b>
                </div>
            </div>
            <div class="d-xl-flex flex-xl-column justify-content-end align-content-xl-end">
                <img src="img/iconFavoritesActive.svg" alt="icon favorite active" width="20" class="d-none d-xl-block position_icon_fav_active_lk_fav align-self-end">
                <div class="margin_bottom">
                    <select class="form-select style_padding_12_20 style_border_transparent fw-bold color_white_bg_blue style_border_radius_40" aria-label=".form-select-lg">
                        <option selected>Заказ поштучно</option>
                        <option value="1">Заказ коробками (12 шт)</option>
                        <option value="2">Заказ блоками (36 шт)</option>
                    </select>
                </div>
                <span class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center add_item_basket cursor_pointer">В корзину</span>
                <div class="d-none d-flex flex-column justify-content-end align-content-xl-end count_item_basket">
                    <div class="d-flex flex-row justify-content-between input_date_rang style_border_radius_40 height_40 style_width100">
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count style_text_24_36">-</button>
                        <output class="output_count_basket style_padding_10 fw-bold" name="basketCount" id="basketCount">1</output>
                        <button class="style_text_30_30 color_white color_bg_blue style_border_transparent rounded-circle text-center btn_count">+</button>
                    </div>
                    <span class="d-inline-block style_width100 text-center fw-bold style_text_12_16">Итого: 108 кг</span>
                </div>
            </div>
        </div>
    </div>
</div>
