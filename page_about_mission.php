<div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
        <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">О компании</span>
        <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
    </div>
    <a href="about_info.php">Информация</a>
    <a href="about_mission.php">Миссия</a>
    <a href="about_leadership.php">Руководство</a>
    <a href="about_geography.php">География</a>
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom_x2">Цель нашей работы – предоставить партнерам высокий уровень сервиса, конкурентные цены, разнообразный ассортимент. Мы развиваем и улучшаем бизнес наших партнеров благодаря широкому масштабу операций и современным технологиям работы.</span>
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Мы работаем, чтобы:</span>
    <div class="d-flex flex-row justify-content-between margin_bottom_x2">
        <span class="d-inline-flex style_16_24  margin_right">—</span>
        <span class="d-inline-flex style_16_24"> предоставлять ведущим мировым производителям потребительских товаров услуги по логистической обработке, дистрибуции и продвижению</span>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom">
        <span class="d-inline-flex style_16_24 margin_right">—</span>
        <span class="d-inline-flex style_16_24">обеспечивать предприятия розничной торговли и конечных потребителей широким спектром товаров </span>
    </div>
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">ЧТО НАС ВДОХНОВЛЯЕТ на эффективную работу и достижение целей – наши корпоративные ценности.</span>
    <img class="" src="img/mission.svg" alt="picture_mission" width="350">
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Команда</span>
    <span class="d-inline-flex style_padding_18_20 bg_blue style_border_radius_20 color_white margin_bottom">Главная ценность АЛИДИ – это люди. Профессионализм и отношение сотрудников к своей работе – залог долгосрочного успеха компании.</span>
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Развитие</span>
    <span class="d-inline-flex style_padding_18_20 bg_blue style_border_radius_20 color_white margin_bottom">Мы работаем в бизнесе с высокой конкуренцией и всегда ставим перед собой высокие цели, с каждым днем решая все более сложные задачи. Мы должны постоянно искать пути для совершенствования, чтобы оставаться конкурентоспособными. Поэтому мы приветствуем желание сотрудников развиваться, чтобы соответствовать самым высоким профессиональным требованиям. </span>
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Партнерство</span>
    <span class="d-inline-flex style_padding_18_20 bg_blue style_border_radius_20 color_white margin_bottom">Общаясь и взаимодействуя друг с другом, мы находим лучшие бизнес-решения, достигаем лучших результатов, обеспечиваем развитие компании и профессиональную преемственность сотрудников. </span>
    <span class="d-inline-flex style_text_20_30 fw-bold margin_bottom">Принципы нашей работы:</span>
    <div class="d-flex flex-row justify-content-between margin_bottom_x2">
        <span class="d-inline-flex style_16_24  margin_right">—</span>
        <span class="d-inline-flex style_16_24">Расти бизнес сегодня! Завтра будет сложнее!</span>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom">
        <span class="d-inline-flex style_16_24 margin_right">—</span>
        <span class="d-inline-flex style_16_24">Всегда будь внимателен к нуждам клиентов</span>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom_x2">
        <span class="d-inline-flex style_16_24  margin_right">—</span>
        <span class="d-inline-flex style_16_24">Старайся сделать свою работу быстро и качественно, тогда ты вправе требовать того же от других</span>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom">
        <span class="d-inline-flex style_16_24 margin_right">—</span>
        <span class="d-inline-flex style_16_24">Уважение – ключ к пониманию коллег, клиентов и партнеров. Без понимания нет эффективности.
    Без эффективности нет прибыли</span>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom_x2">
        <span class="d-inline-flex style_16_24  margin_right">—</span>
        <span class="d-inline-flex style_16_24">Мы открыты для партнеров. Мы ценим их идеи и рассматриваем свой бизнес как неотъемлемую часть глобального бизнеса наших партнеров</span>
    </div>
    <div class="d-flex flex-row justify-content-start margin_bottom">
        <span class="d-inline-flex style_16_24 margin_right">—</span>
        <span class="d-inline-flex style_16_24">Эффективно используй активы компании</span>
    </div>
    <div class="d-flex flex-row justify-content-between margin_bottom_x2">
        <span class="d-inline-flex style_16_24  margin_right">—</span>
        <span class="d-inline-flex style_16_24"> Много зарабатывает тот, кто достигает поставленных целей, а не тот, кто много работает. Находи время для отдыха! Находи время, чтобы подумать. Находи время для шутки!</span>
    </div>
</div>
