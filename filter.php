<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20">
        <div>
            <span class="heading_24 margin_right">Фильтр</span>
        </div>
        <a href="catalog.php" class="close_menu"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <div class="margin_bottom style_width95">
            <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                <div class="d-flex flex-row justify-content-between header_accordion ">
                    <span class="d-inline-flex fw-bold">Бренд</span>
                    <div class="icon_arrow_down_small"></div>
                </div>
                <div class="d-inline-block content_accordion d-none margin_top">
                    <div class="style_border_lightgray style_border_radius_20 style_padding_10">
                        <img src="img/iconSearchBlack.svg" width="20">
                        <input class="style_gray_radius style_border_transparent" type="text">
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter1">
                            <input type="checkbox" name="filter1" class="form-check-input margin_right" checked>
                            Без бренда
                        </label>
                        <span>16</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter2">
                            <input type="checkbox" name="filter2" class="form-check-input margin_right">
                            PrimeBeef
                        </label>
                        <span>14</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter3">
                            <input type="checkbox" name="filter3" class="form-check-input margin_right">
                            Мит Кинг
                        </label>
                        <span>7</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter4">
                            <input type="checkbox" name="filter4" class="form-check-input margin_right">
                            Мираторг
                        </label>
                        <span>14</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter5">
                            <input type="checkbox" name="filter5" class="form-check-input margin_right">
                            Concepcion
                        </label>
                        <span>14</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="margin_bottom style_width95">
            <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                <div class="d-flex flex-row justify-content-between header_accordion ">
                    <span class="d-inline-flex fw-bold">Страна производитель</span>
                    <div class="icon_arrow_down_small"></div>
                </div>
                <div class="d-inline-block content_accordion d-none margin_top">
                    <div class="style_border_lightgray style_border_radius_20 style_padding_10">
                        <img src="img/iconSearchBlack.svg" width="20">
                        <input class="style_gray_radius style_border_transparent" type="text">
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter1">
                            <input type="checkbox" name="filter1" class="form-check-input margin_right" checked>
                            Россия
                        </label>
                        <span>16</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter2">
                            <input type="checkbox" name="filter2" class="form-check-input margin_right">
                            Италия
                        </label>
                        <span>14</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter3">
                            <input type="checkbox" name="filter3" class="form-check-input margin_right">
                            Беларусь
                        </label>
                        <span>7</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter4">
                            <input type="checkbox" name="filter4" class="form-check-input margin_right">
                            Украина
                        </label>
                        <span>14</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between style_padding_18_20 margin_bottom">
                        <label class="" for="filter5">
                            <input type="checkbox" name="filter5" class="form-check-input margin_right">
                            Казахстан
                        </label>
                        <span>14</span>
                    </div>
                </div>
            </div>
        </div>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95 position-sticky button_filter bg_white" type="submit" value="Применить">
    </form>
</div>
<?php require('js.php'); ?>
</body>
</html>


