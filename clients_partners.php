<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Клиенты и партнеры</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk margin_bottom_x2">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <?php require('page_client_partners.php'); ?>
        </div>
        <div class="page-layout__content_mob" >
            <?php require('page_client_partners.php'); ?>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

<!-- <div class="">
        <div class="page-layout__content">
            <?php /*require('page_client_partners.php'); */?>
        </div>
        <div class="">
            <?php /*require('footer.php'); */?>
        </div>
    </div>-->