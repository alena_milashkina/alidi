<div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
        <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Пресс-центр</span>
        <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center"><a href="press_center.php">Пресс-центр</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="blog.php">Блог</a></span>
        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="reviews.php">Отзывы</a></span>
    </div>
    <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
        <img src="img/logo_clients/selgros.svg" alt="selgros" width="96" class="">
    </div>
    <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">ООО «Зельгрос» выражает признательность и благодарность своему партнеру компании АЛИДИ
за качественное предоставление услуг в сфере логистики, профессионализм команды и гибкость в реакции на изменяющиеся запросы. Также хотим отметить открытость и прозрачность во взаимодействии
с нашим партнером.
<br><br>
Стабильное качество в выполнении логистических операций позволяет нашей компании сохранять необходимый уровень обслуживания в цепи поставок, снабжении торговой сети товарами, что позитивно отражается на наших отношениях с клиентами.
<br><br>
Выражаем уверенность в сохранении сложившихся дружественных отношений и надеемся на дальнейшее взаимовыгодное и плодотворное сотрудничество.</span>
    </div>
    <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
        <img src="img/logo_clients/upeco.svg" alt="upeco" width="96" class="">
    </div>
    <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">АЛИДИ - один из важнейших стратегических партнеров компании UPECO. На сегодняшний день мы можем смело утверждать, что за годы сотрудничества команда АЛИДИ заработала безупречную репутацию надежного партнера, в основе работы которого клиентоориентированный сервис и стремление
к лидерству в области логистики и дистрибуции.
<br><br>
В нашем активе – ряд успешно реализованных совместных проектов. Благодаря сотрудничеству мы расширили дистрибуцию продуктов UPECO не только на территории России, но и за ее пределами. При этом хочется отметить, что строить эффективный
и взаимовыгодный бизнес с АЛИДИ легко: бизнес-процессы выстроены и регламентированы,
а вся команда – профессионалы высокого уровня.  Мы благодарим команду АЛИДИ за постоянное совершенствование и эффективную работу. От всей души желаем укрепления лидерских позиций, процветания и расширения границ присутствия и надеемся на дальнейшее плодотворное сотрудничество!</span>
    </div>
    <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
        <img src="img/logo_clients/bagi.svg" alt="bagi" width="96" class="">
    </div>
    <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">ГК БАГИ и компания АЛИДИ тесно сотрудничают, начиная с 2013 года. Сегодня АЛИДИ поставляет нашу продукцию
в более чем 5000 торговых точек в семнадцати регионах России и в Белоруссии, стабильно обеспечивая высокий уровень клиентского сервиса.
<br><br>
Экспертиза АЛИДИ в области построения продаж сыграла значительную роль в расширении дистрибьюции БАГИ на федеральном уровне, налаживании взаимоотношений с крупнейшими торговыми сетями.
<br><br>
Я хочу поблагодарить сотрудников АЛИДИ
за профессионализм в повседневной работе, оперативное и всегда результативное решение самых нестандартных задач.
<br><br>
АЛИДИ – компания-лидер, стратегическое партнерство с которым, открывает новые перспективы развития на российском рынке. </span>
    </div>
    <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
        <img src="img/logo_clients/konti.svg" alt="upeco" width="96" class="">
    </div>
    <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">Работа компании АЛИДИ характеризуется отлично налаженными процессами работы с партнерами, взаимовыгодными условиями сотрудничества. Сотрудники компании в любых рабочих ситуациях проявляют себя как профессионалы своего дела, всегда открыты к диалогу и готовы оперативно решить любую, даже самую нестандартную, задачу.
<br><br>
Оперативность реагирования на все запросы, нацеленность на результат, выполнение
и перевыполнение планов, наращивание объемов продаж характеризуют компанию АЛИДИ как надежного партнера и компанию-лидера.
<br><br>
АЛИДИ, работающая на рынке дистрибуции более 20 лет, является экспертом в области построения продаж на региональном и федеральном уровнях, налаживания взаимоотношений с торговыми сетями. </span>
    </div>
    <div class="d-grid align-content-center justify-content-center style_border_radius_20 style_border_lightgray margin_bottom height_150">
        <img src="img/logo_clients/baby_line.svg" alt="baby_line" width="96" class="">
    </div>
    <div class="style_gray_radius style_padding_18_20 margin_bottom_x2">
        <span class="fw-bolder">Компания АЛИДИ является нашим деловым партнёром и дистрибьютором торговых марок BabyLine, FeedBack и Липуня с 2012 года.
<br><br>
За время совместной работы, сотрудники компании АЛИДИ показали себя высокопрофессиональными специалистами в сфере дистрибуции, трудолюбивыми
и ответственными, способными оперативно принимать решения, умеющими креативно мыслить и идти в ногу со временем.
<br><br>
Следует отметить особое отношение компании АЛИДИ
к своим деловым партнёрам и клиентам. Честность и прозрачность в работе делают эти отношения доверительными, широчайшая клиентская база даёт бескрайние возможности развития, стабильность и оснащение современными технологиями дают уверенность в завтрашнем дне, что всегда приводит к наилучшим результатам.</span>
    </div>
</div>