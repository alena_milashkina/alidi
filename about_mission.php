<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="page-layout__content">
            <?php require('page_about_mission.php'); ?>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

