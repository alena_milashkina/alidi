<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Пресс-центр</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <div>
                <div class="page-layout__content">
                    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                        <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center"><a href="press_center.php">Пресс-центр</a></span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="blog.php">Блог</a></span>
                        <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="reviews.php">Отзывы</a></span>
                    </div>
                    <span class="d-inline-block style_text_24_36 fw-bold margin_top_x2 margin_bottom_x2">Служба поддержки ALIDI PROF по работе с ветеринарными справками и системой Меркурий</span>
                    <img class="margin_bottom_x2" src="img/blog2.png" width="100%" alt="blog_promo">
                    <span class="d-inline-block margin_bottom">Компания ALIDI PROF работает исключительно в рамках законов Российской Федерации.
<br><br>
В связи с этим, все товары, подлежащие ветеринарной сертификации мы регистрируем в системе "Меркурий".
<br><br>
Меркурий - автоматизированная система, предназначенная для электронной сертификации поднадзорных госветнадзору грузов, отслеживания пути их перемещения по территории Российской Федерации в целях создания единой информационной среды для ветеринарии, повышения биологической и пищевой безопасности.
<br><br>
Для всех вопросов по ветеринарным справкам на продукты питания, доставленные нашей компанией, а также, по вопросам и трудностям в работе с системой "Меркурий", мы создали круглосуточную службу поддержки для клиентов.
<br><br>
Для связи, воспользуйтесь телефоном 8 (905) 664-86-27 или напишите на почту: vetdoc@alidi.ru. Режим работы 24/7.
<br><br>
С заботой о Вас и вашем бизнесе! </span>
                    <a href="blog.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold width_200_desk style_padding_10 d-grid align-content-center justify-content-center">Весь блог</a>
                </div>
            </div>
        </div>
        <div class="page-layout__content_mob">
            <div class="page-layout__content">
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion margin_left_right">
                    <span class="d-inline-block auth_fiz style_padding_bottom__top_20 style_text_12_16 fw-bold text-center"><a href="press_center.php">Пресс-центр</a></span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 fw-bold text-center active_tab"><a href="blog.php">Блог</a></span>
                    <span class="d-inline-block auth_ur style_padding_bottom__top_20 style_text_12_16 style_text_18_30_desk fw-bold text-center"><a href="reviews.php">Отзывы</a></span>
                </div>
                <span class="d-inline-block style_text_20_30 fw-bold margin_bottom margin_top_x2">Служба поддержки ALIDI PROF по работе с ветеринарными справками и системой Меркурий</span>
                <img class="margin_bottom" src="img/blog2.png" width="335" alt="blog_promo">
                <span class="d-inline-block margin_bottom">Компания ALIDI PROF работает исключительно в рамках законов Российской Федерации.
<br><br>
В связи с этим, все товары, подлежащие ветеринарной сертификации мы регистрируем в системе "Меркурий".
<br><br>
Меркурий - автоматизированная система, предназначенная для электронной сертификации поднадзорных госветнадзору грузов, отслеживания пути их перемещения по территории Российской Федерации в целях создания единой информационной среды для ветеринарии, повышения биологической и пищевой безопасности.
<br><br>
Для всех вопросов по ветеринарным справкам на продукты питания, доставленные нашей компанией, а также, по вопросам и трудностям в работе с системой "Меркурий", мы создали круглосуточную службу поддержки для клиентов.
<br><br>
Для связи, воспользуйтесь телефоном 8 (905) 664-86-27 или напишите на почту: vetdoc@alidi.ru. Режим работы 24/7.
<br><br>
С заботой о Вас и вашем бизнесе! </span>
                <a href="blog.php" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 d-grid align-content-center justify-content-center">Весь блог</a>
            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

