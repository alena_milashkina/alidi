<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="page-layout__content d-flex flex-column align-content-center justify-content-center align-items-center">
            <div class="bg_grey style_border_radius_20 d-grid align-content-center justify-content-center text-center height_200 margin_bottom_x2 width_560_desk width_355_mob">
                <span class="d-inline-block fw-bold style_text_24_36">Спасибо!</span>
                <span class="d-inline-block fw-bold">Ваш заказ оформлен.<br>Номер заказа <span>466</span></span>
            </div>
            <a href="#" class="style_blue_radious style_btn_blue margin_bottom_x2 style_16_24 fw-bold style_padding_10 d-grid align-content-center justify-content-center width_360_desk style_width100">Перейти в заказы</a>
        </div>
    </div>
    <div class="">
        <?php require('footer.php'); ?>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

