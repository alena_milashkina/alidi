<div class="page-layout__content_mob">
    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_30 margin_bottom footer_text_mob">
        <ul class="open_menu__style_text_mob">
            <li><a href="about_info.php" >О компании</a></li>
            <li><a href="services.php">Услуги</a></li>
            <li><a href="clients_partners.php">Клиенты и партнеры</a></li>
            <li><a href="press_center.php">Пресс-центр</a></li>
            <li><a href="career.php">Карьера</a></li>
            <li><a href="contacts.php">Контакты</a></li>
            <li><a href="help.php">Помощь</a></li>
        </ul>
    </div>
</div>
<div class="page-layout__content_desk">
    <div class="d-flex flex-column justify-content-between style_gray_radius style_border_radius_40 style_padding_18_20">
        <ul class="open_menu__style_text_desk">
            <li><a href="about_info.php" class="fw-bold">О компании</a></li>
            <li><a href="services.php">Услуги</a></li>
            <li><a href="clients_partners.php">Клиенты и партнеры</a></li>
            <li><a href="press_center.php">Пресс-центр</a></li>
            <li><a href="career.php">Карьера</a></li>
            <li><a href="contacts.php">Контакты</a></li>
            <li><a href="help.php">Помощь</a></li>
        </ul>
    </div>
</div>