<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20">
        <div>
            <span class="heading_24 margin_right">Узнать о поступлении</span>
        </div>
        <a href="item.php" class="close_menu"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_border_lightgray style_width95">
            <label for="email" class="style_text_12_16 color_gray">Email</label>
            <input name="email" type="email" class="style_border_transparent">
        </div>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95 position-sticky button_filter bg_white" type="submit" value="Подписаться">
    </form>
</div>
<?php require('js.php'); ?>
</body>
</html>


