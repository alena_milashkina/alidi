<div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk padding_30_desk position-relative">
        <span class="style_text_12_16 color_gray">Получатель</span>
        <span class="style_16_24 margin_bottom">Овечкин Александр Михайлович</span>
        <span class="style_text_12_16 color_gray">Адрес</span>
        <span class="style_16_24 fw-bold margin_bottom">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
        <span class="style_text_12_16 color_gray">Телефон</span>
        <span class="style_16_24 padding_bottom style_border_bottom border_bottom_transparent_desk color_black">+7 925 982-33-77</span>
        <div class="d-flex flex-row align-content-center justify-content-center style_padding_top_18 d-xl-none">
            <a href="refactor_all.php"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            <img class="cursor_pointer delete_js" src="img/iconDelete.svg" width="25" alt="delete">
        </div>
        <div class="d-none d-xl-flex flex-row align-content-center justify-content-center style_padding_top_18 position-absolute top-0 end-2">
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_refactor_all"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_delete_address"><img class="cursor_pointer delete_js" src="img/iconDelete.svg" width="25" alt="delete"></a>
        </div>
    </div>
    <a href="add_address.php" class="d-inline-block text-center style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 width_50_desk margin_bottom_x2">Добавить адрес получателя</a>
</div>
<div class="bg_mask">
    <div class="confirmation_delete_js style_border_radius_20">
        <div class="d-flex flex-column justify-content-center align-items-center style_padding_30">
            <span class="style_text_20_30 fw-bold margin_bottom">Удалить получателя и адрес?</span>
            <button class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10">Удалить</button>
            <span class="style_16_24 fw-bold cancel_popup_js cursor_pointer">Отменить</span>
        </div>
    </div>
</div>