<a href="loyalty.php" class="d-block d-xl-none margin_bottom_x2"><img src="img/banner.png" class="style_border_radius_20" alt="banner alidi coin" width="100%"></a>

<div class="banner_desktop margin_bottom_x2 position-relative">
    <div class="owl-carousel owl6">
        <a href="loyalty.php"><img src="img/banner.png" alt="banner alidi coin" width="100%" class="style_border_radius_40"></a>
        <a href="loyalty.php"><img src="img/banner.png" alt="banner alidi coin" width="100%" class="style_border_radius_40"></a>
    </div>
    <div class="customNavigation button_slider_index position-absolute"></div>
</div>
<div class="page-layout__content__grid_layout_index margin_bottom align-self-center">
    <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_horeca ">
        <div class="bg_index__bg_icon icon_bag">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__heading margin_bottom">Купить продукты для HoReCa</span>
            <span class="bg_index__text">Станем вашим партнером в HoReCa. 3500 товаров на уровне 2019 года!</span>
        </div>
    </div>
    <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_add_business">
        <div class="bg_index__bg_icon  icon_globe">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__heading margin_bottom">Подключить мой бизнес</span>
            <span class="bg_index__text margin_bottom">Если ты производитель  мы поможем продать!</span>
        </div>
    </div>
    <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_advance_business">
        <div class="bg_index__bg_icon  icon_megaphone">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__heading margin_bottom">Продвинуть мой бизнес</span>
            <span class="bg_index__text">Сделаем все что бы после производства  о вашем продукте знал клиент</span>
        </div>
    </div>
    <div class="bg_index bg_index__padding style_border_radius_20 style_border_radius_40_only_desk color_white d-flex flex-column justify-content-between bg_index_logistic">
        <div class="bg_index__bg_icon icon_truck">
        </div>
        <div class="padding_bottom">
            <span class="bg_index__heading margin_bottom">Улучшить логистические позиции</span>
            <span class="bg_index__text">Предоставим 3PL услуги, и поможем с хранением и доставкой</span>
        </div>
    </div>
</div>
