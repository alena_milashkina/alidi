<div class="page-layout__footer">
    <div class="footer_logo">
        <img src="img/logoWhite.svg" width="108">
        <span class="footer_copyright_desktop style_text_16_24_desk">© 2021 Alidi </span>
    </div>
    <div>
        <div class="margin_bottom_x2">
            <span class="footer_phone_tip">Бесплатный звонок по России</span><br>
            <span class="footer_phone style_text_20_30"><a href="tel:88007757500">8-800-775-75-00</a></span>
        </div>
        <div class="margin_bottom_x2">
            <div class="fab fa-instagram icon_style_white icon_size margin_right"></div>
            <div class="fab fa-facebook-f icon_style_white icon_size margin_right"></div>
            <div class="fab fa-tiktok icon_style_white icon_size margin_right"></div>
        </div>
    </div>
    <ul class="footer_text_desktop">
        <li><a href="catalog.php">Каталог</a></li>
        <li><a href="about_info.php">О компании</a></li>
        <li><a href="delivery_payment.php">Доставка и оплата</a></li>
        <li><a href="services.php">Услуги</a></li>
        <li><a href="loyalty.php">Система лояльности</a></li>
    </ul>
    <ul class="footer_text_desktop">
        <li><a href="contacts.php">Контакты</a></li>
        <li><a href="press_center.php">Пресс-центр</a></li>
        <li><a href="help.php">Помощь</a></li>
        <li><a href="career.php">Карьера</a></li>
        <li><span data-bs-toggle="modal" data-bs-target="#modal_question">Задать вопрос</span></li>
    </ul>
    <div class="footer_text_mob">
        <ul>
            <li><a href="catalog.php">Каталог</a></li>
            <li><a href="about_info.php">О компании</a></li>
            <li><a href="delivery_payment.php">Доставка и оплата</a></li>
            <li><a href="services.php">Услуги</a></li>
            <li><a href="loyalty.php">Система лояльности</a></li>
        </ul>
        <ul>
            <li><a href="contacts.php">Контакты</a></li>
            <li><a href="press_center.php">Пресс-центр</a></li>
            <li><a href="help.php">Помощь</a></li>
            <li><a href="career.php">Карьера</a></li>
            <li><a href="question.php">Задать вопрос</a></li>
        </ul>
    </div>
    <span class="footer_copyright_mob">© 2021 Alidi </span>
</div>
<div class="footer_bottom margin_bottom style_16_24">
    <span>Поставщик продуктов питания для сегмента HoReCa в Санкт-Петербурге, Ленинградской области, Великом Новгороде, Пскове и Петрозаводске. </span>
</div>

<!-- Modal -->

<div class="modal fade" id="entrance" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <!-- entrance -->
            <div class="page-layout margin_bottom modal_entrance">
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                    <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Вход</span>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" class="d-flex flex-column justify-content-between align-items-center">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="email" placeholder="Email">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="password" placeholder="Пароль">
                    <span class="cursor_pointer style_16_24 fw-bold margin_bottom open_forgot_password">Забыли пароль?</span>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Войти">
                    <span class="cursor_pointer style_16_24 fw-bold open_auth">Зарегистироваться</span>
                </form>
            </div>

            <!--auth-->
            <div class="page-layout margin_bottom modal_auth d-none">
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                    <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Регистрация</span>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion accordion margin_left_right">
                    <span class="d-inline-block auth_fiz style_padding_18_20 style_text_12_16 fw-bold text-center accordion_active">Физическое лицо</span>
                    <span class="d-inline-block auth_ur style_padding_18_20 style_text_12_16 fw-bold text-center">Юридическое лицо</span>
                </div>
                <form method="post" class="d-flex flex-column justify-content-start align-items-center form_fiz">
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="name" class="style_text_12_16 color_gray">ФИО</label>
                        <input name="name" type="text" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="phone" class="style_text_12_16 color_gray">Телефон</label>
                        <input name="phone" type="tel" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="email" class="style_text_12_16 color_gray">Email</label>
                        <input name="email" type="email" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="password" class="style_text_12_16 color_gray">Пароль</label>
                        <input name="password" class="style_border_transparent" type="password">
                    </div>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Зарегистрироваться">
                    <span class="style_text_12_16 style_width95 margin_bottom text-center fw-normal">Регистрируясь на нашем сайте, Вы даете согласие с условиями <span class="color__lightblue style_width95"><a href="#">пользовательского соглашения</a></span></span>
                    <span class="cursor_pointer style_16_24 fw-bold margin_bottom open_entrance">Войти с паролем</span>
                </form>
                <form method="post" class="d-flex flex-column justify-content-between align-items-center form_ur d-none">
                    <div class="d-flex flex-row justify-content-between margin_bottom style_width95">
                        <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select">
                            <option selected>Организационно-правовая форма</option>
                            <option value="1">ИП</option>
                            <option value="2">ООО</option>
                            <option value="3">ОАО</option>
                        </select>
                    </div>
                    <div class="d-flex flex-row justify-content-between margin_bottom style_width95">
                        <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select">
                            <option selected>Категория клента</option>
                            <option value="1">категория1</option>
                            <option value="2">категория2</option>
                            <option value="3">категория3</option>
                        </select>
                    </div>
                    <div class="d-flex flex-row justify-content-between margin_bottom style_width95">
                        <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select" multiple >
                            <option selected>Кулинарные концепции</option>
                            <option value="1">Япония</option>
                            <option value="2">Паназия</option>
                            <option value="3">Италия</option>
                            <option value="4">Европейская</option>
                            <option value="5">Мексиканская</option>
                        </select>
                    </div>
                    <div class="margin_bottom_x2 style_width95">
                        <span class="d-inline-block margin_bottom_x2">Укажите объем закупок тыс. ₽/год</span>
                        <input type="hidden" class="slider-input margin_top_bottom_10" value="23"/>
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="name_org" class="style_text_12_16 color_gray">Наименование</label>
                        <input name="name_org" type="text" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="ogrn" class="style_text_12_16 color_gray">ОГРН</label>
                        <input name="ogrn" type="number" step="1" min="1" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="inn" class="style_text_12_16 color_gray">ИНН</label>
                        <input name="inn" type="number" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="kpp" class="style_text_12_16 color_gray">КПП</label>
                        <input name="kpp" type="number" class="style_border_transparent">
                    </div>
                    <!--<input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Наименование">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="number" placeholder="ОГРН">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="number" placeholder="ИНН">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="number" placeholder="КПП">-->
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="UrAddress" class="style_text_12_16 color_gray">Юридический адрес</label>
                        <input name="UrAddress" type="text" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="factAddress" class="style_text_12_16 color_gray">Фактический адрес</label>
                        <input name="factAddress" type="text" class="style_border_transparent">
                    </div>
                    <!--<input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Юридический адрес">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Фактический адрес">-->
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="name" class="style_text_12_16 color_gray">ФИО представителя</label>
                        <input name="name" type="text" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="phone" class="style_text_12_16 color_gray">Телефон</label>
                        <input name="phone" type="tel" class="style_border_transparent" placeholder="+7 000 000 00 00">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="email" class="style_text_12_16 color_gray">Email</label>
                        <input name="email" type="email" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="password" class="style_text_12_16 color_gray">Пароль</label>
                        <input name="password" class="style_border_transparent" type="password">
                    </div>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Зарегистрироваться">
                    <span class="style_text_12_16 style_width95 margin_bottom text-center fw-normal">Регистрируясь на нашем сайте, Вы даете согласие с условиями <span class="color__lightblue style_width95"><a href="#">пользовательского соглашения</a></span></span>
                    <span class="cursor_pointer style_16_24 fw-bold margin_bottom open_entrance">Войти с паролем</span>
                </form>
            </div>

            <!--forgot password-->
            <div class="page-layout margin_bottom modal_forgot_password d-none">
                <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                    <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Восстановление пароля</span>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" class="d-flex flex-column justify-content-between align-items-center">
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95 email_forgot_password">
                        <label for="email" class="style_text_12_16 color_gray">Email</label>
                        <input name="email" type="email" required class="style_border_transparent">
                    </div>
                    <span class="d-grid margin_bottom style_width95 text-center fw-bold text_success d-none">На адрес buyer@gmail.com отправлено письмо с новым паролем</span>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95 position-sticky button_filter bg_white" type="submit" value="Отправить новый пароль">
                    <span class="cursor_pointer style_16_24 fw-bold come_back">Вернуться назад</span>
                </form> <!--if form post .come_back and .email_forgot_password add class d-none and .text_success remove d-none-->
            </div>
        </div>
    </div>
</div>

<!--refactor lk data single-->
<div class="modal fade" id="modal_refactor_single" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Редактирование</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" class="d-flex flex-column justify-content-start align-items-center form_fiz">
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="tel" class="style_text_12_16 color_gray">Телефон</label>
                        <input name="tel" type="tel" class="style_border_transparent">
                    </div>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Сохранить">
                    <span class="style_16_24 fw-bold"><a href="lk_fiz_data.php">Отменить</a></span>
                </form>
            </div>
        </div>
    </div>
</div>
<!--refactor lk all-->
<div class="modal fade" id="modal_refactor_all" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Редактирование</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" class="d-flex flex-column justify-content-start align-items-center form_fiz">
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="name" class="style_text_12_16 color_gray">Получатель</label>
                        <input name="name" type="text" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="address" class="style_text_12_16 color_gray">Адрес</label>
                        <input name="address" type="text" class="style_border_transparent">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <label for="tel" class="style_text_12_16 color_gray">Телефон</label>
                        <input name="tel" type="tel" class="style_border_transparent">
                    </div>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Сохранить">
                    <span class="style_16_24 fw-bold"><a href="lk_fiz_data.php">Отменить</a></span>
                </form>
            </div>
        </div>
    </div>
</div>
<!--new address -->
<div class="modal fade" id="modal_add_address" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Новый адрес</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" class="d-flex flex-column justify-content-start align-items-center form_fiz">
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <!--<label for="name" class="style_text_12_16 color_gray">Получатель</label>-->
                        <input name="name" type="text" class="style_border_transparent" placeholder="ФИО получателя">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <!--<label for="address" class="style_text_12_16 color_gray">Адрес</label>-->
                        <input name="address" type="text" class="style_border_transparent" placeholder="Адрес">
                    </div>
                    <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
                        <!--<label for="tel" class="style_text_12_16 color_gray">Телефон</label>-->
                        <input name="tel" type="tel" class="style_border_transparent" placeholder="Телефон">
                    </div>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Сохранить">
                    <span class="style_16_24 fw-bold"><a href="lk_fiz_data.php">Отменить</a></span>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- delete address desk-->
<div class="modal fade" id="modal_delete_address" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Удалить получателя и адрес?</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="style_border_radius_20">
                    <div class="d-flex flex-column justify-content-center align-items-center style_padding_30">
                        <button class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10">Удалить</button>
                        <span class="style_16_24 fw-bold cancel_popup_js cursor_pointer">Отменить</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- delete address mob-->
<div class="modal fade" id="modal_delete_address_mob" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_20 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center text-center margin_bottom style_text_20_30">
                <span class="d-inline-block fw-bold">Удалить<br> получателя и адрес?</span>
            </div>
            <div class="modal-body">
                <div class="style_border_radius_20">
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <button class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10">Удалить</button>
                        <span data-bs-dismiss="modal" aria-label="Close" class="style_16_24 fw-bold cancel_popup_js cursor_pointer">Отменить</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- модальное окно узнать о поступлении -->
<div class="modal fade" id="modal_item_in_stock" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Узнать о поступлении</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="style_border_radius_20">
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95 email_forgot_password">
                            <label for="email" class="style_text_12_16 color_gray">Email</label>
                            <input name="email" type="email" required class="style_border_transparent">
                        </div>
                        <button class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10">Подписаться</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- модальное окно Узнать о снижении цены -->
<div class="modal fade" id="modal_item_price_low" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Узнать о снижении цены</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="style_border_radius_20">
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95 email_forgot_password">
                            <label for="email" class="style_text_12_16 color_gray">Email</label>
                            <input name="email" type="email" required class="style_border_transparent">
                        </div>
                        <button class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10">Подписаться</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- модальное окно  задать вопрос -->
<div class="modal fade" id="modal_question" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Задать вопрос</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <span class="dropdown-el margin_bottom select_question" id="drop1">
            <input type="radio" name="cities" value="cities1" checked="checked" id="cities1"><label for="cities1">Тема вопроса</label>
            <input type="radio" name="cities" value="cities1" id="theme1"><label for="theme1">тема1</label>
            <input type="radio" name="cities" value="cities1" id="theme2"><label for="theme2">тема2</label>
            <input type="radio" name="cities" value="cities1" id="theme3"><label for="theme3">тема3</label>
            <input type="radio" name="cities" value="cities1" id="theme4"><label for="theme4">тема4</label>
            <input type="radio" name="cities" value="cities1" id="theme5"><label for="theme5">тема5</label>
         </span>
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="ФИО">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="tel" placeholder="Телефон">
                    <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="email" placeholder="Email">
                    <textarea class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" rows="6" placeholder="Задайте вопрос"></textarea>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Получить ответ">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- модальное окно   оставить отзыв -->
<div class="modal fade" id="modal_add_review" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content style_border_radius_40 style_padding_30 style_min_width_560">
            <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 style_text_24_36">
                <span class="d-inline-block fw-bold margin_right me-auto ms-auto">Отзыв</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" class="d-flex flex-column justify-content-between align-items-center">
                    <span class="d-inline-block margin_bottom_x2 fw-bold">Оцените покупку</span>
                    <div class="d-flex flex-row justify-content-between margin_bottom_x2">
                        <span class="iconStar_40 iconStar_active_40"></span>
                        <span class="iconStar_40 iconStar_active_40"></span>
                        <span class="iconStar_40 iconStar_active_40"></span>
                        <span class="iconStar_40"></span>
                        <span class="iconStar_40"></span>
                    </div>
                    <textarea class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" placeholder="Ваш отзыв" rows="6"></textarea>
                    <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95 position-sticky button_filter bg_white" type="submit" value="Отправить">
                </form>
            </div>
        </div>
    </div>
</div>
