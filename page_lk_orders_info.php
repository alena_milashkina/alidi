<div>
    <div class="d-flex flex-column justify-content-between margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <div class="d-xl-flex flex-xl-row justify-content-between style_border_bottom_desk">
            <div class="d-flex flex-column style_padding_18_20 style_border_bottom border_bottom_transparent_desk">
                <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered d-xl-none">Доставлен 25 января 2021</span>
                <span class="style_16_24">23 января 2021</span>
                <span class="style_text_24_36 fw-bold">№ <span>378-5929-3378</span></span>
                <span class="style_16_24">Санкт-Петербург, ул Верейская, д. 47, кв, 45 </span>
                <!--if fiz two span under d-none-->
                <span class="style_16_24 d-inline-block margin_top_05">Плательщик: ИП Варнаков</span>
                <span class="d-xl-none">Накладная: № 38759459345</span>
                <span  class="style_text_24_36 fw-bold d-xl-none">24 578,90 ₽</span>
            </div>
            <div class="d-none d-xl-flex flex-column justify-content-between align-items-end style_border_bottom border_bottom_transparent_desk style_padding_18_20">
                <span class="style_blue_radious style_btn_blue margin_bottom style_text_10_10 fw-bold style_order_tip text-uppercase style_order_delivered">Доставлен 25 января 2021</span>
                <span class="style_text_24_36 fw-bold">24 578,90 ₽</span>
                <span>Накладная: № 38759459345</span>
            </div>
        </div>
        <div class="d-inline-block margin_bottom width_100_desk">
            <div class="d-flex flex-column flex-xl-row justify-content-start style_padding_18_20 style_border_bottom">
                <div class="position-relative in_stock_tip margin_right_desk">
                    <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                </div>
                <div class="d-flex flex-column justify-content-start flex-xl-grow-1 margin_right_desk">
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom margin_bottom_x2_desk">СВИНИНА шея без кости с/м вес 3 кг.</a>
                    <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <div class="d-xl-flex d-xl-row justify-content-between align-items-center">
                            <b class="d-inline-block">Цена за блок: 181 278,00 ₽</b>
                            <span class="style_text_24_36 fw-bold d-none d-xl-block">12 шт.</span>
                        </div>
                    </div>
                    <span class="style_text_24_36 fw-bold d-xl-none">12 шт.</span>
                </div>
            </div>
        </div>
        <div class="d-inline-block margin_bottom width_100_desk">
            <div class="d-flex flex-column flex-xl-row justify-content-start style_padding_18_20 padding_bottom_0">
                <div class="position-relative in_stock_tip margin_right_desk">
                    <img class="width_220_desk" src="img/img_items_catalog/с%20полями%206.jpg" width="100%" alt="photo item">
                </div>
                <div class="d-flex flex-column justify-content-start flex-xl-grow-1 margin_right_desk">
                    <span class="d-inline-block fw-bold style_text_10_10 color__lightblue text-uppercase margin_bottom">Новинка</span>
                    <a href="item.php" class="fw-bold margin_bottom margin_bottom_x2_desk">СВИНИНА шея без кости с/м вес 3 кг.</a>
                    <div class="d-flex flex-column justify-content-between position-relative margin_bottom">
                        <span>Цена за кг: 1 790,12 ₽</span>
                        <div class="d-xl-flex d-xl-row justify-content-between align-items-center">
                            <b class="d-inline-block">Цена за блок: 181 278,00 ₽</b>
                            <span class="style_text_24_36 fw-bold d-none d-xl-block">12 шт.</span>
                        </div>
                    </div>
                    <span class="style_text_24_36 fw-bold d-xl-none">12 шт.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column flex-xl-row justify-content-between">
        <button class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 width_50_desk margin_right_40_desk margin_bottom"><b>Повторить заказ</b> <br> <span class="fw-normal">10 354, 60 ₽</span></button>
        <button class="style_blue_radious style_btn_blue style_16_24 fw-bold style_width100 style_padding_10 width_50_desk margin_bottom">Скачать декларации и сертификаты</span></button>
    </div>
</div>