<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="d-flex flex-row justify-content-between align-items-center margin_bottom_x2">
            <span class="d-inline-block heading_24 style_text_40_50_desk margin_right flex-fit">Помощь</span>
            <hr class="flex-fill align-self-center" style="opacity: 1; height: 2px; color: #000000;">
        </div>
        <div class="page-layout__content__desk margin_bottom_x2">
            <div>
                <?php require('page_about_company.php'); ?>
            </div>
            <div class="page-layout__content">
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion ">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Обратная связь</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <div class="content_accordion d-none margin_top">
                        <ol class="padding_left">
                            <li class="margin_bottom_x2 fw-bold">Для клиентов или будущих клиентов 8-800-775-75-00</li>
                            <li class="margin_bottom_x2 fw-bold">Для поставщиков 8-800-775-75-00</li>
                            <li class="fw-bold">Для прессы 8-800-775-75-00</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Как стать клиентом</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        Заказ для физического лица:<br><br>
                        1.Заказ можно оформить самостоятельно на нашем сайте.<br><br>

                        Минимальная сумма заказа 3000 рублей. Оформить заказ можно круглосуточно.<br><br>
                        C 9.00 до 19.00 по будням, с вами свяжется наш оператор для подтверждения заказа.<br><br>
                        Внимание! Если ваш заказ не подтвердили, значит он не оформлен.<br><br>
                        2. Заказ можно оформить по телефону нашего Контактного центра <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        Заказ для юридического лица:<br><br>
                        . Для того, чтобы стать нашим клиентом, необходимо позвонить по номеру Контактного центра <a href="tel:88007757500">8-800-775-75-00</a> и приготовить следующие документы:<br><br>
                        • Номер ИНН<br>
                        • ОГРН (ОГРНИП) – при оплате заказа наличными средствами.<br>
                        • Свидетельство о регистрации<br>
                        • Свидетельство о постановке на учет в налоговом органе и уставные документы – при отгрузке заказа с отсрочкой платежа и/или при согласовании кредитного лимита.<br><br>
                        Номер GUID в системе Меркурий, для отгрузки товаров животного происхождения.<br><br>
                        Если вы уже наш клиент, но у вас нет доступа к сайту, вы можете получить его удобным для Вас способом:<br><br>
                        • Позвоните на номер <a href="tel:88007757500">8-800-775-75-00</a><br>
                        • Заполните заявку в разделе «Задать вопрос — Нет доступа к личному кабинету» на сайте alidi.ru.<br>
                        • Обратитесь к своему Торговому представителю и он подаст за вас заявку. Для получения доступа необходимо подготовить следующие данные:<br><br>
                        • Адрес электронной почты<br>
                        • Фамилия, имя, отчество<br>
                        • Контактный номер телефона лица, которое будет заниматься оформлением заказоd.<br><br>
                        Когда ваш запрос будет обработан, на указанный вами почтовый ящик придет письмо с логином и паролем для авторизации.
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Как оплатить заказ</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        Существует несколько способов оплаты:<br><br>
                        1. Наличными по факту доставки или при самовывозе.<br>
                        2. Безналичный расчет на наш счет, указанный в договоре.<br>
                        3. Отсрочка оплаты, согласованная для каждого клиента индивидуально.<br><br>
                        Если на момент оформления заказа у Вас будет просроченная дебиторская задолженность или превышение кредитного лимита – система сделает предупреждение.<br><br>
                        Если у Вас есть вопросы по задолженности – их можно задать вашему Торговому представителю (контактная информация есть в Личном кабинете) или специалистам Контактного центра по номеру <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        Для того, чтобы отслеживать денежный оборот, воспользуйтесь разделом «Финансы», который находится в Личном кабинете. Здесь Вы можете увидеть:<br><br>
                        • Каждого подключенного к аккаунту плательщика<br>
                        • Информацию по лимиту кредита<br>
                        • Задолженности по отгруженным заказам<br>
                        • Просроченные задолженности<br>
                        • Свободный кредитный лимит и условии отсрочки<br>
                        • Ниже в разделе можно увидеть информацию по оплатам и накладным.<br><br>
                        Данную информацию можно выгрузить в exel.
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Как получить заказ</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        Доставка осуществляется по Санкт-Петербургу и Ленинградской области:<br><br>
                        • Минимальная сумма заказа 3000 рублей<br>
                        • Заказ, оформленный до 18.00, будет доставлен Вам на следующий день<br>
                        • Заказ, оформленный после 18.00, будет доставлен Вам в течение двух дней<br>
                        • График доставки заказов: с 9.00 до 20.00, с понедельника по субботу (за исключением праздничных дней). Вы можете выбрать удобное время доставки при оформлении заказа.<br><br>
                        Доставка по Северо-Западному Федеральному округу<br><br>
                        Сюда входят отдаленные части Ленинградской области, Петрозаводск, Великий Новгород и часть Псковской области.<br><br>
                        Поставка осуществляется по графику доставки заказов, уточнить который вы можете в Контактном центре по номеру: <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        При доставке в Северо-Западный Федеральный округ, цены могут отличаться от указанных на сайте. Уточнить цены можно у вашего Торгового Представителя или по телефону.
                        Самовывоз со склада<br><br>
                        Заказ, оформленный до 18.00, можно забрать со склада на следующий день.<br>
                        Заказ, оформленный после 18.00, можно забрать со склада через день после оформления.<br>
                        График работы склада: с 10.00 до 18.00, с понедельника по субботу (за исключением праздничных дней).<br><br>
                        Внимание! Самовывоз осуществляется после оплаты в кассе, путем наличного расчета, или с помощью предоплаты на расчетный счет компании.<br><br>
                        Оплата по банковским картам самовывоза, невозможна.<br><br>
                        Для вашего удобства на территории склада стоит банкомат ПАО "Сбербанк"<br><br>
                        Перед приездом, необходимо оформить пропуск на ваш транспорт по телефону <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        При самовывозе в субботу, пропуск нужно оформить в пятницу.<br><br>
                        Открытие нового адреса доставки<br>
                        Для открытия нового адреса доставки обратитесь в наш Контактный центр по номеру:<a href="tel:88007757500">8-800-775-75-00</a>
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Запрос документов</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        content
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Возврат товара</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        К сожалению, на сайте нет возможности оформления возврата.
                        Запрос на возврат может быть сделан через специалистов Контактного центра по телефону :<a href="tel:88007757500">8-800-775-75-00</a> или через вашего Торгового представителя.<br><br>
                        ALIDI производит возврат своими силами, при условии подтверждения того, что товар ненадлежащего качества. Для подтверждения возврата товара, необходим визит Торгового представителя.
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Личный кабинет</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        content
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Действия при возникновении проблем с оплатой</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        Оплата проходит в течение нескольких минут.<br><br>
                        Оплата может не пройти, потому что:<br>
                        - Вы ввели неверные данные карты.<br>
                        - У карты закончился срок действия.<br>
                        - На карте недостаточно денег.<br>
                        - Нельзя подтвердить операцию по карте одноразовым паролем из СМС.<br>
                        - Банк установил запрет на оплату в интернете.<br><br>
                        Если оплата не прошла:<br>
                        - Повторите попытку через 20 минут.<br>
                        - Обратитесь в банк, выпустивший карту.<br>
                        - Попробуйте оплатить другой картой.<br><br>
                        Если у Вас все же не получилось оплатить картой, позвоните по телефону <a href="tel:88007757500">8-800-775-75-00</a> и оператор поможет вас решить данный вопрос.
                    </span>
                </div>
            </div>
            <div class="margin_bottom">
                <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                    <div class="d-flex flex-row justify-content-between header_accordion">
                        <span class="d-inline-flex style_text_20_30 style_text_24_36_desk fw-bold">Информация о SSL-шифровании</span>
                        <div class="icon_arrow_down_small"></div>
                    </div>
                    <span class="d-inline-block content_accordion d-none margin_top">
                        Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платёжный шлюз ПАО                                СБЕРБАНК.<br><br>
                        Соединение с платёжным шлюзом и передача информации осуществляется в защищённом режиме с использованием протокола шифрования SSL.<br><br>
                        В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa, MasterCard SecureCode, MIR Accept, J-Secure для проведения платежа также может потребоваться ввод специального пароля.<br><br>
                        Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платёжных систем МИР, Visa Int., MasterCard Europe Sprl, JCB.
                    </span>
                </div>
            </div>
        </div>
        </div>
        <div class="page-layout__content_mob">
            <div class="page-layout__content">
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion ">
                            <span class="d-inline-flex style_text_20_30  fw-bold">Обратная связь</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <div class="content_accordion d-none margin_top">
                            <ol class="padding_left">
                                <li class="margin_bottom_x2 fw-bold">Для клиентов или будущих клиентов 8-800-775-75-00</li>
                                <li class="margin_bottom_x2 fw-bold">Для поставщиков 8-800-775-75-00</li>
                                <li class="fw-bold">Для прессы 8-800-775-75-00</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Как стать клиентом</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        Заказ для физического лица:<br><br>
                        1.Заказ можно оформить самостоятельно на нашем сайте.<br><br>

                        Минимальная сумма заказа 3000 рублей. Оформить заказ можно круглосуточно.<br><br>
                        C 9.00 до 19.00 по будням, с вами свяжется наш оператор для подтверждения заказа.<br><br>
                        Внимание! Если ваш заказ не подтвердили, значит он не оформлен.<br><br>
                        2. Заказ можно оформить по телефону нашего Контактного центра <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        Заказ для юридического лица:<br><br>
                        . Для того, чтобы стать нашим клиентом, необходимо позвонить по номеру Контактного центра <a href="tel:88007757500">8-800-775-75-00</a> и приготовить следующие документы:<br><br>
                        • Номер ИНН<br>
                        • ОГРН (ОГРНИП) – при оплате заказа наличными средствами.<br>
                        • Свидетельство о регистрации<br>
                        • Свидетельство о постановке на учет в налоговом органе и уставные документы – при отгрузке заказа с отсрочкой платежа и/или при согласовании кредитного лимита.<br><br>
                        Номер GUID в системе Меркурий, для отгрузки товаров животного происхождения.<br><br>
                        Если вы уже наш клиент, но у вас нет доступа к сайту, вы можете получить его удобным для Вас способом:<br><br>
                        • Позвоните на номер <a href="tel:88007757500">8-800-775-75-00</a><br>
                        • Заполните заявку в разделе «Задать вопрос — Нет доступа к личному кабинету» на сайте alidi.ru.<br>
                        • Обратитесь к своему Торговому представителю и он подаст за вас заявку. Для получения доступа необходимо подготовить следующие данные:<br><br>
                        • Адрес электронной почты<br>
                        • Фамилия, имя, отчество<br>
                        • Контактный номер телефона лица, которое будет заниматься оформлением заказоd.<br><br>
                        Когда ваш запрос будет обработан, на указанный вами почтовый ящик придет письмо с логином и паролем для авторизации.
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Как оплатить заказ</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        Существует несколько способов оплаты:<br><br>
                        1. Наличными по факту доставки или при самовывозе.<br>
                        2. Безналичный расчет на наш счет, указанный в договоре.<br>
                        3. Отсрочка оплаты, согласованная для каждого клиента индивидуально.<br><br>
                        Если на момент оформления заказа у Вас будет просроченная дебиторская задолженность или превышение кредитного лимита – система сделает предупреждение.<br><br>
                        Если у Вас есть вопросы по задолженности – их можно задать вашему Торговому представителю (контактная информация есть в Личном кабинете) или специалистам Контактного центра по номеру <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        Для того, чтобы отслеживать денежный оборот, воспользуйтесь разделом «Финансы», который находится в Личном кабинете. Здесь Вы можете увидеть:<br><br>
                        • Каждого подключенного к аккаунту плательщика<br>
                        • Информацию по лимиту кредита<br>
                        • Задолженности по отгруженным заказам<br>
                        • Просроченные задолженности<br>
                        • Свободный кредитный лимит и условии отсрочки<br>
                        • Ниже в разделе можно увидеть информацию по оплатам и накладным.<br><br>
                        Данную информацию можно выгрузить в exel.
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Как получить заказ</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        Доставка осуществляется по Санкт-Петербургу и Ленинградской области:<br><br>
                        • Минимальная сумма заказа 3000 рублей<br>
                        • Заказ, оформленный до 18.00, будет доставлен Вам на следующий день<br>
                        • Заказ, оформленный после 18.00, будет доставлен Вам в течение двух дней<br>
                        • График доставки заказов: с 9.00 до 20.00, с понедельника по субботу (за исключением праздничных дней). Вы можете выбрать удобное время доставки при оформлении заказа.<br><br>
                        Доставка по Северо-Западному Федеральному округу<br><br>
                        Сюда входят отдаленные части Ленинградской области, Петрозаводск, Великий Новгород и часть Псковской области.<br><br>
                        Поставка осуществляется по графику доставки заказов, уточнить который вы можете в Контактном центре по номеру: <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        При доставке в Северо-Западный Федеральный округ, цены могут отличаться от указанных на сайте. Уточнить цены можно у вашего Торгового Представителя или по телефону.
                        Самовывоз со склада<br><br>
                        Заказ, оформленный до 18.00, можно забрать со склада на следующий день.<br>
                        Заказ, оформленный после 18.00, можно забрать со склада через день после оформления.<br>
                        График работы склада: с 10.00 до 18.00, с понедельника по субботу (за исключением праздничных дней).<br><br>
                        Внимание! Самовывоз осуществляется после оплаты в кассе, путем наличного расчета, или с помощью предоплаты на расчетный счет компании.<br><br>
                        Оплата по банковским картам самовывоза, невозможна.<br><br>
                        Для вашего удобства на территории склада стоит банкомат ПАО "Сбербанк"<br><br>
                        Перед приездом, необходимо оформить пропуск на ваш транспорт по телефону <a href="tel:88007757500">8-800-775-75-00</a><br><br>
                        При самовывозе в субботу, пропуск нужно оформить в пятницу.<br><br>
                        Открытие нового адреса доставки<br>
                        Для открытия нового адреса доставки обратитесь в наш Контактный центр по номеру:<a href="tel:88007757500">8-800-775-75-00</a>
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Запрос документов</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        content
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Возврат товара</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        К сожалению, на сайте нет возможности оформления возврата.
                        Запрос на возврат может быть сделан через специалистов Контактного центра по телефону :<a href="tel:88007757500">8-800-775-75-00</a> или через вашего Торгового представителя.<br><br>
                        ALIDI производит возврат своими силами, при условии подтверждения того, что товар ненадлежащего качества. Для подтверждения возврата товара, необходим визит Торгового представителя.
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Личный кабинет</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        content
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Действия при возникновении проблем с оплатой</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        Оплата проходит в течение нескольких минут.<br><br>
                        Оплата может не пройти, потому что:<br>
                        - Вы ввели неверные данные карты.<br>
                        - У карты закончился срок действия.<br>
                        - На карте недостаточно денег.<br>
                        - Нельзя подтвердить операцию по карте одноразовым паролем из СМС.<br>
                        - Банк установил запрет на оплату в интернете.<br><br>
                        Если оплата не прошла:<br>
                        - Повторите попытку через 20 минут.<br>
                        - Обратитесь в банк, выпустивший карту.<br>
                        - Попробуйте оплатить другой картой.<br><br>
                        Если у Вас все же не получилось оплатить картой, позвоните по телефону <a href="tel:88007757500">8-800-775-75-00</a> и оператор поможет вас решить данный вопрос.
                    </span>
                    </div>
                </div>
                <div class="margin_bottom">
                    <div class="d-flex flex-column justify-content-between style_gray_radius style_padding_15_20 cursor_pointer">
                        <div class="d-flex flex-row justify-content-between header_accordion">
                            <span class="d-inline-flex style_text_20_30 fw-bold">Информация о SSL-шифровании</span>
                            <div class="icon_arrow_down_small"></div>
                        </div>
                        <span class="d-inline-block content_accordion d-none margin_top">
                        Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платёжный шлюз ПАО                                СБЕРБАНК.<br><br>
                        Соединение с платёжным шлюзом и передача информации осуществляется в защищённом режиме с использованием протокола шифрования SSL.<br><br>
                        В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa, MasterCard SecureCode, MIR Accept, J-Secure для проведения платежа также может потребоваться ввод специального пароля.<br><br>
                        Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платёжных систем МИР, Visa Int., MasterCard Europe Sprl, JCB.
                    </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

