<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout container">
    <div class="page-layout__header d-flex flex-row justify-content-between">
        <?php require('header.php'); ?>
    </div>
    <div class="container">
        <div class="page-layout__content d-flex flex-column justify-content-lg-between">
            <?php require('page_index.php'); ?>
        <div class="">
            <?php require('footer.php'); ?>
        </div>
        </div>
    </div>
</div>
<?php require('js.php'); ?>
</body>
</html>

