<div>
    <div class="d-flex flex-row justify-content-start style_gray_radius style_padding_18_20 margin_bottom rounded-pill">
        <img src="img/iconUserRectangle.svg" width="20" alt="search" class="margin_right">
        <select class="form-select form-select-sm style_border_transparent input_date_rang fw-bold" aria-label=".form-select-lg example">
            <option selected>Все плательщики</option>
            <option value="1">плательщик 1</option>
            <option value="2">плательщик 2</option>
            <option value="3">плательщик 3</option>
            <option value="4">плательщик 4</option>
            <option value="5">плательщик 5</option>
        </select>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk padding_30_desk position-relative">
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 margin_top-15_desk style_border_bottom">
            <span class="style_16_24 style_text_24_36_desk fw-bold">Варнаков Николай Иванович</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Телефон</span>
            <span>+7 925 747-98-33</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Email</span>
            <span>varnakov_n@gmail.com</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom border_bottom_transparent_desk">
            <span class="style_16_24 style_width50">Уровень доступа</span>
            <span>Покупка</span>
        </div>
        <div class="d-flex flex-row align-content-center justify-content-center style_padding_top_18 d-xl-none">
            <a href="refactor_all.php"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            <img class="cursor_pointer" data-bs-toggle="modal" data-bs-target="#modal_delete_address_mob" src="img/iconDelete.svg" width="25" alt="delete">
        </div>
        <div class="d-none d-xl-flex flex-row align-content-center justify-content-center style_padding_top_18 position-absolute top-0 end-2">
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_refactor_all"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_delete_address"><img class="cursor_pointer delete_js" src="img/iconDelete.svg" width="25" alt="delete"></a>
        </div>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_gray_radius style_border_lightgray style_border_radius_40_only_desk padding_30_desk position-relative">
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 margin_top-15_desk style_border_bottom">
            <span class="style_16_24 style_text_24_36_desk fw-bold">Варнаков Николай Иванович</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Телефон</span>
            <span>+7 925 747-98-33</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom">
            <span class="style_16_24 style_width50">Email</span>
            <span>varnakov_n@gmail.com</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_bottom__top_10 style_border_bottom border_bottom_transparent_desk">
            <span class="style_16_24 style_width50">Уровень доступа</span>
            <span>Покупка</span>
        </div>
        <div class="d-flex flex-row align-content-center justify-content-center style_padding_top_18 d-xl-none">
            <a href="refactor_all.php"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            <img class="cursor_pointer" data-bs-toggle="modal" data-bs-target="#modal_delete_address_mob" src="img/iconDelete.svg" width="25" alt="delete">
        </div>
        <div class="d-none d-xl-flex flex-row align-content-center justify-content-center style_padding_top_18 position-absolute top-0 end-2">
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_refactor_all"><img class="margin_right_x2 cursor_pointer" src="img/iconEdit.svg" width="25" alt="edit"></a>
            <a href="#" data-bs-toggle="modal" data-bs-target="#modal_delete_address"><img class="cursor_pointer delete_js" src="img/iconDelete.svg" width="25" alt="delete"></a>
        </div>
    </div>
    <button data-bs-toggle="modal" data-bs-target="#modal_add_address" class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width100 style_padding_10 width_50_desk">Добавить пользователя</button>
</div>
