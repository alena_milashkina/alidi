<!DOCTYPE html>
<html lang="ru-RU">
<?php require('head.php'); ?>
<body>
<div class="page-layout">
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_padding_18_20 position-relative">
        <div class="d-flex flex-row style_text_24_36 justify-content-between header_accordion align-items-center style_width100">
            <div class="d-flex flex-row align-items-center">
                <span class="d-inline-flex fw-bold margin_right">Регистрация</span>
                <div class="icon_arrow_down_small icon_arrow_down_small_bg_white"></div>
            </div>
            <a href="index.php" class="close_menu margin_top_0 align-self-start"><img src="img/iconCancel.svg" width="20" alt="cancel"></a>
        </div>
        <div class="d-flex flex-column justify-content-between style_gray_radius style_text_20_30 fw-bold style_padding_15_20 content_accordion d-none position-absolute top-100 start-05 style_width95">
            <a href="entrance.php" class="style_border_bottom d-inline-block">Вход</a>
            <a href="auth.php" class="d-inline-block">Регистрация</a>
        </div>
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center margin_bottom style_blue_radious style_accordion accordion margin_left_right">
            <span class="d-inline-block auth_fiz style_padding_18_20 style_text_12_16 fw-bold text-center accordion_active">Физическое лицо</span>
            <span class="d-inline-block auth_ur style_padding_18_20 style_text_12_16 fw-bold text-center">Юридическое лицо</span>
    </div>
    <form method="post" class="d-flex flex-column justify-content-start align-items-center form_fiz">
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="name" class="style_text_12_16 color_gray">ФИО</label>
            <input name="name" type="text" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="phone" class="style_text_12_16 color_gray">Телефон</label>
            <input name="phone" type="tel" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="email" class="style_text_12_16 color_gray">Email</label>
            <input name="email" type="email" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="password" class="style_text_12_16 color_gray">Пароль</label>
            <input name="password" class="style_border_transparent" type="password">
        </div>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Зарегистрироваться">
        <span class="style_text_12_16 style_width95 margin_bottom text-center fw-normal">Регистрируясь на нашем сайте, Вы даете согласие с условиями <span class="color__lightblue style_width95"><a href="#">пользовательского соглашения</a></span></span>
        <span class="style_16_24 fw-bold"><a href="entrance.php">Войти с паролем</a></span>
    </form>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center form_ur d-none">
        <div class="d-flex flex-row justify-content-between margin_bottom style_width95">
            <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select">
                <option selected>Организационно-правовая форма</option>
                <option value="1">ИП</option>
                <option value="2">ООО</option>
                <option value="3">ОАО</option>
            </select>
        </div>
        <div class="d-flex flex-row justify-content-between margin_bottom style_width95">
            <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select">
                <option selected>Категория клента</option>
                <option value="1">категория1</option>
                <option value="2">категория2</option>
                <option value="3">категория3</option>
            </select>
        </div>
        <div class="d-flex flex-column justify-content-between cursor_pointer style_border_radius_20 border_right_lightgray style_padding_18_20 margin_bottom">
            <div class="d-flex flex-row justify-content-between header_accordion">
                <span class="d-inline-block">Кулинарные концепции</span>
                <div class="icon_arrow_down_small_bg_white"></div>
            </div>
            <div class="d-inline-block content_accordion d-none margin_top">
                <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_border_bottom">
                    <label class="" for="concept">
                        <input type="checkbox" name="concept" class="form-check-input margin_right" checked>
                        Япония
                    </label>
                </div>
                <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_border_bottom">
                    <label class="" for="concept">
                        <input type="checkbox" name="concept" class="form-check-input margin_right" checked>
                        Паназия
                    </label>
                </div>
                <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom style_border_bottom">
                    <label class="" for="concept">
                        <input type="checkbox" name="concept" class="form-check-input margin_right" checked>
                        Италия
                    </label>
                </div>
            </div>
        </div>
        <!--<div class="d-flex flex-row justify-content-between margin_bottom style_width95">
            <select class="form-select form-select-lg style_16_24 style_border_radius_20" aria-label=".form-select" multiple >
                <option selected>Кулинарные концепции</option>
                <option value="1">Япония</option>
                <option value="2">Паназия</option>
                <option value="3">Италия</option>
                <option value="4">Европейская</option>
                <option value="5">Мексиканская</option>
            </select>
        </div>-->
        <div class="margin_bottom_x2 style_width95">
            <span class="d-inline-block margin_bottom_x2">Укажите объем закупок тыс. ₽/год</span>
            <input type="hidden" class="slider-input margin_top_bottom_10" value="23"/>
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="name_org" class="style_text_12_16 color_gray">Наименование</label>
            <input name="name_org" type="text" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="ogrn" class="style_text_12_16 color_gray">ОГРН</label>
            <input name="ogrn" type="number" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="inn" class="style_text_12_16 color_gray">ИНН</label>
            <input name="inn" type="number" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="kpp" class="style_text_12_16 color_gray">КПП</label>
            <input name="kpp" type="number" class="style_border_transparent">
        </div>
        <!--<input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Наименование">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="number" placeholder="ОГРН">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="number" placeholder="ИНН">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="number" placeholder="КПП">-->
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="UrAddress" class="style_text_12_16 color_gray">Юридический адрес</label>
            <input name="UrAddress" type="text" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="factAddress" class="style_text_12_16 color_gray">Фактический адрес</label>
            <input name="factAddress" type="text" class="style_border_transparent">
        </div>
        <!--<input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Юридический адрес">
        <input class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width95" type="text" placeholder="Фактический адрес">-->
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="name" class="style_text_12_16 color_gray">ФИО представителя</label>
            <input name="name" type="text" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="phone" class="style_text_12_16 color_gray">Телефон</label>
            <input name="phone" type="tel" class="style_border_transparent" placeholder="+7 000 000 00 00">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="email" class="style_text_12_16 color_gray">Email</label>
            <input name="email" type="email" class="style_border_transparent">
        </div>
        <div class="d-flex flex-column justify-content-between style_padding_18_20 style_padding_bottom__top_10 margin_bottom style_gray_radius style_border_lightgray style_width95">
            <label for="password" class="style_text_12_16 color_gray">Пароль</label>
            <input name="password" class="style_border_transparent" type="password">
        </div>
        <input class="style_blue_radious style_btn_blue margin_bottom style_16_24 fw-bold style_width95" type="submit" value="Зарегистрироваться">
        <span class="style_text_12_16 style_width95 margin_bottom text-center fw-normal">Регистрируясь на нашем сайте, Вы даете согласие с условиями <span class="color__lightblue style_width95"><a href="#">пользовательского соглашения</a></span></span>
        <span class="style_16_24 fw-bold margin_bottom"><a href="entrance.php">Войти с паролем</a></span>
    </form>
</div>
<?php require('js.php'); ?>
</body>
</html>



