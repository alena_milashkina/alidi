<div class="max_width_560_desk">
    <div class="d-flex flex-column flex-xl-row justify-content-between">
        <div class="d-flex flex-column justify-content-between margin_bottom">
            <span class="style_16_24">Ваш статус</span>
            <span class="style_text_24_36 color_blue fw-bold">Розничный покупатель</span>
            <span class="style_16_24 fw-bold">Вы участвуете в бальной системе</span>
        </div>
        <div class="d-flex flex-column justify-content-between margin_bottom text-xl-end">
            <span class="style_16_24">Ваш баланс</span>
            <span class="style_text_24_36 color_blue fw-bold">1459</span>
            <span class="style_16_24 fw-bold">баллов</span>
        </div>
    </div>
    <form method="post" class="d-flex flex-column justify-content-between align-items-center">
        <output class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width100 position-relative" type="text"><span class="position-absolute top-0 style_text_12_16 color_gray">ФИО</span>пример множественного редактирования МОБИЛКА<a href="refactor_all.php"><img class="position-absolute top-50 end-1 translate-middle-y" src="img/iconEdit.svg" width="25" alt="edit"></a></output>
        <output class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width100 position-relative" type="tel"><span class="position-absolute top-0 style_text_12_16 color_gray">Телефон</span>пример еденичного редактирования через модальное окно ДЕСКТОП<a href="#"><img data-bs-toggle="modal" data-bs-target="#modal_refactor_single" class="position-absolute top-50 end-1 translate-middle-y" src="img/iconEdit.svg" width="25" alt="edit"></a></output>
        <output class="style_gray_radius style_padding_18_20 style_input margin_bottom style_width100 position-relative" type="email"><span class="position-absolute top-0 style_text_12_16 color_gray">Email</span>ovechkin@gmail.com</output>
    </form>
    <div>
        <span class="style_16_24 style_text_24_36_desk fw-bold d-grid align-content-center justify-content-start margin_bottom">История списаний и начислений</span>
    </div>
    <div class="d-flex flex-column justify-content-between style_padding_18_20 margin_bottom_x2 style_gray_radius style_border_lightgray style_border_radius_40_only_desk">
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">Дата</span>
            <span>Баллы</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">17.01.2021</span>
            <span>- 340</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">17.01.2021</span>
            <span>- 120</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">17.01.2021</span>
            <span>+497</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">17.01.2021</span>
            <span>+1038</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">17.01.2021</span>
            <span>-285</span>
        </div>
        <div class="d-flex flex-row justify-content-between style_padding_10 style_border_bottom">
            <span class="style_16_24 fw-bold">17.01.2021</span>
            <span>+34</span>
        </div>
    </div>
</div>
